/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

/* this is all going away... replaced by code in js/jsd/java */

#if 0

#ifndef XP_MAC
#include "_stubs/netscape_jsdebug_Script.c"
#include "_stubs/netscape_jsdebug_DebugController.c"
#include "_stubs/netscape_jsdebug_JSThreadState.c"
#include "_stubs/netscape_jsdebug_JSStackFrameInfo.c"
#include "_stubs/netscape_jsdebug_JSPC.c"
#include "_stubs/netscape_jsdebug_JSSourceTextProvider.c"
#else
#include "netscape_jsdebug_Script.c"
#include "n_jsdebug_DebugController.c"
#include "n_jsdebug_JSThreadState.c"
#include "n_jsdebug_JSStackFrameInfo.c"
#include "netscape_jsdebug_JSPC.c"
#include "n_j_JSSourceTextProvider.c"
#endif

#endif
