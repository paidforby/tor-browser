#
# The contents of this file are subject to the Netscape Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/NPL/
#
# Software distributed under the License is distributed on an "AS
# IS" basis, WITHOUT WARRANTY OF ANY KIND, either express oqr
# implied. See the License for the specific language governing
# rights and limitations under the License.
#
# The Original Code is Mozilla Communicator client code, released
# March 31, 1998.
#
# The Initial Developer of the Original Code is Netscape
# Communications Corporation.  Portions created by Netscape are
# Copyright (C) 1998 Netscape Communications Corporation. All
# Rights Reserved.
#
# Contributor(s): 
#
# Alternatively, the contents of this file may be used under the
# terms of the GNU Public License (the "GPL"), in which case the
# provisions of the GPL are applicable instead of those above.
# If you wish to allow use of your version of this file only
# under the terms of the GPL and not to allow others to use your
# version of this file under the NPL, indicate your decision by
# deleting the provisions above and replace them with the notice
# and other provisions required by the GPL.  If you do not delete
# the provisions above, a recipient may use your version of this
# file under either the NPL or the GPL.
#

DEPTH		= ../..
topsrcdir	= @top_srcdir@
srcdir		= @srcdir@
VPATH		= @srcdir@

include $(DEPTH)/config/autoconf.mk

MODULE		= js
LIBRARY_NAME	= mozjs

DIRS		= fdlibm

CSRCS		= \
		jsapi.c \
		jsarena.c \
		jsarray.c \
		jsatom.c \
		jsbool.c \
		jscntxt.c \
		jsdate.c \
		jsdbgapi.c \
		jsdhash.c \
		jsdtoa.c \
		jsemit.c \
		jsexn.c \
		jsfun.c \
		jsgc.c \
		jshash.c \
		jsinterp.c \
		jslock.c \
		jslog2.c \
		jslong.c \
		jsmath.c \
		jsnum.c \
		jsobj.c \
		jsopcode.c \
		jsparse.c \
		jsprf.c \
		jsregexp.c \
		jsscan.c \
		jsscope.c \
		jsscript.c \
		jsstr.c \
		jsutil.c \
		jsxdrapi.c \
		prmjtime.c \
		$(NULL)

EXPORTS		= \
		js.msg \
		jsapi.h \
		jsarray.h \
		jsarena.h \
		jsatom.h \
		jsbit.h \
		jsbool.h \
		jsclist.h \
		jscntxt.h \
		jscompat.h \
		jsconfig.h \
		jsdate.h \
		jsdbgapi.h \
		jsdhash.h \
		jsemit.h \
		jsfun.h \
		jsgc.h \
		jshash.h \
		jsinterp.h \
		jslock.h \
		jslong.h \
		jsmath.h \
		jsnum.h \
		jsobj.h \
		jsopcode.tbl \
		jsopcode.h \
		jsosdep.h \
		jsotypes.h \
		jsparse.h \
		jsprf.h \
		jsprvtd.h \
		jspubtd.h \
		jsregexp.h \
		jsscan.h \
		jsscope.h \
		jsscript.h \
		jsstr.h \
		jstypes.h \
		jsutil.h \
		jsxdrapi.h \
		jsstddef.h \
		$(NULL)

EXPORTS		:= $(addprefix $(srcdir)/, $(EXPORTS))

FDLIBM_LIBRARY	= fdlibm/libfdm.$(LIB_SUFFIX)
JSMATH_PRELINK	= jsmathtemp.o
JS_SAFE_ARENA	= 1

DASH_R		= -r

include $(topsrcdir)/config/config.mk

ifeq ($(OS_ARCH),OS2)
ifneq ($(MOZ_WIDGET_TOOLKIT),os2)
ifndef XCFLAGS
OS2_IMPLIB	= 1
LIBRARY		= js$(MOZ_BITS)$(VERSION_NUMBER).$(LIB_SUFFIX)
DEF_FILE	= jsos2$(VERSION_NUMBER).def
EXTRA_LIBS	= $(NSPR_LIBS) $(LIBNSJAVA)
else
EXTRA_LIBS	= $(NSPR_LIBS) $(LIBNSJAVA) libjs.lib
endif
OS_CFLAGS	+= -tm-
endif
endif

EXTRA_DSO_LDOPTS += $(MOZ_COMPONENT_NSPR_LIBS)

# When using gcc the assembly is inlined in the C-file (see jslock.c)
ifdef NS_USE_NATIVE
ASFILES		= $(notdir $(wildcard $(srcdir)/*_$(OS_ARCH).s))
endif

ifeq ($(MOZ_WIDGET_TOOLKIT),os2)
DEF_OBJS = jsapi.o jsarena.o jsdbgapi.o jsdhash.o jsdtoa.o jsgc.o jshash.o \
	jsinterp.o jslog2.o jslong.o jsprf.o jsutil.o jsxdrapi.o prmjtime.o
#ADD_TO_DEF_FILE = cat < $(srcdir)/extradefs.os2 >>$(DEF_FILE)
endif

ifndef BUILD_OPT
MOCHAFILE	= 1
endif

ifndef NSBUILDROOT
JSJAVA_STUBHEADERS = \
		-I$(topsrcdir)/sun-java/include/_gen \
		-I$(topsrcdir)/sun-java/netscape/javascript/_jri \
		-I$(topsrcdir)/sun-java/netscape/security/_jri
else
JSJAVA_STUBHEADERS = -I$(JRI_GEN_DIR) -I$(JDK_GEN_DIR)
endif

JSJAVA_CFLAGS	= \
		-I$(topsrcdir)/sun-java/md-include \
		-I$(topsrcdir)/sun-java/include \
		$(JSJAVA_STUBHEADERS)

include $(topsrcdir)/config/rules.mk

DEFINES		+= -DEXPORT_JS_API 

INCLUDES	+= -I$(srcdir)

GARBAGE		+= $(JSMATH_PRELINK) jscpucfg.o jsautocfg.h jsautocfg.tmp jscpucfg

TARGETS		+= jscpucfg$(BIN_SUFFIX)

ifdef JS_SAFE_ARENA
DEFINES		+= -DJS_USE_SAFE_ARENA
endif

ifdef JS_THREADSAFE
DEFINES		+= -DJS_THREADSAFE
endif

ifdef JS_NO_THIN_LOCKS
DEFINES		+= -DJS_USE_ONLY_NSPR_LOCKS
endif

ifdef JS_VERSION
DEFINES		+= -DJS_VERSION=$(JS_VERSION)
endif

ifneq ($(findstring -L,$(NSPR_LIBS)),)
NSPR_STATIC_PATH = $(subst -L,,$(findstring -L,$(NSPR_LIBS)))
else
NSPR_STATIC_PATH = $(DIST)/lib
endif

LDFLAGS		+= $(pathsubst -l%,$(NSPR_STATIC_PATH)/%.a,$(NSPR_LIBS))

# BeOS and HP-UX do not require the extra linking of "-lm"
ifeq (,$(filter BeOS HP-UX,$(OS_ARCH)))
LDFLAGS		+= -lm
endif

ifeq ($(OS_ARCH),FreeBSD)
LDFLAGS		+= -pthread
endif
ifeq ($(OS_ARCH),IRIX)
ifdef USE_N32
DASH_R		+= -n32
endif
endif
ifeq ($(OS_ARCH),Linux)
LDFLAGS		+= -ldl
endif
ifeq ($(OS_ARCH),OSF1)
LDFLAGS		+= -lc_r
endif
ifeq ($(OS_ARCH),SunOS)
ifeq ($(CPU_ARCH),sparc)

ifndef JS_NO_ULTRA
ULTRA_OPTIONS	:= -xarch=v8plus,-DULTRA_SPARC
ULTRA_OPTIONSCC	:= -DULTRA_SPARC
else
ULTRA_OPTIONS	:= -xarch=v8
ULTRA_OPTIONSCC	:=
endif

ifeq ($(shell uname -m),sun4u)
ASFLAGS		+= -Wa,$(ULTRA_OPTIONS),-P,-L,-D_ASM,-D__STDC__=0 $(ULTRA_OPTIONSCC)
else
ASFLAGS		+= -Wa,-xarch=v8,-P,-L,-D_ASM,-D__STDC__=0
endif

endif
ifeq ($(OS_RELEASE),4.1)
LDFLAGS		+= -ldl -lnsl
else
LDFLAGS		+= -lposix4 -ldl -lnsl -lsocket
endif
endif

ifeq ($(OS_ARCH),QNX)
ifneq ($(OS_TARGET),NTO)
# Don't use wildcard here, because we only want this resolved at link time.
OBJS		+= fdlibm/*.o
endif
endif

# OS/2 linkers expect to create executables or dlls, not object files
# so we pull in what's needed from fdlibm when creating the js dll
ifneq ($(MOZ_WIDGET_TOOLKIT),os2)
# special rule for jsmath.o since we want to incrementally link
# against fdlibm to pull in only what is needed
jsmath.o: $(FDLIBM_LIBRARY) $(JSMATH_PRELINK)
ifeq ($(OS_ARCH),QNX)
ifneq ($(OS_TARGET),NTO)
	@cp $(JSMATH_PRELINK) $@
else
	$(LD) $(DASH_R) -o $@ $(JSMATH_PRELINK) $(FDLIBM_LIBRARY)
endif
else
	$(LD) $(DASH_R) -o $@ $(JSMATH_PRELINK) $(FDLIBM_LIBRARY)
endif

$(JSMATH_PRELINK): jsmath.c
ifeq ($(OS_ARCH),WINNT)
	$(CC) -Fo$@ -c $(CFLAGS) $<
else
	$(CC) -o $@ -c $(COMPILE_CFLAGS) $<
endif
endif

# An AIX Optimization bug causes PR_dtoa() & JS_dtoa to produce wrong result.
# This suppresses optimization for this single compilation unit.
ifeq ($(OS_ARCH),AIX)
jsdtoa.o: jsdtoa.c
	$(CC) -o $@ -c $(filter-out -O, $(COMPILE_CFLAGS)) $<
endif

$(FDLIBM_LIBRARY):
	@$(CONTINUE_ON_ERROR) \
	$(MAKE) -C $(@D) $(@F); \
	$(EXIT_ON_ERROR)

jsopcode.h jsopcode.c: jsopcode.tbl

jsautocfg.h: jscpucfg$(BIN_SUFFIX)
	@rm -f $@ jsautocfg.tmp
	./jscpucfg > jsautocfg.tmp
	mv jsautocfg.tmp $@

# jscpucfg is a strange target
# Needs to be built with the host compiler but needs to include
# the mdcpucfg for the target so it needs the appropriate target defines
ifdef HOST_NSPR_MDCPUCFG
HOST_CC := $(HOST_CC) -DMDCPUCFG=$(TARGET_NSPR_MDCPUCFG)
endif

ifeq ($(OS_ARCH),QNX)
ifneq ($(OS_TARGET),NTO)
# QNX's compiler apparently can't build a binary directly from a source file.
jscpucfg.o: jscpucfg.c
	$(HOST_CC) $(HOST_CFLAGS) -c $(DEFINES) $(NSPR_CFLAGS) -o $@ $<

jscpucfg: jscpucfg.o
	$(HOST_CC) $(HOST_CFLAGS) $(DEFINES) -o $@ $<
endif
else
jscpucfg$(BIN_SUFFIX): jscpucfg.c
ifeq ($(MOZ_OS2_TOOLS),VACPP)
	$(HOST_CC) $(HOST_CFLAGS) $(DEFINES) $(NSPR_CFLAGS) /Fe$@ $<
else
	$(HOST_CC) $(HOST_CFLAGS) $(DEFINES) $(NSPR_CFLAGS) -o $@ $<
endif
endif

export:: jsautocfg.h
	$(INSTALL) -m 444 $< $(PUBLIC)

