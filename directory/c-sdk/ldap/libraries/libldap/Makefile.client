DEPTH		= ../../../../..
NS_DEPTH	= ../../../..
LDAPSRC		= ../..

RM		= rm -f
SED		= sed

SRCS		= abandon.c \
		  add.c \
		  bind.c \
		  cache.c \
		  charray.c \
		  charset.c \
		  compare.c \
		  compat.c \
		  control.c \
		  countvalues.c \
		  delete.c \
		  disptmpl.c \
		  dsparse.c \
		  error.c \
		  extendop.c \
		  free.c \
		  freevalues.c \
		  friendly.c \
		  getattr.c \
		  getdn.c \
		  getdxbyname.c \
		  getentry.c \
		  getfilter.c \
		  getoption.c \
		  getvalues.c \
		  memcache.c \
		  message.c \
		  modify.c \
		  open.c \
		  os-ip.c \
		  psearch.c \
		  referral.c \
		  regex.c \
		  rename.c \
		  request.c \
		  reslist.c \
		  result.c \
		  saslbind.c \
		  sbind.c \
		  search.c \
		  setoption.c \
		  sort.c \
		  sortctrl.c \
		  srchpref.c \
		  tmplout.c \
		  ufn.c \
		  unbind.c \
		  unescape.c \
		  url.c \
		  utf8.c \
		  vlistctrl.c

REALOBJS	= $(SRCS:.c=.o)

#OBJS		= $(REALOBJS) versiont.o
OBJS		= $(REALOBJS)

HDIR		= $(LDAPSRC)/include

CFLAGS		= $(INCLUDES) $(DEFINES)

LIBRARY_NAME	= ldap

#
# DEFS are included in CFLAGS
#
DEFS		= $(PLATFORMCFLAGS) $(LDAP_DEBUG) $(KERBEROS) $(AFSKERBEROS) \
		  $(UOFM) $(UOFA) $(NO_USERINTERFACE) $(CLDAP) $(NO_CACHE) \
		  $(LDAP_REFERRALS) $(LDAP_DNS) $(STR_TRANSLATION) \
		  $(LIBLDAP_CHARSETS) $(LIBLDAP_DEF_CHARSET) \
		  $(SLAPD_BACKENDS) $(LDBMBACKEND) $(LDBMINCLUDE) $(PHONETIC) \
		  $(LDAP_SSLIO_HOOKS)

include $(DEPTH)/config/rules.mk

LOCAL_INCLUDES  = -I$(PUBLIC)/nspr
INCLUDES	+= -I$(HDIR) $(KRBINCLUDEFLAG)
DEFINES		+= $(DEFS) -DFILTERFILE="\"$(RUNTIMEETCDIR)/ldapfilter.conf\"" \
		   -DTEMPLATEFILE="\"$(RUNTIMEETCDIR)/ldaptemplates.conf\"" \
		   -DNET_SSL -UMOZILLA_CLIENT

# So we actually get the definition of hostent_data....
ifeq ($(OS_ARCH),AIX)
DEFINES               += -D_THREAD_SAFE
endif

GARBAGE		+= $(ETCDIR)/ldapfriendly $(ETCDIR)/ldapfilter.conf \
		   $(ETCDIR)/ldaptemplates.conf $(ETCDIR)/ldapsearchprefs.conf

PLATFORMCFLAGS	= -DUSE_WAITPID -DNEEDPROTOS
PLATFORMLIBS	=
THREADS		=
THREADSLIB	=

ETCFILES	= ldapfilter.conf \
		  ldapfriendly \
		  ldapsearchprefs.conf \
		  ldaptemplates.conf \
		  $(NULL)

ETCDIR		= $(DIST)/etc

#
# if you want things to run in a different directory from where they
# are installed, set this accordingly (this path gets compiled into a
# few binaries). otherwise, leave it alone.
#
RUNTIMEETCDIR	= $(ETCDIR)

#
# To build slapd (the stand-alone ldap daemon), uncomment the MAKESLAPD
# line and select the SLAPD_BACKENDS you want to use. If you enable the
# LDBM backend, also select one of the LDBM backends.
#
MAKESLAPD	= yes
SLAPD_BACKENDS	= -DLDAP_LDBM -DLDAP_SHELL -DLDAP_PASSWD
LDBMBACKEND	= -DLDBM_USE_NDBM

#
# uncomment this line to enable debugging code (a good idea)
#
ifndef BUILD_OPT
LDAP_DEBUG	= -DLDAP_DEBUG
endif

#
# uncomment this line to enable support for LDAP referrals in libldap
#
LDAP_REFERRALS		= -DLDAP_REFERRALS

#
# uncomment this line to enable support for SSL I/O in libldap
#
LDAP_SSLIO_HOOKS	= -DLDAP_SSLIO_HOOKS


###########################################################################

versiont.c:	Makefile.client Version.c
	@$(RM) $@
	@(u="$${USER-root}" v="$(shell cat ../../build/version)" d="$(shell pwd)" \
	h="$(shell hostname)" t="$(shell date)"; $(SED) -e "s|%WHEN%|$${t}|" \
	-e "s|%WHOANDWHERE%|$${u}@$${h}:$${d}|" \
	-e "s|%VERSION%|$${v}|" \
	< Version.c > $@)

$(LIBRARY):	$(OBJS)

install::	$(LIBRARY) $(ETCFILES)
	$(INSTALL) -m 444 $(LIBRARY) $(DIST)/ldap/lib
ifdef MKSHLIB
	$(INSTALL) -m 555 $(SHARED_LIBRARY) $(DIST)/ldap/bin
endif
	$(INSTALL) $(INSTALLFLAGS) -m 644 $(ETCFILES) $(ETCDIR)
	
