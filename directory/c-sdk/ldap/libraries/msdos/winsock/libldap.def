LIBRARY         NSLDAP
DESCRIPTION     'Lightweight Directory Access Protocol Client API for 16-bit Windows'
EXETYPE         WINDOWS
VERSION		1.0
CODE            PRELOAD MOVEABLE DISCARDABLE
DATA            PRELOAD MOVEABLE SINGLE

HEAPSIZE        4096

EXPORTS
; we need to manually assign ordinal numbers so we can add new routines
; and not disturb the ordinals and thus not require callers to relink.
	_ldap_abandon				@10
	_ldap_add					@11
	_ldap_unbind				@13
;obsolete	_ldap_enable_cache			@14
;obsolete	_ldap_disable_cache			@15
;obsolete 	_ldap_destroy_cache			@16
;obsolete	_ldap_flush_cache			@17
;obsolete	_ldap_uncache_entry			@18
	_ldap_compare				@19
	_ldap_delete				@20
	_ldap_result2error			@21
	_ldap_err2string			@22
	_ldap_modify				@23
	_ldap_modrdn				@24
	_ldap_open					@25
	_ldap_first_entry			@26
	_ldap_next_entry			@27
	_ldap_delete_result_entry	@28
	_ldap_add_result_entry		@29
	_ldap_get_dn				@30
	_ldap_dn2ufn				@31
	_ldap_first_attribute		@32
	_ldap_next_attribute		@33
	_ldap_get_values			@34
	_ldap_get_values_len		@35
	_ldap_count_entries			@36
	_ldap_count_values			@37
	_ldap_value_free			@38
	_ldap_explode_dn			@39
	_ldap_result				@40
	_ldap_msgfree				@41
	_ldap_msgdelete				@42
	_ldap_search				@43
	_ldap_add_s					@44
	_ldap_bind_s				@45
	_ldap_unbind_s				@46
	_ldap_delete_s				@47
	_ldap_modify_s				@48
	_ldap_modrdn_s				@49
	_ldap_search_s				@50
	_ldap_search_st				@51
	_ldap_compare_s				@52
	_ldap_ufn_search_c			@53
	_ldap_ufn_search_s			@54
	_ldap_init_getfilter		@55
	_ldap_getfilter_free		@56
	_ldap_getfirstfilter		@57
	_ldap_getnextfilter			@58
	_ldap_simple_bind			@59
	_ldap_simple_bind_s			@60
	_ldap_bind					@61
	_ldap_friendly_name			@62
	_ldap_free_friendlymap		@63
	_ldap_ufn_search_ct			@64
;obsolete	_ldap_set_cache_options		@65
;obsolete	_ldap_uncache_request		@66
	_ldap_modrdn2				@67
	_ldap_modrdn2_s				@68
	_ldap_ufn_setfilter			@69
	_ldap_ufn_setprefix			@70
	_ldap_ufn_timeout			@71
	_ldap_init_getfilter_buf	@72
	_ldap_setfilteraffixes		@73
	_ldap_sort_entries			@74
	_ldap_sort_values			@75
	_ldap_sort_strcasecmp		@76
	_ldap_count_values_len		@77
	_ldap_name2template		@78
	_ldap_value_free_len		@79

; manually comment and uncomment these five as necessary
;obsolete	_ldap_kerberos_bind1		@80
;obsolete	_ldap_kerberos_bind2		@81
;obsolete	_ldap_kerberos_bind_s		@82
;obsolete	_ldap_kerberos_bind1_s		@83
;obsolete	_ldap_kerberos_bind2_s		@84

	_ldap_init			@85
	_ldap_is_dns_dn			@86
	_ldap_explode_dns		@87
	_ldap_mods_free			@88

    	_ldap_is_ldap_url		@89
    	_ldap_free_urldesc		@90
    	_ldap_url_parse			@91
    	_ldap_url_search		@92
    	_ldap_url_search_s		@93
    	_ldap_url_search_st		@94
	_ldap_set_rebind_proc		@95

	_ber_skip_tag				@100
	_ber_peek_tag				@101
	_ber_get_int				@102
	_ber_get_stringb			@103
	_ber_get_stringa			@104
	_ber_get_stringal			@105
	_ber_get_bitstringa			@106
	_ber_get_null				@107
	_ber_get_boolean			@108
	_ber_first_element			@109
	_ber_next_element			@110
	_ber_scanf					@111
	_ber_bvfree					@112
	_ber_bvecfree				@113
	_ber_put_int				@114
	_ber_put_ostring			@115
	_ber_put_string				@116
	_ber_put_bitstring			@117
	_ber_put_null				@118
	_ber_put_boolean			@119
	_ber_start_seq				@120
	_ber_start_set				@121
	_ber_put_seq				@122
	_ber_put_set				@123
	_ber_printf					@124
	_ber_read					@125
	_ber_write					@126
	_ber_free					@127
	_ber_flush					@128
	_ber_alloc					@129
	_ber_dup					@130
	_ber_get_next				@131
	_ber_get_tag				@132
	_ber_put_enum				@133
	_der_alloc					@134
	_ber_alloc_t				@135
	_ber_bvdup				@136
	_ber_init				@137
	_ber_reset				@138
	_ber_bytes_remaining			@139

	_ldap_memfree				@200
	_ldap_ber_free				@201

	_ldap_init_searchprefs		@300
	_ldap_init_searchprefs_buf	@301
	_ldap_free_searchprefs		@302
	_ldap_first_searchobj		@303
	_ldap_next_searchobj		@304
	_ldap_build_filter			@305

	_ldap_init_templates		@400
	_ldap_init_templates_buf	@401
	_ldap_free_templates		@402
	_ldap_first_disptmpl		@403
	_ldap_next_disptmpl			@404
	_ldap_oc2template			@405
	_ldap_tmplattrs				@406
	_ldap_first_tmplrow			@407
	_ldap_next_tmplrow			@408
	_ldap_first_tmplcol			@409
	_ldap_next_tmplcol			@410
	_ldap_entry2text_search		@411
	_ldap_entry2text			@412
	_ldap_vals2text				@413
	_ldap_entry2html			@414
	_ldap_entry2html_search			@415
	_ldap_vals2html				@416
	_ldap_tmplerr2string			@417
	_ldap_set_option			@418
	_ldap_get_option			@419
;obsolete	_mark_select_read			@420
;obsolete	_mark_select_clear			@421
;obsolete	_do_ldap_select				@422
;obsolete	_is_read_ready				@423
;obsolete	_new_select_info			@424
;obsolete	_free_select_info			@425
;obsolete	_connect_to_host			@426
;obsolete	_close_connection			@427
	_ldap_get_lderrno			@430
	_ldap_set_lderrno			@431
	_ldap_perror				@432
	_ldap_set_filter_additions		@433
	_ldap_create_filter			@434
	_ldap_version				@440

