/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

/*

  The interface to global history.

 */

#include "nsISupports.idl"

%{ C++
#include "nscore.h" // for PRUnichar
%}

[scriptable, uuid(9491C383-E3C4-11d2-BDBE-0050040A9B44)]
interface nsIGlobalHistory : nsISupports
{
    // Add a page to the history
    void AddPage(in string aURL, in string aReferrerURL, in long long aDate);

    // Set the title for a page in the global history
    void SetPageTitle(in string aURL, in wstring aTitle);

    // Remove the specified page from the global history
    void RemovePage(in string aURL);

    // Get the URL's last visit date
    long long GetLastVisitDate(in string aURL);

    // Get the preferred completion for aURL
    string GetURLCompletion(in string aURL);

    // get the last page visited
    string GetLastPageVisted();
};

%{ C++
// {9491C382-E3C4-11D2-BDBE-0050040A9B44}
#define NS_GLOBALHISTORY_CID \
{ 0x9491c382, 0xe3c4, 0x11d2, { 0xbd, 0xbe, 0x0, 0x50, 0x4, 0xa, 0x9b, 0x44} }

#define NS_GLOBALHISTORY_CONTRACTID \
    "@mozilla.org/browser/global-history;1"

#define NS_GLOBALHISTORY_DATASOURCE_CONTRACTID \
    "@mozilla.org/rdf/datasource;1?name=history"
%}

