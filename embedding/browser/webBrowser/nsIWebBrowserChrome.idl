/* -*- Mode: IDL; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Mozilla Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/MPL/
 * 
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 * 
 * The Original Code is the Mozilla browser.
 * 
 * The Initial Developer of the Original Code is Netscape
 * Communications, Inc.  Portions created by Netscape are
 * Copyright (C) 1999, Mozilla.  All Rights Reserved.
 * 
 * Contributor(s):
 *   Travis Bogard <travis@netscape.com>
 */

#include "nsISupports.idl"

interface nsIWebBrowser;
interface nsIDocShellTreeItem;

/**
 * The nsIWebBrowserChrome
 */

[scriptable, uuid(BA434C60-9D52-11d3-AFB0-00A024FFC08C)]
interface nsIWebBrowserChrome : nsISupports
{
	const unsigned long STATUS_SCRIPT         = 0x00000001;
	const unsigned long STATUS_SCRIPT_DEFAULT = 0x00000002;
	const unsigned long STATUS_LINK           = 0x00000003;

	/*
	    Called when the status text in the chrome needs to be updated.
		The statusType indicates what is setting the text, the text is nsnull
		when there is no longer any status
    */
	void setStatus(in unsigned long statusType, in wstring status);

	/*
		This is the currently loaded webBrowser.  The browser chrome may be
		told to set the webBrowser object to a new object via setting of this
		attribute.  In this case the implementer is responsible for taking the 
		new webBrowser object and doing any necessary initialization or setup 
		as if it had created the webBrowser itself.  This includes positioning
		setting up listeners etc.
	*/
	attribute nsIWebBrowser webBrowser;

	/*
		Definitions for the chrome flags
	*/
	const unsigned long CHROME_DEFAULT          = 0x00000001;
	const unsigned long CHROME_WINDOW_BORDERS   = 0x00000002;
	const unsigned long CHROME_WINDOW_CLOSE     = 0x00000004;
	const unsigned long CHROME_WINDOW_RESIZE    = 0x00000008;
	const unsigned long CHROME_MENUBAR          = 0x00000010;
	const unsigned long CHROME_TOOLBAR          = 0x00000020;
	const unsigned long CHROME_LOCATIONBAR      = 0x00000040;
	const unsigned long CHROME_STATUSBAR        = 0x00000080;
	const unsigned long CHROME_PERSONAL_TOOLBAR = 0x00000100;
	const unsigned long CHROME_SCROLLBARS       = 0x00000200;
	const unsigned long CHROME_TITLEBAR         = 0x00000400;
	const unsigned long CHROME_EXTRA            = 0x00000800;

    // createBrowserWindow specific flags
    const unsigned long CHROME_WITH_SIZE        = 0x00001000;
    const unsigned long CHROME_WITH_POSITION    = 0x00002000;

	const unsigned long CHROME_WINDOW_RAISED    = 0x02000000;
	const unsigned long CHROME_WINDOW_LOWERED   = 0x04000000;
	const unsigned long CHROME_CENTER_SCREEN    = 0x08000000;
	const unsigned long CHROME_DEPENDENT        = 0x10000000;
	// Note: The modal style bit just affects the way the window looks and does
	//       mean it's actually modal.
	const unsigned long CHROME_MODAL            = 0x20000000; 
	const unsigned long CHROME_OPENAS_DIALOG    = 0x40000000;
	const unsigned long CHROME_OPENAS_CHROME    = 0x80000000;

	const unsigned long CHROME_ALL              = 0x00000ffe;

	/*
		The chrome flags for this browser chrome
	*/
	attribute unsigned long chromeFlags;

	/*
	    Asks the implementer of this interface to create a new webbrowser
		object and return the nsIWebBrowser interface to it. The chrome
		flags indicate the style of the surrounding frame window and
		associated chrome. The position parameters should be ignored
		unless the CHROME_WITH_POSITION chrome flags is set. The size
		parameters should be ignored unless the CHROME_WITH_SIZE chrome flag
		is set. 
	*/

    nsIWebBrowser createBrowserWindow(in unsigned long chromeFlags,
                          in long aX, in long aY, in long aCX, in long aCY);


	/*
	Return the child DocShellTreeItem with the specified name.  To implement this
	an embedder should call nsIDocShellTreeItem::findItemWithName on all the open
	webBrowser objects.  If one succeeds this should be returned.  If they all fail
	simply return null.
	@param aName - This is the name of the item that is trying to be found.
	*/
	nsIDocShellTreeItem findNamedBrowserItem(in wstring aName);

	/*
		Tells the chrome to size itself such that the browser will be the 
		specified size.
	*/
	void sizeBrowserTo(in long aCX, in long aCY);

	/*
		Shows the window as a modal window.
	*/
	void showAsModal();

    /**
        Exit a modal event loop if we're in one. The implementation
        should also exit out of the loop if the window is destroyed.
        @param aStatus - the result code to return from showAsModal
    */
    void exitModalEventLoop(in nsresult aStatus);

	/*
	Sets the persistence of different dimensions of the window.
	*/
	void setPersistence(in boolean persistX, in boolean persistY, 
		in boolean persistCX, in boolean persistCY,
		in boolean persistSizeMode);

	/*
	Gets the current persistence states of the window.
	*/
	void getPersistence(out boolean persistX, out boolean persistY, 
		out boolean persistCX, out boolean persistCY,
		out boolean persistSizeMode);

};
