//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by winEmbed.rc
//
#define IDC_MYICON                      2
#define IDD_WINEMBED_DIALOG             102
#define IDD_ABOUTBOX                    103
#define IDS_APP_TITLE                   103
#define IDM_ABOUT                       104
#define MOZ_OpenURI                     104
#define MOZ_GetURI                      104
#define IDM_EXIT                        105
#define IDS_HELLO                       106
#define IDI_WINEMBED                    107
#define IDI_SMALL                       108
#define IDC_WINEMBED                    109
#define IDR_MAINFRAME                   128
#define MOZ_EDIT_URI                    1001
#define MOZ_Open                        32771
#define MOZ_Print                       32772
#define MOZ_NewBrowser                  32773
#define MOZ_NewEditor                   32774
#define MOZ_Save                        32775
#define MOZ_Cut                         32776
#define MOZ_Copy                        32777
#define MOZ_Paste                       32778
#define MOZ_Delete                      32779
#define MOZ_SelectAll                   32780
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32781
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
