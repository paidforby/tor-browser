/* -*- Mode: IDL; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

/* 
   The nsIProxy interface allows setting and getting of proxy host and port. 
   This is for use by protocol handlers. If you are writing a protocol handler
   and would like to support proxy behaviour then derive from this as well as
   the nsIProtocolHandler class.
   
   -Gagan Saksena 02/25/99
*/

#include "nsISupports.idl"

[scriptable, uuid(0492D011-CD2F-11d2-B013-006097BFC036)]
interface nsIProxy : nsISupports
{
    attribute string proxyHost;
    
    /* -1 on Set call indicates switch to default port */
    attribute long proxyPort;
    
    attribute string proxyType;
};
