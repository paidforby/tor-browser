/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

#include "nsISupports.idl"

/**
 * URIs are essentially structured names for things -- anything.
 * This interface provides accessors to destructure those names.
 *
 * This interface follows Tim Berners-Lee's URI spec:
 *
 *     http://www.w3.org/Addressing/URI/URI_Overview.html
 *  
 * essentially:
 *  
 *      ftp://username:password@hostname:portnumber/pathname
 *      \ /   \               / \      / \        /\       /
 *       -     ---------------   ------   --------  -------
 *       |            |             |        |         |
 *       |            |             |        |        Path
 *       |            |             |       Port         
 *       |            |            Host      /
 *       |         PreHost                  /
 *     Scheme                              /
 *       \                                /
 *        --------------------------------
 *                       |
 *                    PrePath
 *
 * The subclass nsIURL provides a means to open an input or output              
 * stream to a URI as a source/destination, as well as providing additional     
 * accessors to destructure the path, query and reference portions typically    
 * associated with URLs.                                                        
 */

%{C++
#undef GetPort  // XXX Windows!
#undef SetPort  // XXX Windows!
%}

[scriptable, uuid(07a22cc0-0ce5-11d3-9331-00104ba0fd40)]
interface nsIURI : nsISupports
{
    /**
     * Returns a string representation of the URI. Setting the spec 
     * causes the new spec to be parsed, initializing the URI. Setting
     * the spec (or any of the accessors) causes also any currently
     * open streams on the URI's channel to be closed.
     */
    attribute string spec;

    /**
     * The prePath returns the stuff before the path
     * (e.g. protocol://user:password@host:port/). This is useful for
     * authentication, or managing sessions.
     */
    attribute string prePath;

    /**
     * The Scheme is the protocol to which this URI refers. Setting
     * the scheme is a special operation that builds up an equivalent
     * URI string from the new scheme and all the other URI attributes
     * and passes the it to the nsIOService to create a new URI for
     * the new scheme.
     */
    attribute string scheme;

    /**
     * The PreHost portion includes elements like the optional 
     * username:password, or maybe other scheme specific items. 
     */
    attribute string preHost;

    attribute string username;

    attribute string password;

    /**
     * The Host is the internet domain name to which this URI refers. 
     * Note that it could be an IP address as well. 
     */
    attribute string host;

    /**
     * A return value of -1 indicates that no port value is set and the 
     * implementor of the specific scheme will use its default port. 
     * Similarly setting a value of -1 indicates that the default is to be used.
     * Thus as an example:
     *     for HTTP, Port 80 is same as a return value of -1. 
     * However after setting a port (even if its default), the port number will
     * appear in the ToNewCString function.
     */
    attribute long port;

    /**
     * Note that the path includes the leading '/' Thus if no path is 
     * available the Path accessor will return a "/" 
     * For SetPath if none is provided, one would be prefixed to the path. 
     */
    attribute string path;

    /**
     * Note that this comparison is only on char* level. Use 
     * the scheme specific URI to do a more thorough check. For example,
     * in HTTP:
     *     http://foo.com:80 == http://foo.com
     * but this function through nsIURI alone will not return equality
     * for this case.
     */
    boolean equals(in nsIURI other);

    /**
     * Clones the current URI. The newly created URI will be in a closed
     * state even if the underlying channel of the cloned URI is open.
     * Cloning allows the current location to be retained since once the
     * channel is opened the URI may get redirected to a new location.
     */
    nsIURI clone();

    /**
     * This method resolves a relative string into an absolute URI string,
     * using the URI as the base. 
     *
     * This method subsumes the deprecated method nsIIOService::MakeAbsolute.
     */
    string resolve(in string relativePath);

};

%{C++
// Malformed URI Error
#define NS_ERROR_MALFORMED_URI  NS_ERROR_GENERATE_FAILURE(NS_ERROR_MODULE_NETWORK, 10)

/**
 * Protocol writers can obtain a very basic (ok, degenerate) implementation
 * of nsIURI by calling the component manager with NS_SIMPLEURI_CID. The
 * implementation returned will only parse things of the form:
 *
 *    about:cache
 *    \   / \   /
 *     ---   ---
 *      |     |
 *   Scheme  Path
 *
 * where the path is everything after the colon. Note that this is probably 
 * only useful for cases like about: or javascript: URIs. 
 *
 * *** What you most likely will want is NS_STANDARDURL_CID which is much more 
 * full featured. Look at nsIURL.idl for more details.
 */

#define NS_SIMPLEURI_CID                              \
{ /* e0da1d70-2f7b-11d3-8cd0-0060b0fc14a3 */          \
     0xe0da1d70,                                      \
     0x2f7b,                                          \
     0x11d3,                                          \
     {0x8c, 0xd0, 0x00, 0x60, 0xb0, 0xfc, 0x14, 0xa3} \
}

%}
