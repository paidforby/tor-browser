/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

#include "nsISupports.idl"
%{C++
#include "nsFileSpec.h"
%}

interface nsIChannel;
interface nsIEventSinkGetter;
interface nsIInputStream;
interface nsIRunnable;
interface nsIFile;
interface nsIStreamIO;

[scriptable, uuid(57211a60-8c45-11d3-93ac-00104ba0fd40)]
interface nsIFileTransportService : nsISupports
{
    nsIChannel createTransport(in nsIFile file,
                               in long ioFlags,
                               in long perm);

    // This version can be used with an existing input stream to serve
    // as a data pump:
    nsIChannel createTransportFromStream(in string name,
                                         in nsIInputStream fromStream,
                                         in string contentType,
                                         in long contentLength);

    nsIChannel createTransportFromStreamIO(in nsIStreamIO io);

    void dispatchRequest(in nsIRunnable runnable);
    void processPendingRequests();
    void shutdown();

	/**
	 * Total number of transports currently alive
	 */
	readonly attribute unsigned long totalTransportCount;
	/**
	 * A number of transports with I/O operation currently in-progress
	 */
	readonly attribute unsigned long inUseTransportCount;
	/**
	 * A number of transports connected/opened
	 */
	readonly attribute unsigned long connectedTransportCount;
};

%{C++
#define NS_FILETRANSPORTSERVICE_CID                  \
{ /* 2bb2b250-ea35-11d2-931b-00104ba0fd40 */         \
    0x2bb2b250,                                      \
    0xea35,                                          \
    0x11d2,                                          \
    {0x93, 0x1b, 0x00, 0x10, 0x4b, 0xa0, 0xfd, 0x40} \
}

/**
 * Status nsresult codes: used with nsIProgressEventSink::OnStatus 
 */
#define NS_NET_STATUS_READ_FROM NS_ERROR_GENERATE_FAILURE(NS_ERROR_MODULE_NETWORK, 8)
#define NS_NET_STATUS_WROTE_TO  NS_ERROR_GENERATE_FAILURE(NS_ERROR_MODULE_NETWORK, 9)

%}
