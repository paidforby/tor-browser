/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */


#ifndef __UNIX_DNS_H__
#define __UNIX_DNS_H__

#define DNS_SOCK_NAME	"/tmp/netscape_dns_lookup"	//  Name of unix socket


/* The internal status codes that are used; these follow the basic
   SMTP/NNTP model of three-digit codes.
 */
#define DNS_STATUS_GETHOSTBYNAME_OK	101	
#define DNS_STATUS_LOOKUP_OK		102
#define DNS_STATUS_KILLED_OK		103
#define DNS_STATUS_LOOKUP_STARTED	201
#define DNS_STATUS_GETHOSTBYNAME_FAILED 501
#define DNS_STATUS_LOOKUP_FAILED	502
#define DNS_STATUS_LOOKUP_NOT_STARTED	503
#define DNS_STATUS_KILL_FAILED		504
#define DNS_STATUS_UNIMPLEMENTED	601
#define DNS_STATUS_INTERNAL_ERROR	602

#endif /* __UNIX_DNS_H__ */
