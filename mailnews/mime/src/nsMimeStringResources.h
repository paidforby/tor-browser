/*
# The contents of this file are subject to the Netscape Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/NPL/
#
# Software distributed under the License is distributed on an "AS
# IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
# implied. See the License for the specific language governing
# rights and limitations under the License.
#
# The Original Code is mozilla.org code.
#
# The Initial Developer of the Original Code is Netscape
# Communications Corporation. Portions created by Netscape are
# Copyright (C) 1998 Netscape Communications Corporation. All
# Rights Reserved.
#
# Contributor(s): 
*/

#ifndef _NAME_OF_THIS_HEADER_FILE__
#define _NAME_OF_THIS_HEADER_FILE__

#define	MIME_OUT_OF_MEMORY                                 -1000
#define	MIME_UNABLE_TO_OPEN_TMP_FILE                                 -1001
#define	MIME_ERROR_WRITING_FILE                                 -1002
#define	MIME_MHTML_SUBJECT                                  1000
#define	MIME_MHTML_RESENT_COMMENTS                                  1001
#define	MIME_MHTML_RESENT_DATE                                  1002
#define	MIME_MHTML_RESENT_SENDER                                  1003
#define	MIME_MHTML_RESENT_FROM                                  1004
#define	MIME_MHTML_RESENT_TO                                  1005
#define	MIME_MHTML_RESENT_CC                                  1006
#define	MIME_MHTML_DATE                                  1007
#define	MIME_MHTML_SENDER                                  1008
#define	MIME_MHTML_FROM                                  1009
#define	MIME_MHTML_REPLY_TO                                  1010
#define	MIME_MHTML_ORGANIZATION                                  1011
#define	MIME_MHTML_TO                                  1012
#define	MIME_MHTML_CC                                  1013
#define	MIME_MHTML_NEWSGROUPS                                  1014
#define	MIME_MHTML_FOLLOWUP_TO                                  1015
#define	MIME_MHTML_REFERENCES                                  1016
#define	MIME_MHTML_NAME                                  1017
#define	MIME_MHTML_TYPE                                  1018
#define	MIME_MHTML_ENCODING                                  1019
#define	MIME_MHTML_DESCRIPTION                                  1020
#define	MIME_MHTML_MESSAGE_ID                                  1021
#define	MIME_MHTML_RESENT_MESSAGE_ID                                  1022
#define	MIME_MHTML_BCC                                  1023
#define	MIME_MHTML_DOWNLOAD_STATUS_HEADER                                  1024
#define	MIME_MHTML_DOWNLOAD_STATUS_NOT_DOWNLOADED                                  1025
#define	MIME_MSG_LINK_TO_DOCUMENT                                  1026
#define	MIME_MSG_DOCUMENT_INFO                                  1027
#define	MIME_MSG_ATTACHMENT                                  1028
#define	MIME_FORWARDED_MESSAGE_ATTACHMENT                                  1029
#define	MIME_MSG_ADDBOOK_MOUSEOVER_TEXT                                  1030
#define	MIME_MSG_XSENDER_INTERNAL                                  1031
#define	MIME_MSG_X_USER_WROTE                                  1032
#define	MIME_MSG_USER_WROTE                                  1033
#define	MIME_MSG_NO_HEADERS                                  1034
#define	MIME_MSG_SHOW_ATTACHMENT_PANE                                  1035
#define	MIME_MSG_NOT_DOWNLOADED                                       1036
#define MIME_MSG_PARTIAL_FMT_1          1037
#define MIME_MSG_PARTIAL_FMT_2          1038
#define MIME_MSG_PARTIAL_FMT_3          1039

#endif /* _NAME_OF_THIS_HEADER_FILE__ */
