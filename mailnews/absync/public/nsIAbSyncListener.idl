/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

#include "nsISupports.idl"
#include "nsrootidl.idl"
#include "nsIFileSpec.idl"

%{C++
%}

[scriptable, uuid(E0ED29E0-098A-11d4-8FD6-00A024A7D144)]
interface nsIAbSyncListener : nsISupports {

    /**
     * Notify the observer that the AB Sync Authorization operation has begun. 
     *
     */
    void OnStartAuthOperation();

    /**
     * Notify the observer that the AB Sync operation has been completed.  
     * 
     * This method is called regardless of whether the the operation was 
     * successful.
     * 
     *  aTransactionID    - the ID for this particular request
     *  aStatus           - Status code for the sync request
     *  aMsg              - A text string describing the error (if any).
     *  aCookie           - hmmm...cooookies!
     */
    void OnStopAuthOperation(in nsresult aStatus, in wstring aMsg, in string aCookie);

    /**
     * Notify the observer that the AB Sync operation has begun. This method is
     * called only once, at the beginning of a sync transaction
     *
     */
    void OnStartOperation(in PRInt32 aTransactionID, in PRUint32 aMsgSize);

    /**
     * Notify the observer that progress as occurred for the AB Sync operation
     */
    void OnProgress(in PRInt32 aTransactionID, in PRUint32 aProgress, in PRUint32 aProgressMax);

    /**
     * Notify the observer with a status message for sync operation
     */
    void OnStatus(in PRInt32 aTransactionID, in wstring aMsg);

    /**
     * Notify the observer that the AB Sync operation has been completed.  
     * 
     * This method is called regardless of whether the the operation was 
     * successful.
     * 
     *  aTransactionID    - the ID for this particular request
     *  aStatus           - Status code for the sync request
     *  aMsg              - A text string describing the error (if any).
     */
    void OnStopOperation(in PRInt32 aTransactionID, in nsresult aStatus, 
                         in wstring aMsg);
};

