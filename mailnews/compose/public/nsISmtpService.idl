/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

#include "nsISupports.idl"
#include "nsIFileSpec.idl"

interface nsISmtpServer;
interface nsIURI;
interface nsIUrlListener;
interface nsISupportsArray;
interface nsIMsgIdentity;
interface nsIPrompt;

[scriptable, uuid(FBAF0F10-CA9B-11d2-8063-006008128C4E)]
interface nsISmtpService : nsISupports {
  
  ///////////////////////////////////////////////////////////////////////////
  // SendMailMessage requires the file name of the message to send, the
  // sender, a comma delimited list of recipients.
  // It builds an Smtp url, makes an smtp connection and runs the url. If you
  // want a handle on the running task, pass in  a valid nsIURI ptr. You can
  // later interrupt this action by asking the netlib service manager to
  // interrupt the url you are given back. Remember to release aURL when you
  // are done with it. Pass nsnull in for aURL if you don't care about 
  // the returned URL.
  //
  // If you don't care about listening to the url, feel free to pass in
  // nsnull for that argument.
  //
  // You can also pass an SMTP server as an argument if you want to send
  // this message with a specific server.. otherwise it will use the
  // default server
  //////////////////////////////////////////////////////////////////////////

  void SendMailMessage(in nsIFileSpec aFilePath, in string aRecipients, 
                       in nsIMsgIdentity aSenderIdentity,
					   in nsIUrlListener aUrlListener, 
                       in nsISmtpServer aServer,
                       in nsIPrompt aNetPrompt,
                       out nsIURI aURL);

  /**
   * A copy of the array of SMTP servers, as stored in the preferences
   */
  readonly attribute nsISupportsArray smtpServers;

  /**
   * The default server, across sessions of the app
   * (eventually there will be a session default which does not
   * persist past shutdown)
   */
  attribute nsISmtpServer defaultServer;

  /**
   * The "session default" server - this is never saved, and only used
   * for the current session. Always falls back to the default server
   * unless explicitly set.
   */
  attribute nsISmtpServer sessionDefaultServer;

  /**
   * create a new SMTP server.
   * Use this instead of createInstance(), so that the SMTP Service can
   * be aware of this server
   */
  nsISmtpServer createSmtpServer();

  /**
   * find the server with the given hostname.
   * @param hostname the hostname of the server
   * @returns null if no server is found
   */
  nsISmtpServer findServer(in string username, in string hostname);

  /**
   * look up the server with the given key
   * if the server does not exist, create it and add it to our list
   */
  nsISmtpServer getServerByKey(in string key);
  
  /**
   * delete the given server from the server list.
   * does nothing if the server does not exist
   * @param server the server to delete. Use findServer() if you only know
   *   the hostname
   */
  void deleteSmtpServer(in nsISmtpServer server);

};
