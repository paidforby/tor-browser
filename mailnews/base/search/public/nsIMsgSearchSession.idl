/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1999 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

#include "nsISupports.idl"
#include "nsMsgSearchCore.idl"
#include "nsIMsgSearchValue.idl"

interface nsMsgResultElement;
interface nsIMsgSearchAdapter;
interface nsIMsgSearchTerm;
interface nsIMsgSearchNotify;
interface nsIMsgHdr;

//////////////////////////////////////////////////////////////////////////////
// The Msg Search Session is an interface designed to make constructing
// searches easier. Clients typically build up search terms, and then run
// the search
//////////////////////////////////////////////////////////////////////////////

[scriptable, uuid(a819050a-0302-11d3-a50a-0060b0fc04b7)]
interface nsIMsgSearchSession :  nsISupports {

    void AddSearchTerm(in nsMsgSearchAttribValue attrib, /* attribute for this term */
                       in nsMsgSearchOpValue op,/* operator e.g. opContains */
                       in nsIMsgSearchValue value, /* value e.g. "Dogbert" */
                       in boolean BooleanAND, /* set to true if associated boolean operator is AND */
                       in string arbitraryHeader); /* user defined arbitrary header. ignored unless attrib = attribOtherHeader */

    readonly attribute nsISupportsArray searchTerms;

	nsIMsgSearchTerm createTerm ();
    void appendTerm(in nsIMsgSearchTerm term);

	void registerListener (in nsIMsgSearchNotify listener);
	void unregisterListener (in nsIMsgSearchNotify listener);

    readonly attribute unsigned long numSearchTerms;

	readonly attribute nsIMsgSearchAdapter runningAdapter;

    void getNthSearchTerm(in long whichTerm,
                          in nsMsgSearchAttribValue attrib,
                          in nsMsgSearchOpValue op,
                          in nsIMsgSearchValue value); // wrong, should be out

    long countSearchScopes();

    void getNthSearchScope(in long which,out nsMsgSearchScopeValue scopeId, out nsIMsgFolder folder);

	/* add a scope (e.g. a mail folder) to the search */
    void addScopeTerm(in nsMsgSearchScopeValue attrib,
                      in nsIMsgFolder folder);

    void clearScopes();
    
    /* special cases for LDAP since LDAP isn't really a folderInfo */
    [noscript] void AddLdapScope(in nsMsgDIRServer server);
    /* void AddAllLdapScopes(XP_List* dirServerList); */

    /* Call this function everytime the scope changes! It informs the FE if 
       the current scope support custom header use. FEs should not display the
       custom header dialog if custom headers are not supported */
    [noscript] boolean ScopeUsesCustomHeaders(in nsMsgSearchScopeValue scope,
                                   /* could be a folder or server based on scope */
                                   in voidPtr selection,
                                   in boolean forFilters);

    /* use this to determine if your attribute is a string attrib */
    boolean IsStringAttribute(in nsMsgSearchAttribValue attrib);

    /* add all scopes of a given type to the search */
    void AddAllScopes(in nsMsgSearchScopeValue attrib);
    
    void search(in nsIMsgWindow aWindow);
    void interruptSearch();

	// these two methods are used when the search session is using
	// a timer to do local search, and the search adapter needs
	// to run a url (e.g., to reparse a local folder) and wants to
	// pause the timer while running the url. This will fail if the
	// current adapter is not using a timer.
    void pauseSearch();
	void resumeSearch();

    [noscript] readonly attribute voidPtr searchParam;
    readonly attribute nsMsgSearchType searchType;
    
    [noscript] nsMsgSearchType SetSearchParam(in nsMsgSearchType type,
                                              in voidPtr param);

	[noscript] void AddResultElement(in nsMsgResultElement element);

	void addSearchHit(in nsIMsgDBHdr header, in nsIMsgFolder folder);

    readonly attribute long numResults;
	attribute nsIMsgWindow window;
    
    /* these longs are all actually of type nsMsgSearchBooleanOp */
    const long BooleanOR=0;
    const long BooleanAND=1;

};
