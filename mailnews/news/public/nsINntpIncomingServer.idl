/* -*- Mode: IDL; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

#include "nsISupports.idl"
#include "nsIFileSpec.idl"

interface nsIMsgNewsFolder;
interface nsINNTPProtocol;
interface nsIURI;
interface nsIMsgWindow;

[scriptable, uuid(21ea0654-f773-11d2-8aec-004005263078)]
interface nsINntpIncomingServer : nsISupports {
	/* the on-disk path to the newsrc file for this server */
	attribute nsIFileSpec newsrcFilePath;

	/* the newsrc root path (the directories all the newsrc files live) */
	attribute nsIFileSpec newsrcRootPath;
	
	/* ask the user before downloading more than maxArticles? */
	attribute boolean notifyOn;

	/* the max articles to download */
	attribute long maxArticles;

    /* when we don't download all, do we mark the rest read? */
	attribute boolean markOldRead;

	/* abbreviate the newsgroup names in the folder pane? */
	attribute boolean abbreviate;

	/* the server keeps track of all the newsgroups we are subscribed to */
	void addNewsgroup(in string name);
	void removeNewsgroup(in string name);

    void writeNewsrcFile();

    attribute boolean newsrcHasChanged;

	attribute long maximumConnectionsNumber;

	readonly attribute long numGroupsNeedingCounts;
	readonly attribute nsISupports firstGroupNeedingCounts;

	void displaySubscribedGroup(in nsIMsgNewsFolder msgFolder, in long firstMessage, in long lastMessage, in long totalMessages);


	void GetNntpConnection(in nsIURI url, in nsIMsgWindow window,
                                           out nsINNTPProtocol aNntpConnection);
	void RemoveConnection(in nsINNTPProtocol aNntpConnection);

	/* used for auto subscribing */
	boolean containsNewsgroup(in string name);
	void subscribeToNewsgroup(in string name);

	/* used for the subscribe dialog */
	void addNewsgroupToSubscribeDS(in string name);

};
