/* -*- Mode: IDL; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

/*
 * network-oriented interface to newsgroups.
 * This is similar to the old MSG_NewsFolderInfo but is an interface
 * only NNTP uses for talking to the news server and maintaining state.
 *
 */


#include "nsISupports.idl"

%{ C++
#include "nsMsgKeySet.h"
%}

interface nsMsgKeySet;
interface nsINNTPNewsgroupList;

[scriptable, uuid(1A39CD90-ACAF-11d2-B7EE-00805F05FFA5)]
interface nsINNTPNewsgroup : nsISupports {

  attribute string name;
  attribute string prettyName;
  
  attribute boolean needsExtraInfo;
  
  boolean IsOfflineArticle(in long num);

  attribute boolean category;

  attribute boolean subscribed;
  attribute boolean wantNewTotals;
  attribute nsINNTPNewsgroupList newsgroupList;

  void UpdateSummaryFromNNTPInfo(in long oldest,
                                 in long youngest,
                                 in long total_messages);

  void Initialize(in string name,
                  in nsMsgKeySet set,
                  in boolean subscribed);
};


