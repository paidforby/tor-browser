/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

#ifndef nsCalShellCIID_h__
#define nsCalSehllCIID_h__

#include "nsISupports.h"
#include "nsIFactory.h"
#include "nsRepository.h"

//d98368c0-ea8e-11d1-9244-00805f8a7ab6
#define NS_CALENDAR_WIDGET_CID \
{ 0xd98368c0, 0xea8e, 0x11d1, \
{ 0x92, 0x44, 0x00, 0x80, 0x5f, 0x8a, 0x7a, 0xb6 } }

//dffa50d0-ea8e-11d1-9244-00805f8a7ab6
#define NS_CAL_SHELL_CID \
{ 0xdffa50d0, 0xea8e, 0x11d1, \
{ 0x92, 0x44, 0x00, 0x80, 0x5f, 0x8a, 0x7a, 0xb6 } }

//ac62f5d0-3077-11d2-9247-00805f8a7ab6
#define NS_CALENDAR_CONTAINER_CID \
{ 0xac62f5d0, 0x3077, 0x11d2, \
{ 0x92, 0x44, 0x00, 0x80, 0x5f, 0x8a, 0x7a, 0xb6 } }


#endif
