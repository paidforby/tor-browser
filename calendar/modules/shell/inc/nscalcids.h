/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

#ifndef __NS_CALCIDS_H
#define __NS_CALCIDS_H

#include "nsCalUICIID.h"
#include "nsCalUtilCIID.h"
#include "nsCalParserCIID.h"
#include "nsxpfcCIID.h"

static NS_DEFINE_IID(kICalWidgetIID, NS_ICALENDAR_WIDGET_IID);
static NS_DEFINE_IID(kCCalMultiDayViewCID,    NS_CAL_MULTIDAYVIEWCANVAS_CID);
static NS_DEFINE_IID(kCCalMultiUserViewCID,   NS_CAL_MULTIUSERVIEWCANVAS_CID);
static NS_DEFINE_IID(kIXPFCCanvasIID, NS_IXPFC_CANVAS_IID);
static NS_DEFINE_IID(kCXPFCCanvasCID, NS_XPFC_CANVAS_CID);
static NS_DEFINE_IID(kCCalTimeContextCID, NS_CAL_TIME_CONTEXT_CID);
static NS_DEFINE_IID(kCCalComponentCID, NS_CAL_COMPONENT_CID);
static NS_DEFINE_IID(kCCalContextControllerCID, NS_CAL_CONTEXT_CONTROLLER_CID);
static NS_DEFINE_IID(kCLayoutCID, NS_LAYOUT_CID);
static NS_DEFINE_IID(kCBoxLayoutCID, NS_BOXLAYOUT_CID);
static NS_DEFINE_IID(kCCalTimebarContextControllerCID, NS_CAL_TIMEBAR_CONTEXT_CONTROLLER_CID);
static NS_DEFINE_IID(kCCalMonthContextControllerCID, NS_CAL_MONTH_CONTEXT_CONTROLLER_CID);
static NS_DEFINE_IID(kCXPFCCommandCID, NS_XPFC_COMMAND_CID);
static NS_DEFINE_IID(kCXPFCObserverCID, NS_XPFC_OBSERVER_CID);
static NS_DEFINE_IID(kCXPFCSubjectCID, NS_XPFC_SUBJECT_CID);
static NS_DEFINE_IID(kCXPFCObserverManagerCID, NS_XPFC_OBSERVERMANAGER_CID);
static NS_DEFINE_IID(kCXPFCCanvasManagerCID, NS_XPFC_CANVASMANAGER_CID);
static NS_DEFINE_IID(kCCalToolkitCID, NS_CAL_TOOLKIT_CID);
static NS_DEFINE_IID(kCVectorCID, NS_ARRAY_CID);
static NS_DEFINE_IID(kCVectorIteratorCID, NS_ARRAY_ITERATOR_CID);
static NS_DEFINE_IID(kCDateTimeCID, NS_DATETIME_CID);
static NS_DEFINE_IID(kCstackCID, NS_STACK_CID);
static NS_DEFINE_IID(kCDurationCID, NS_DURATION_CID);
static NS_DEFINE_IID(kCCalDurationCommandCID, NS_CAL_DURATION_COMMAND_CID);
static NS_DEFINE_IID(kCCalDayListCommandCID, NS_CAL_DAYLIST_COMMAND_CID);
static NS_DEFINE_IID(kCCalFetchEventsCommandCID, NS_CAL_FETCHEVENTS_COMMAND_CID);
static NS_DEFINE_IID(kCCalNewModelCommandCID, NS_CAL_NEWMODEL_COMMAND_CID);
static NS_DEFINE_IID(kCXPFCMethodInvokerCommandCID, NS_XPFC_METHODINVOKER_COMMAND_CID);
static NS_DEFINE_IID(kCXPFCNotificationStateCommandCID, NS_XPFC_NOTIFICATIONSTATE_COMMAND_CID);
static NS_DEFINE_IID(kCXPFCModelUpdateCommandCID, NS_XPFC_MODELUPDATE_COMMAND_CID);
static NS_DEFINE_IID(kCCalStatusCanvasCID, NS_CAL_STATUSCANVAS_CID);
static NS_DEFINE_IID(kCCalCommandCanvasCID, NS_CAL_COMMANDCANVAS_CID);
static NS_DEFINE_IID(kCalTimebarScaleCID,     NS_CAL_TIMEBARSCALE_CID);
static NS_DEFINE_IID(kCalTimebarHeadingCID,     NS_CAL_TIMEBARHEADING_CID);
static NS_DEFINE_IID(kCalTimebarUserHeadingCID,     NS_CAL_TIMEBARUSERHEADING_CID);
static NS_DEFINE_IID(kCalTimebarTimeHeadingCID,     NS_CAL_TIMEBARTIMEHEADING_CID);
static NS_DEFINE_IID(kCCalDayViewCID,   NS_CAL_DAYVIEWCANVAS_CID);
static NS_DEFINE_IID(kCCalMonthViewCID,   NS_CAL_MONTHVIEWCANVAS_CID);
static NS_DEFINE_IID(kCCalTodoComponentCanvasCID,   NS_CAL_TODOCOMPONENTCANVAS_CID);
static NS_DEFINE_IID(kCCalXMLDTD, NS_ICALXML_DTD_IID);
static NS_DEFINE_IID(kCCalXMLContentSink, NS_CALXMLCONTENTSINK_IID);
static NS_DEFINE_IID(kCCalXPFCDTD, NS_IXPFCXML_DTD_IID);
static NS_DEFINE_IID(kCCalXPFCContentSink, NS_XPFCXMLCONTENTSINK_IID);
static NS_DEFINE_IID(kCXPFCActionCommandCID, NS_XPFC_ACTION_COMMAND_CID);

#endif

