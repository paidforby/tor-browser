/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

#ifndef nsCalParserCIID_h__
#define nsCalParserCIID_h__

#include "nsISupports.h"
#include "nsIFactory.h"
#include "nsRepository.h"

// 13b04870-1ab1-11d2-9246-00805f8a7ab6
#define NS_ICALXML_DTD_IID   \
{ 0x13b04870, 0x1ab1, 0x11d2, \
  {0x92, 0x46, 0x00, 0x80, 0x5f, 0x8a, 0x7a, 0xb6} }

// 610f34d0-1ab1-11d2-9246-00805f8a7ab6
#define NS_CALXMLCONTENTSINK_IID   \
{ 0x610f34d0, 0x1ab1, 0x11d2, \
  {0x92, 0x46, 0x00, 0x80, 0x5f, 0x8a, 0x7a, 0xb6} }


#endif
