/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

#ifndef nscalcoreicalCIID_h__
#define nscalcoreicalCIID_h__

#include "nsISupports.h"
#include "nsIFactory.h"
#include "nsRepository.h"

// 2b502840-646e-11d2-943c-006008268c31
#define NS_CALICALENDARVCALENDAR_CID      \
 { 0x2b502840, 0x646e, 0x11d2, \
   {0x94, 0x3c, 0x00, 0x60, 0x08, 0x26, 0x8c, 0x31} }

//8cda7330-646e-11d2-943c-006008268c31
#define NS_CALICALENDARVEVENT_CID      \
 { 0x8cda7330, 0x646e, 0x11d2, \
   {0x94, 0x3c, 0x00, 0x60, 0x08, 0x26, 0x8c, 0x31} }

//904609a0-646e-11d2-943c-006008268c31
#define NS_CALICALENDARVTODO_CID      \
 { 0x904609a0, 0x646e, 0x11d2, \
   {0x94, 0x3c, 0x00, 0x60, 0x08, 0x26, 0x8c, 0x31} }

//9101fdb0-646e-11d2-943c-006008268c31
#define NS_CALICALENDARVJOURNAL_CID      \
 { 0x9101fdb0, 0x646e, 0x11d2, \
   {0x94, 0x3c, 0x00, 0x60, 0x08, 0x26, 0x8c, 0x31} }

//9177a720-646e-11d2-943c-006008268c31
#define NS_CALICALENDARVFREEBUSY_CID      \
 { 0x9177a720, 0x646e, 0x11d2, \
   {0x94, 0x3c, 0x00, 0x60, 0x08, 0x26, 0x8c, 0x31} }

//91ad22b0-646e-11d2-943c-006008268c31
#define NS_CALICALENDARVTIMEZONE_CID      \
 { 0x91ad22b0, 0x646e, 0x11d2, \
   {0x94, 0x3c, 0x00, 0x60, 0x08, 0x26, 0x8c, 0x31} }

//91d97320-646e-11d2-943c-006008268c31
#define NS_CALICALENDARVALARM_CID      \
 { 0x91d97320, 0x646e, 0x11d2, \
   {0x94, 0x3c, 0x00, 0x60, 0x08, 0x26, 0x8c, 0x31} }

//9205c390-646e-11d2-943c-006008268c31
#define NS_CALDATETIMEPROPERTY_CID \
 { 0x9205c390, 0x646e, 0x11d2, \
   { 0x94, 0x3c, 0x00, 0x60, 0x08, 0x26, 0x8c, 0x31} }

//92352260-646e-11d2-943c-006008268c31
#define NS_CALSTRINGPROPERTY_CID \
 { 0x92352260, 0x646e, 0x11d2, \
   { 0x94, 0x3c, 0x00, 0x60, 0x08, 0x26, 0x8c, 0x31} }

//925cdd40-646e-11d2-943c-006008268c31
#define NS_CALINTEGERPROPERTY_CID \
 { 0x925cdd40, 0x646e, 0x11d2, \
   { 0x94, 0x3c, 0x00, 0x60, 0x08, 0x26, 0x8c, 0x31} }

//9287a680-646e-11d2-943c-006008268c31
#define NS_CALICALENDARTIMEBASEDEVENT_CID      \
 { 0x9287a680, 0x646e, 0x11d2, \
   {0x94, 0x3c, 0x00, 0x60, 0x08, 0x26, 0x8c, 0x31} }

//92b3f6f0-646e-11d2-943c-006008268c31
#define NS_CALATTENDEEPROPERTY_CID \
 { 0x92b3f6f0, 0x646e, 0x11d2, \
   { 0x94, 0x3c, 0x00, 0x60, 0x08, 0x26, 0x8c, 0x31} }

//92dec030-646e-11d2-943c-006008268c31
//9329a060-646e-11d2-943c-006008268c31

#endif




