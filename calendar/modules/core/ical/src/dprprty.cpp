/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- 
 * 
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape 
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

// dprprty.cpp
// John Sun
// 3:15 PM February 12 1998

#include "stdafx.h"
#include "jdefines.h"

#include "dprprty.h"
#include "datetime.h"
#include "prprty.h"
#include "keyword.h"

//---------------------------------------------------------------------
// private never use

DateTimeProperty::DateTimeProperty():
m_DateTime(-1)
{
    PR_ASSERT(FALSE);
}

//---------------------------------------------------------------------

DateTimeProperty::DateTimeProperty(const DateTimeProperty & that)
{
    setParameters(that.m_vParameters);
    m_DateTime = that.m_DateTime;
}

//---------------------------------------------------------------------

DateTimeProperty::DateTimeProperty(DateTime value, JulianPtrArray * parameters)
: StandardProperty(parameters)
{
    //PR_ASSERT(value != 0);
    //m_string = *((UnicodeString *) value);
    m_DateTime = value;
}
//---------------------------------------------------------------------

DateTimeProperty::~DateTimeProperty()
{
    //delete m_DateTime;
    //deleteICalParameterVector(m_vParameters);
    //delete m_vParameters; m_vParameters = 0;
}

//---------------------------------------------------------------------

void * DateTimeProperty::getValue() const
{
    return (void *) &m_DateTime;
}

//---------------------------------------------------------------------
    
void DateTimeProperty::setValue(void * value)
{
    PR_ASSERT(value != 0);
    if (value != 0)
    {
        m_DateTime = *((DateTime *) value);
    }
}

//---------------------------------------------------------------------

ICalProperty * DateTimeProperty::clone(JLog * initLog)
{
    if (initLog) {} // NOTE: Remove later to avoid warnings
    return new DateTimeProperty(*this);   
}

//---------------------------------------------------------------------

t_bool DateTimeProperty::isValid()
{
    //if (m_DateTime == 0)
    //    return FALSE;
    //return m_DateTime->isValid();
    return m_DateTime.isValid();
}

//---------------------------------------------------------------------

UnicodeString & DateTimeProperty::toString(UnicodeString & out)
{

    //PR_ASSERT(m_DateTime != 0);

    //if (m_DateTime->isValid()
        out = m_DateTime.toString();
    //else
    //    out = "NULL";
    return out;
}
//---------------------------------------------------------------------

UnicodeString & 
DateTimeProperty::toString(UnicodeString & dateFmt, UnicodeString & out)
{
    out = m_DateTime.strftime(dateFmt);
    if (out.size() > 0)
        return out;
    else 
        return toString(out);
}

//---------------------------------------------------------------------

UnicodeString & DateTimeProperty::toExportString(UnicodeString & out)
{
    //PR_ASSERT(m_DateTime != 0);
    out = m_DateTime.toISO8601();
    return out;
}
//---------------------------------------------------------------------

UnicodeString & 
DateTimeProperty::toICALString(UnicodeString & out) 
{
    UnicodeString name;
    return toICALString(name, out);
}

//---------------------------------------------------------------------

UnicodeString & 
DateTimeProperty::toICALString(UnicodeString & sProp,
                               UnicodeString & out) 
{
    ICalParameter * aName;
    UnicodeString sVal;
    sVal = toExportString(sVal);
     
    out = "";
    t_int32 size = 0;
    t_int32 i;

    if (m_vParameters != 0)
        size = m_vParameters->GetSize();
    if (sVal.size() > 0)
    {
        UnicodeString u;
        out = sProp;
        for (i = 0; i < size; i++)
        {
            aName = (ICalParameter *) m_vParameters->GetAt(i);
            u = aName->getParameterName(u);

            // don't print the TZID and VALUE parameters
            // thus it will only print in Z time and VALUE assumed to be DATETIME
            if ((u.compareIgnoreCase(nsCalKeyword::Instance()->ms_sTZID) != 0) &&
                (u.compareIgnoreCase(nsCalKeyword::Instance()->ms_sVALUE) != 0))
                out += aName->toICALString(u);

        }
        out += ':'; out += sVal; out += nsCalKeyword::Instance()->ms_sLINEBREAK;
    }
    //if (FALSE) TRACE("out = %s\r\n", out.toCString(""));
    return out;
}

//---------------------------------------------------------------------

