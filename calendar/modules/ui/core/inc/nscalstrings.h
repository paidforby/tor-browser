/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/*
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

#ifndef NS_CALSTRINGS__
#define NS_CALSTRINGS__

#define CAL_STRING_BACKGROUNDCOLOR                "backgroundcolor"
#define CAL_STRING_CLASS                          "class"
#define CAL_STRING_CONTROL                        "control"
#define CAL_STRING_DAY                            "day"
#define CAL_STRING_DEFAULTUSERNAME                "A Default UserName"
#define CAL_STRING_CONTAINER                      "container"
#define CAL_STRING_COUNT                          "count"
#define CAL_STRING_CTX                            "ctx"
#define CAL_STRING_DAY                            "day"
#define CAL_STRING_EAST                           "east"
#define CAL_STRING_DEFAULT                        "default"
#define CAL_STRING_FOREGROUNDCOLOR                "foregroundcolor"
#define CAL_STRING_HEIGHT                         "height"
#define CAL_STRING_HOUR                           "hour"
#define CAL_STRING_PREF_JULIAN_UI_XML_MENUBAR     "julian.ui.xml.menubar"
#define CAL_STRING_PREF_JULIAN_UI_XML_TOOLBAR     "julian.ui.xml.toolbar"
#define CAL_STRING_PREF_JULIAN_UI_XML_CALENDAR    "julian.ui.xml.calendar"
#define CAL_STRING_ID                             "id"
#define CAL_STRING_LABEL                          "label"
#define CAL_STRING_LAYOUT                         "layout"
#define CAL_STRING_MAXREPEAT                      "maxrepeat"
#define CAL_STRING_MINREPEAT                      "minrepeat"
#define CAL_STRING_MINUTE                         "minute"
#define CAL_STRING_MONTH                          "month"
#define CAL_STRING_NAME                           "name"
#define CAL_STRING_NORTH                          "north"
#define CAL_STRING_NORTHWEST                      "northwest"
#define CAL_STRING_ORIENTATION                    "orientation"
#define CAL_STRING_PANEL                          "panel"
#define CAL_STRING_RESOURCE_UI_MENUBAR            "resource://res/ui/menubar.ui"
#define CAL_STRING_RESOURCE_UI_TOOLBAR            "resource://res/ui/toolbar.ui"
#define CAL_STRING_RESOURCE_UI_CALENDAR           "resource://res/ui/trex.ui"
#define CAL_STRING_RULE                           "rule"
#define CAL_STRING_SECOND                         "second"
#define CAL_STRING_SETBACKGROUNDCOLOR             "setbackgroundcolor"
#define CAL_STRING_SOUTH                          "south"
#define CAL_STRING_SOUTHEAST                      "southeast"
#define CAL_STRING_TIMEBAR_USER_HEADING           "timebaruserheading"
#define CAL_STRING_TITLE                          "title"
#define CAL_STRING_TYPE                           "type"
#define CAL_STRING_USECTXRULE                     "usectxrule"
#define CAL_STRING_USEPREFERREDSIZE               "usepreferredsize"
#define CAL_STRING_WEIGHTMAJOR                    "weightmajor"
#define CAL_STRING_WEIGHTMINOR                    "weightminor"
#define CAL_STRING_WEST                           "west"
#define CAL_STRING_WIDTH                          "width"
#define CAL_STRING_XBOX                           "xbox"
#define CAL_STRING_YBOX                           "ybox"
#define CAL_STRING_YEAR                           "year"

#define CAL_STRING_PREF_LOCAL_ADDRESS             "cal.localCalAddress"
#define CAL_STRING_PREF_LOCAL_ADDRESS_DEFAULT     "file://localhost/$USERPROFILE/cal/$USERNAME"
#define CAL_STRING_PREF_PREFERRED_ADDR            "cal.preferredCalAddress"
#define CAL_STRING_PREF_PREFERRED_ADDR_DEFAULT    CAL_STRING_PREF_LOCAL_ADDRESS_DEFAULT
#define CAL_STRING_PREF_USERNAME                  "cal.username"
#define CAL_STRING_PREF_USERNAME_DEFAULT          "$USERNAME"
#define CAL_STRING_PREF_USER_DISPLAY_NAME         "cal.userDisplayName"
#define CAL_STRING_PREF_USER_DISPLAY_NAME_DEFAULT "$USERNAME"

#define CAL_STRING_CMD_MCC                        "mcc"

#endif


