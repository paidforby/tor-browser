/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

#ifndef nsCapiCIID_h__
#define nsCapiCIID_h__

#include "nsISupports.h"
#include "nsIFactory.h"
#include "nsRepository.h"

// b27849f0-41f9-11d2-924a-00805f8a7ab6
#define NS_CAPI_LOCAL_CID   \
{ 0xb27849f0, 0x41f9, 0x11d2, \
  {0x92, 0x4a, 0x00, 0x80, 0x5f, 0x8a, 0x7a, 0xb6} }

// 9e3dc500-41fa-11d2-924a-00805f8a7ab6
#define NS_CAPI_CST_CID   \
{ 0x9e3dc500, 0x41fa, 0x11d2, \
  {0x92, 0x4a, 0x00, 0x80, 0x5f, 0x8a, 0x7a, 0xb6} }


#endif
