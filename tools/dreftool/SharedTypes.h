/*================================================================*
	Copyright � 1998 Rick Gessner. All Rights Reserved.
 *================================================================*/

#ifndef SHARED_TYPES__
#define SHARED_TYPES__



const int   kInterrupted      = 10;
const int   kNoError          = 0;


const unsigned int  kNewLine          = '\n';
const unsigned int  kCR               = '\r';
const unsigned int  kLF               = '\n';
const unsigned int  kTab              = '\t';
const unsigned int  kSpace            = ' ';
const unsigned int  kQuote            = '"';
const unsigned int  kApostrophe       = '\'';
const unsigned int  kLessThan         = '<';
const unsigned int  kGreaterThan      = '>';
const unsigned int  kAmpersand        = '&';
const unsigned int  kForwardSlash     = '/';
const unsigned int  kBackSlash        = '\\';
const unsigned int  kEqual            = '=';
const unsigned int  kMinus            = '-';
const unsigned int  kPlus             = '+';
const unsigned int  kExclamation      = '!';
const unsigned int  kSemicolon        = ';';
const unsigned int  kPoundsign        = '#';
const unsigned int  kAsterisk         = '*';
const unsigned int  kUnderbar         = '_';
const unsigned int  kComma            = ',';
const unsigned int  kLeftParen        = '(';
const unsigned int  kRightParen       = ')';
const unsigned int  kLeftBrace        = '{';
const unsigned int  kRightBrace       = '}';
const unsigned int  kLeftBracket      = '[';
const unsigned int  kRightBracket     = ']';
const unsigned int  kColon            = ':';
const unsigned int  kDot              = '.';
const unsigned int  kVerticalBar      = '|';
const unsigned int  kPercentage       = '%';
const unsigned int  kTilde            = '~';
const unsigned int  kQuestionMark     = '?';
const unsigned int  kCaret            = '^';


#endif


