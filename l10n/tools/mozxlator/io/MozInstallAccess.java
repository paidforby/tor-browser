/*
 * MozInstallReader.java
 *
 * Created on 21. august 2000, 21:28
 */

package org.mozilla.translator.io;

import java.io.*;
/**
 *
 * @author  Henrik Lynggaard
 * @version 
 */
public interface MozInstallAccess 
{
    public void load();
    
    public void save();
    
}
