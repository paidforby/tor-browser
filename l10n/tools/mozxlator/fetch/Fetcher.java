/*
 * Fetcher.java
 *
 * Created on 19. august 2000, 12:59
 */

package org.mozilla.translator.fetch;

import org.mozilla.translator.datamodel.*;
/**
 *
 * @author  Henrik Lynggaard
 * @version 
 */
public interface Fetcher 
{
    public boolean check(Phrase ph);
}

