/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

#ifndef resources_h___
#define resources_h___

#define TIMER_OPEN                40010
#define TIMER_EXIT                40011
#define OPENBLD                   40012

#define BSTNOOPT                  40013
#define BSTOPT                    40014
#define DRAWTEST                  40015
#define FILLTEST                  40016
#define ARCTEST                   40017
#define COMPRESET                 40018
#define COMPTST                   40019
#define COMPTSTSPEED              40020
#define RDMSK                     40021
#define COMPINT                   40022

#endif /* resources_h___ */
