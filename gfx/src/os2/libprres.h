/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is the Mozilla OS/2 libraries.
 *
 * The Initial Developer of the Original Code is John Fairhurst,
 * <john_fairhurst@iname.com>.  Portions created by John Fairhurst are
 * Copyright (C) 1999 John Fairhurst. All Rights Reserved.
 *
 * Contributor(s): 
 *
 */

/* resource definitions for printer dialog */

#ifndef _printres_h
#define _printres_h

#define ID_PRINTER_BASE   100 /* you can change this as needed */

#define IDD_PICKPRINTER   (ID_PRINTER_BASE)
#define IDB_HELP          (ID_PRINTER_BASE + 1)
#define IDLB_QUEUES       (ID_PRINTER_BASE + 2)
#define IDB_JOBPROPERTIES (ID_PRINTER_BASE + 3)
#define IDICO_PRINTER     (ID_PRINTER_BASE + 4)

#endif
