/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 *
 * The contents of this file are subject to the Netscape Public License
 * Version 1.0 (the "NPL"); you may not use this file except in
 * compliance with the NPL.  You may obtain a copy of the NPL at
 * http://www.mozilla.org/NPL/
 *
 * Software distributed under the NPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the NPL
 * for the specific language governing rights and limitations under the
 * NPL.
 *
 * The Initial Developer of this code under the NPL is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation.  All Rights
 * Reserved.
 * 
 * Contributor(s): 
 *   
 */

#ifndef UNIX_MAKEFILE_NO_WORKY

#include "nscore.h"
#include "nsCRT.h"
#include "nsString.h"
#include "nsIURI.h"
#include "nsExternalProtocol.h"
#include "nsXPIDLString.h"


#ifdef NS_DEBUG
#define DEBUG_LOG0( x) printf( x)
#define DEBUG_LOG1( x, y) printf( x, y)
#else
#define DEBUG_LOG0( x)
#define DEBUG_LOG1( x, y)
#endif

#endif

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

nsresult nsExternalProtocol::DefaultLaunch( nsIURI *pUri)
{
    nsresult rv = NS_ERROR_FAILURE;

    return( rv);
}

