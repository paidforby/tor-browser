#
# The contents of this file are subject to the Netscape Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/NPL/
#
# Software distributed under the License is distributed on an "AS
# IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
# implied. See the License for the specific language governing
# rights and limitations under the License.
#
# The Original Code is mozilla.org code.
#
# The Initial Developer of the Original Code is Netscape
# Communications Corporation.  Portions created by Netscape are
# Copyright (C) 1998 Netscape Communications Corporation. All
# Rights Reserved.
#
# Contributor(s): 
#

DEPTH		= ../..
topsrcdir	= @top_srcdir@
srcdir		= @srcdir@
VPATH		= @srcdir@

include $(DEPTH)/config/autoconf.mk

MODULE		= htmlparser
LIBRARY_NAME	= htmlpars
IS_COMPONENT	= 1
SHORT_LIBNAME	= htmlpars

ifeq ($(MOZ_WIDGET_TOOLKIT),os2)
EXTRA_DSO_LIBS	= expat_s xmltok_s 

ifdef MOZ_PERF_METRICS
EXTRA_DSO_LIBS	+= mozutil_s
endif

else
SHARED_LIBRARY_LIBS = \
		$(DIST)/lib/libexpat_s.$(LIB_SUFFIX) \
		$(DIST)/lib/libxmltok_s.$(LIB_SUFFIX) \
		$(NULL)

ifdef MOZ_PERF_METRICS
SHARED_LIBRARY_LIBS += $(DIST)/lib/libmozutil_s.$(LIB_SUFFIX)
endif

endif


CPPSRCS		= \
		nsDTDUtils.cpp \
		nsHTMLTokenizer.cpp \
		nsXMLTokenizer.cpp \
		nsExpatTokenizer.cpp \
		nsElementTable.cpp \
		CNavDTD.cpp \
		COtherDTD.cpp \
		CRtfDTD.cpp \
		nsHTMLEntities.cpp \
		nsHTMLNullSink.cpp \
		nsHTMLTags.cpp \
		nsHTMLTokens.cpp \
		nsLoggingSink.cpp \
		nsParser.cpp \
		CParserContext.cpp \
		nsParserModule.cpp \
		nsParserNode.cpp \
		nsScanner.cpp \
		nsToken.cpp \
		nsTokenHandler.cpp \
		nsHTMLContentSinkStream.cpp \
		nsHTMLToTXTSinkStream.cpp \
		nsValidDTD.cpp \
		nsWellFormedDTD.cpp \
		nsViewSourceHTML.cpp \
		nsXIFDTD.cpp \
		nsExpatDTD.cpp \
		$(NULL)

#
# Disable some DTD debugging code in the parser that 
# breaks on some compilers because of some broken 
# streams code in prstrm.cpp.
#
ifndef MOZ_DISABLE_DTD_DEBUG
CPPSRCS		+= nsDTDDebug.cpp prstrm.cpp
endif

EXPORTS		= \
		nshtmlpars.h \
		nsIContentSink.h \
		nsITokenizer.h \
		nsIExpatTokenizer.h \
		nsIHTMLContentSink.h \
		nsHTMLContentSinkStream.h \
		nsIHTMLFragmentContentSink.h \
		nsHTMLToTXTSinkStream.h \
		nsHTMLEntities.h \
		nsHTMLEntityList.h \
		nsHTMLTags.h \
		nsHTMLTagList.h \
		nsHTMLTokens.h \
		nsILoggingSink.h \
		nsIParserNode.h \
		nsIParser.h \
		nsParser.h \
		nsIDTD.h \
		nsIDTDDebug.h \
		nsIParserFilter.h \
		nsToken.h \
		CNavDTD.h \
		COtherDTD.h \
		nsWellFormedDTD.h \
		nsValidDTD.h \
		CRtfDTD.h \
		nsXIFDTD.h \
		nsParserCIID.h \
		nsExpatDTD.h \
		nsParserError.h \
		nsIElementObserver.h \
		nsIParserService.h \
		$(NULL)

EXPORTS		:= $(addprefix $(srcdir)/, $(EXPORTS))


ifeq ($(MOZ_WIDGET_TOOLKIT),os2)
EXTRA_DSO_LDOPTS += $(EXTRA_DSO_LIBS)
endif

EXTRA_DSO_LDOPTS += $(MOZ_COMPONENT_LIBS)

include $(topsrcdir)/config/rules.mk

DEFINES		+= -D_IMPL_NS_HTMLPARS -DXML_DTD


