/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/*
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

/**
 * MODULE NOTES:
 * @update  jevering 6/17/98
 * 
 * This interface is not yet used; it was intended to allow an observer object
 * to "look at" the i/o stream coming into the parser before, during and after
 * the parser saw it. The intention of this was to allow an observer to modify
 * the stream at various stages.
 */

#ifndef  IPARSERFILTER
#define  IPARSERFILTER

#include "nsISupports.h"

class CToken;

#define NS_IPARSERFILTER_IID     \
  {0x14d6ff0,  0x0610,  0x11d2,  \
  {0x8c, 0x3f, 0x00,    0x80, 0x5f, 0x8a, 0x1d, 0xb7}}


class nsIParserFilter : public nsISupports {
  public:

   NS_DEFINE_STATIC_IID_ACCESSOR(NS_IPARSERFILTER_IID)
      
   NS_IMETHOD RawBuffer(char * buffer, PRUint32 * buffer_length) = 0;

   NS_IMETHOD WillAddToken(CToken & token) = 0;

   NS_IMETHOD ProcessTokens( /* dont know what goes here yet */ void ) = 0;

   NS_IMETHOD Finish() = 0;

};



#endif

