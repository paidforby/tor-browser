/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

#include "Exceptions.h"

#include <stdlib.h>
#include <string.h>
#include <ostream.h>

ostream& operator<<(ostream &s, Exception &e)
{
  return s << e.GetMessage() << '\n';
}

Exception::Exception()
{
  mMessage = (char*)0;
}

Exception::Exception(const char *aMessage)
{
  size_t size = strlen(aMessage) + 1;
  mMessage = new char[size];
  strcpy(mMessage, aMessage);
}

Exception::~Exception()
{
  if (mMessage) {
    delete[] mMessage;
  }
}

Exception::Exception(Exception &aException)
{
  size_t size = strlen(aException.mMessage) + 1;
  mMessage = new char[size];
  strcpy(mMessage, aException.mMessage);
}

void Exception::SetMessage(char *aMessage)
{
  if (mMessage) {
    delete[] mMessage;
    mMessage = (char*)0;
  }

  if (aMessage) {
    size_t size = strlen(aMessage) + 1;
    mMessage = new char[size];
    strcpy(mMessage, aMessage);
  }
}

char* Exception::GetMessage()
{
  return mMessage;
}

AbortParser::AbortParser(char *aFileName, long aLineNumber)
{
  char message[512]; // seems big enough
  char lineNumber[20]; 
  strcpy(message, "File: ");
  strcat(message, aFileName);
  strcat(message, ". Line Number: ");

#if defined(XP_UNIX) || defined(XP_MAC) || defined(XP_BEOS)
  sprintf(lineNumber,"%d",aLineNumber);
#else
  itoa(aLineNumber, lineNumber, 10);
#endif
  strcat(message, lineNumber);

  SetMessage(message);
}

FileNotFoundException::FileNotFoundException(char *aFileName)
{
  char message[512]; // seems big enough
  strcpy(message, "File not found: ");
  strcat(message, aFileName);

  SetMessage(message);
}

CantOpenFileException::CantOpenFileException(char *aFileName)
{
  char message[512]; // seems big enough
  strcpy(message, "Can't open file: ");
  strcat(message, aFileName);

  SetMessage(message);
}
