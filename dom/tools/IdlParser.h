/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

#ifndef _IdlParser_h__
#define _IdlParser_h__

#include "IdlObject.h"
#include "IdlScanner.h"

class IdlSpecification;
class IdlInterface;
class IdlTypedef;
class IdlStruct;
class IdlEnum;
class IdlUnion;
class IdlException;
class IdlAttribute;
class IdlFunction;
class IdlParameter;
class IdlVariable;

class IdlParser {
private:
  IdlScanner *mScanner;

public:
                  IdlParser();
                  ~IdlParser();

  void            Parse(char *aFileName, IdlSpecification &aSpecification);

protected:

  IdlInterface*   ParseInterface(IdlSpecification &aSpecification);
  void            ParseInterfaceBody(IdlSpecification &aSpecification, IdlInterface &aInterface);

  IdlTypedef*     ParseTypedef(IdlSpecification &aSpecification);
  IdlStruct*      ParseStruct(IdlSpecification &aSpecification);
  IdlEnum*        ParseEnum(IdlSpecification &aSpecification);
  IdlUnion*       ParseUnion(IdlSpecification &aSpecification);
  IdlVariable*    ParseConst(IdlSpecification &aSpecification);
  IdlException*   ParseException(IdlSpecification &aSpecification);
  IdlAttribute*   MaybeParseAttribute(IdlSpecification &aSpecification, int aTokenID);
  IdlAttribute*   ParseAttribute(IdlSpecification &aSpecification, int aTokenID, int aIsNoScript=0);
  IdlFunction*    ParseFunction(IdlSpecification &aSpecification, Token *aToken);

  IdlParameter*   ParseFunctionParameter(IdlSpecification &aSpecification);

  void            TrimComments();
  int             TrimCommentsSpecial();

};

#endif // _IdlParser_h__

