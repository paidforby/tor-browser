/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

#include "nsISupports.idl"

[ptr] native nsIAppShellService( nsIAppShellService );
[ptr] native nsICmdLineService( nsICmdLineService );

%{C++
class nsIAppShellService;
class nsICmdLineService;
%}

/*--------------------------- nsIAppShellComponent -----------------------------
| This interface describes the fundamental communications between the          |
| application shell and its dynamically installable "components."  Components  |
| are self-contained bits of browser client application functionality.  They   |
| range in size from the general-purpose "Find dialog" up to the "browser."    |
|                                                                              |
| The interface has three main purposes:                                       |
|   o It provides a means for the app shell to identify and load components    |
|     dynamically.                                                             |
|   o It enables those components to contribute to the browser client          |
|     application User Interface (e.g., by adding menu options, etc.).         |
|   o It gives the app shell (and other components) a way to broadcast         |
|     interesting events to other components.                                  |
|                                                                              |
| [Currently, only the first purpose is exploited.]                            |
|                                                                              |
| All app shell components must:                                               |
|   1. Implement this interface.                                               |
|   2. Publish and implement additional interfaces, as necessary, to           |
|      support component-specific function.  For example, the "find            |
|      component" also implements the nsIFindComponent interface which         |
|      adds functions specific to that component.                              |
|   3. Register (using the Mozilla XPCOM component registration facilities)    |
|      using a ContractID of the form:                                             |
|       @mozilla.org/appshell/component/foo;1                            |
|      where "foo" is a component-specific identifier (which should match the  |
|      component-specific interface name).                                     |
|                                                                              |
| The app shell will instantiate each such component at application start-up   |
| and invoke the components Initialize() member function, passing a pointer    |
| to the app shell and to the command-line service corresponding to the        |
| command-line arguments used to start the app shell.                          |
|                                                                              |
| The component should implement this function as necessary.  Typically, a     |
| component will register interest in an appropriate set of events (e.g.,      |
| browser windows opening).  Some components, such as the browser, will        |
| examine the command line argements and open windows.                         |
|                                                                              |
| Upon return, the app shell will maintain a reference to the component and    |
| notify it when events of interest occur.                                     |
------------------------------------------------------------------------------*/
[scriptable, uuid(a6cf90ed-15b3-11d2-932e-00805f8add32)]
interface nsIAppShellComponent : nsISupports {

    /*------------------------------ Initialize --------------------------------
    | Called at application startup.                                           |
    --------------------------------------------------------------------------*/
    [noscript] void Initialize( in nsIAppShellService anAppShell,
                                in nsICmdLineService args );

    /*------------------------------- Shutdown ---------------------------------
    | Called at application shutdown (if the component had registered as a     |
    | service).                                                                |
    --------------------------------------------------------------------------*/
    void Shutdown();

};

%{C++
#define NS_IAPPSHELLCOMPONENT_CONTRACTID    "@mozilla.org/appshell/component"
#define NS_IAPPSHELLCOMPONENT_CLASSNAME "Mozilla AppShell Component"
#define NS_IAPPSHELLCOMPONENT_KEY       "software/netscape/appshell/components"
%}
