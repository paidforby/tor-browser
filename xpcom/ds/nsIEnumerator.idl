/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

#include "nsISupports.idl"

%{C++
#define NS_ENUMERATOR_FALSE 1
%}

[scriptable, uuid(D1899240-F9D2-11D2-BDD6-000064657374)]
interface nsISimpleEnumerator : nsISupports {
  boolean hasMoreElements();
  nsISupports getNext();
};

/*
 * DO NOT USE THIS INTERFACE.  IT IS HORRIBLY BROKEN, USES NS_COMFALSE
 * AND IS BASICALLY IMPOSSIBLE TO USE CORRECTLY THROUGH PROXIES OR
 * XPCONNECT.  IF YOU SEE NEW USES OF THIS INTERFACE IN CODE YOU ARE
 * REVIEWING, YOU SHOULD INSIST ON nsISimpleEnumerator.
 *
 * DON'T MAKE ME COME OVER THERE.
 */
[scriptable, uuid(ad385286-cbc4-11d2-8cca-0060b0fc14a3)]
interface nsIEnumerator : nsISupports {

  /** First will reset the list. will return NS_FAILED if no items
   */
  void first();

  /** Next will advance the list. will return failed if already at end
   */
  void next();

  /** CurrentItem will return the CurrentItem item it will fail if the 
   *  list is empty
   */
  nsISupports currentItem();

  /** return if the collection is at the end.  that is the beginning following 
   *  a call to Prev and it is the end of the list following a call to next
   */
  void isDone();
};

[uuid(75f158a0-cadd-11d2-8cca-0060b0fc14a3)]
interface nsIBidirectionalEnumerator : nsIEnumerator {

  /** Last will reset the list to the end. will return NS_FAILED if no items
   */
  void last();

  /** Prev will decrement the list. will return failed if already at beginning
   */
  void prev();
};

%{C++
extern "C" NS_COM nsresult
NS_NewEmptyEnumerator(nsISimpleEnumerator** aResult);

// Construct and return an implementation of a "conjoining enumerator." This
// enumerator lets you string together two other enumerators into one sequence.
// The result is an nsIBidirectionalEnumerator, but if either input is not
// also bidirectional, the Last and Prev operations will fail.
extern "C" NS_COM nsresult
NS_NewConjoiningEnumerator(nsIEnumerator* first, nsIEnumerator* second,
                           nsIBidirectionalEnumerator* *aInstancePtrResult);

// Construct and return an implementation of a "union enumerator." This
// enumerator will only return elements that are found in both constituent
// enumerators.
extern "C" NS_COM nsresult
NS_NewUnionEnumerator(nsIEnumerator* first, nsIEnumerator* second,
                      nsIEnumerator* *aInstancePtrResult);

// Construct and return an implementation of an "intersection enumerator." This
// enumerator will return elements that are found in either constituent
// enumerators, eliminating duplicates.
extern "C" NS_COM nsresult
NS_NewIntersectionEnumerator(nsIEnumerator* first, nsIEnumerator* second,
                             nsIEnumerator* *aInstancePtrResult);

%}
