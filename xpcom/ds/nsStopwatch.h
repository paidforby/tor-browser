/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

#ifndef nsStopwatch_h__
#define nsStopwatch_h__

#include "nsIStopwatch.h"

#ifdef  NS_ENABLE_STOPWATCH

#include "nsHashtable.h"

////////////////////////////////////////////////////////////////////////////////

class nsStopwatchService : public nsIStopwatchService
{
public:
    NS_DECL_ISUPPORTS
    NS_DECL_NSIPROPERTIES
    NS_DECL_NSISTOPWATCHSERVICE

    nsStopwatchService();
    virtual ~nsStopwatchService();

    static NS_METHOD
    Create(nsISupports* outer, const nsIID& aIID, void* *aInstancePtr);

    nsresult Init();

protected:
    nsSupportsHashtable         mStopwatches;
};

////////////////////////////////////////////////////////////////////////////////

class nsTimingData {
public:
    nsTimingData()
        : mStartTime(0),
          mTotalTime(0),
          mTotalSquaredTime(0),
          mCount(0) {
    }
    PRIntervalTime              mStartTime;
    double                      mTotalTime;
    double                      mTotalSquaredTime;
    double                      mCount;
};

////////////////////////////////////////////////////////////////////////////////

class nsStopwatch : public nsIStopwatch
{
public:
    NS_DECL_ISUPPORTS
    NS_DECL_NSISTOPWATCH

    nsStopwatch();
    virtual ~nsStopwatch();

    static NS_METHOD
    Create(nsISupports* outer, const nsIID& aIID, void* *aInstancePtr);

    void TimeUnits(double timeInMilliSeconds,
                   double *adjustedValue, const char* *adjustedUnits,
                   double *factor);

protected:
    char*                       mName;
    char*                       mCountUnits;
    PRBool                      mPerThread;
    PRUintn                     mThreadTimingDataIndex;
    nsTimingData                mTimingData;
};

////////////////////////////////////////////////////////////////////////////////

#endif // NS_ENABLE_STOPWATCH

#endif // nsStopwatch_h__
