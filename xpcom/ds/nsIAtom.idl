/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/*
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is Mozilla Communicator client code, 
 * released March 31, 1998. 
 *
 * The Initial Developer of the Original Code is Netscape Communications 
 * Corporation.  Portions created by Netscape are
 * Copyright (C) 1998-1999 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 *     
 */
#include "nsISupports.idl"

interface nsISizeOfHandler;

%{C++
#include "nsAWritableString.h"
%}
[ref] native nsStringRef(nsAWritableString);

[scriptable, uuid(3d1b15b0-93b4-11d1-895b-006008911b81)]
interface nsIAtom : nsISupports
{
  /**
   * Translate the unicode string into the stringbuf.
   */
  [noscript] void ToString(in nsStringRef aString); 
	
  /**
   * Return a pointer to a zero terminated unicode string.
   */
   void GetUnicode([shared, retval] out wstring aResult);

  /**
   * Get the size, in bytes, of the atom.
   */
  PRUint32 SizeOf(in nsISizeOfHandler aHandler);
};


%{C++

/**
 * Find an atom that matches the given iso-latin1 C string. The
 * C string is translated into it's unicode equivalent.
 */
extern NS_COM nsIAtom* NS_NewAtom(const char* isolatin1);

/**
 * Find an atom that matches the given unicode string. The string is assumed
 * to be zero terminated.
 */
extern NS_COM nsIAtom* NS_NewAtom(const PRUnichar* unicode);

/**
 * Find an atom that matches the given string.
 */
extern NS_COM nsIAtom* NS_NewAtom(const nsAReadableString& aString);

/**
 * Return a count of the total number of atoms currently
 * alive in the system.
 */
extern NS_COM nsrefcnt NS_GetNumberOfAtoms(void);

extern NS_COM void NS_PurgeAtomTable(void);

%}
