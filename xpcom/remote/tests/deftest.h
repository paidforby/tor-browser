#define BUF_MAX 500
#define BUF_MIN 100
#define SERVER_IN_RES "serverIn.res"
#define CLIENT_IN_RES "clientIn.res"
#define SERVER_OUT_RES "serverOut.res"
#define CLIENT_OUT_RES "clientOut.res"
#define SERVER_INOUT_RES "serverInOut.res"
#define CLIENT_INOUT_RES "clientInOut.res"
#define SERVER_COMB_RES "serverComb.res"
#define CLIENT_COMB_RES "clientComb.res"
#define IN_FDATA "fileIn.dat"
#define OUT_FDATA "fileOut.dat"
#define INOUT_FDATA "fileInOut.dat"
#define COMB_FDATA "fileComb.dat"
#define USHRT_MIN 555
#define ULONG_MIN 555


