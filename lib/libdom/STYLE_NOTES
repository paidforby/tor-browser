Notes on DOM style support, or: Everything you ever wanted to know
about Perignon, but managed to avoid asking.

Data Structures:
----------------
Style data is represented by sets of disjoint trees, rooted on the
innermost element type (simple selector).  The sets are stored in a
hash table. It's easier to understand with a picture, so here goes.

Legend:
--       ``enclosing''
|        ``sibling''
(=...)   ``rule''

Style data:
H1         { color:blue }
H1 A	   { color:red }
H1 B A	   { color:green }
B A	   { color:purple }
A:link	   { color:orange }
.A	   { color:forclassA }

A -- B  (=color:purple) -- H1 (=color:green)
|    |
|    H1 (=color:red)
|
A:link (=color:orange)
|
|
.A (=color:forclassA)

H1 (=color:blue)

Note that there are no rules on the ``A'' selector.  If the only rule
in the sheet for ``A'' was the ``A:visited'' one, a dummy selector
would be added, since the hash table is keyed on the pseudo-less
selector.

Classes (".H1") and IDs ("#H1") are chained in the sibling list with
their like-named tags.

A selector can correspond to a tag, class or id, depending on the
value of the type field.  If a class selector applies to the magic
``all'' class, it will have no extra field.  Otherwise, the extra
field will be the tag selector.  If a tag selector has an ID as well,
it will be found in the extra field.

JSSS:
-----

Perignon currently depends on JSSS to populate it, but someday I should
implement DOM_StyleParseRule for the heathens that just use CSS.

CSS2:
-----

I don't do CSS2 selectors yet.  I don't yet know how much I care about 
that, because I don't yet know what CSS2 selectors are. =)

Questions:
----------

- Do visited links get A:link properties as well?  They don't appear to in
  the current implementation, but to my mind they should (unless, of course,
  such properties are overridden by A:visited rules).
