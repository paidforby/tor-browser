/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */
/*
** The names of the characters in the ISO Latin1 set.
** Index into this array is the encoding for that character
*/
char *isotab[256] = {
"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
"", "", "", "", "", "", "", "", "", "", "", "", "", "", "space",
"exclam", "quotedbl", "numbersign", "dollar", "percent", "ampersand",
"quoteright", "parenleft", "parenright", "asterisk", "plus", "comma",
"hyphen", "period", "slash", "zero", "one", "two", "three", "four",
"five", "six", "seven", "eight", "nine", "colon", "semicolon", "less",
"equal", "greater", "question", "at", "A", "B", "C", "D", "E", "F",
"G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
"U", "V", "W", "X", "Y", "Z", "bracketleft", "backslash",
"bracketright", "asciicircum", "underscore", "quoteleft", "a", "b",
"c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p",
"q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "braceleft", "bar",
"braceright", "asciitilde", "", "", "", "", "", "", "", "", "", "", "", "",
"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
"", "", "", "space", "exclamdown", "cent", "sterling", "currency",
"yen", "brokenbar", "section", "dieresis", "copyright", "ordfeminine",
"guillemotleft", "logicalnot", "hyphen", "registered", "macron",
"degree", "plusminus", "twosuperior", "threesuperior", "acute", "mu",
"paragraph", "periodcentered", "cedilla", "onesuperior",
"ordmasculine", "guillemotright", "onequarter", "onehalf",
"threequarters", "questiondown", "Agrave", "Aacute", "Acircumflex",
"Atilde", "Adieresis", "Aring", "AE", "Ccedilla", "Egrave", "Eacute",
"Ecircumflex", "Edieresis", "Igrave", "Iacute", "Icircumflex",
"Idieresis", "Eth", "Ntilde", "Ograve", "Oacute", "Ocircumflex",
"Otilde", "Odieresis", "multiply", "Oslash", "Ugrave", "Uacute",
"Ucircumflex", "Udieresis", "Yacute", "Thorn", "germandbls", "agrave",
"aacute", "acircumflex", "atilde", "adieresis", "aring", "ae",
"ccedilla", "egrave", "eacute", "ecircumflex", "edieresis", "igrave",
"iacute", "icircumflex", "idieresis", "eth", "ntilde", "ograve",
"oacute", "ocircumflex", "otilde", "odieresis", "divide", "oslash",
"ugrave", "uacute", "ucircumflex", "udieresis", "yacute", "thorn",
"ydieresis"
};
