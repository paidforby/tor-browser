/* -*- Mode: Java; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

// Stubs for a class that we can't distribute yet.

package netscape.asw;
import java.awt.*;

public class AMDProgressBar extends Canvas {
    public AMDProgressBar() {
    }

    public void reshape (int x, int y, int width, int height) {
    }

    public void update(Graphics g) {
    }

    public void paint(Graphics g) {
    }

    public void setPercent(double percent) {
    }

    public void setText(String text) {
    }

    public void setBoxColors(Color a, Color b) {
    }

    public void setBarColor(Color bar) {
    }

    public void setBackgroundColors(Color top, Color bottom) {
    }

    public void setTextColors(Color text, Color shadow) {
    }

    public void setFont(Font font) {
    }
}

