/* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */
/* 
   BrowserFrame.h -- class definition for the browser frame class
   Created: Spence Murray <spence@netscape.com>, 17-Oct-96.
 */



#ifndef _xfe_browserframe_h
#define _xfe_browserframe_h

#include "Frame.h"
#include "URLBar.h"
#include "Dashboard.h"
#include "xp_core.h"
#include "BrowserDrop.h"
#include <Xm/Xm.h>

class XFE_RDFToolbox;
class XFE_EditorToolbar;

class XFE_BrowserFrame : public XFE_Frame
{
public:
  XFE_BrowserFrame(Widget toplevel, XFE_Frame *parent_frame, Chrome *chromespec);
  virtual ~XFE_BrowserFrame();

  virtual void updateToolbar();
  
  virtual XP_Bool isCommandEnabled(CommandType cmd, void *calldata = NULL,
						 XFE_CommandInfo* = NULL);
  virtual void doCommand(CommandType cmd, void *calldata = NULL,
						 XFE_CommandInfo* = NULL);
  virtual XP_Bool handlesCommand(CommandType cmd, void *calldata = NULL,
						 XFE_CommandInfo* = NULL);
  virtual char *commandToString(CommandType cmd, void *calldata = NULL,
						 XFE_CommandInfo* = NULL);

  virtual int getURL(URL_Struct *url);

  virtual void queryChrome(Chrome * chrome);
  virtual void respectChrome(Chrome * chrome);

  static void bringToFrontOrMakeNew(Widget toplevel);

  virtual XFE_Command* getCommand(CommandType);

#ifdef ENDER
  void showEditorToolbar(XFE_View*);
  void hideEditorToolbar();
#endif /* ENDER */
                                          
private:

  XFE_RDFToolbox *          m_rdfToolbars;
#ifdef ENDER
  XFE_EditorToolbar *			m_editorStyleToolbar;
#endif /* ENDER */

  XFE_BrowserDrop *			m_browserDropSite;

  XP_Bool					m_notification_added;

  static MenuSpec menu_bar_spec[];
  static MenuSpec file_menu_spec[];
  static MenuSpec edit_menu_spec[];
  static MenuSpec view_menu_spec[];
  static MenuSpec go_menu_spec[];
  static MenuSpec encoding_menu_spec[]; 

  //static MenuSpec help_menu_spec[];

  // static MenuSpec file_bookmarks_menu_spec[];
  // static MenuSpec navigate_menu_spec[];

#ifdef ENDER
  void createEditorToolbar();
#endif /* ENDER */

  XFE_CALLBACK_DECL(navigateToURL) // URL_Struct is sent in callData
  XFE_CALLBACK_DECL(newPageLoading) // URL_Struct is sent in callData

  // update the toolbar appearance
  XFE_CALLBACK_DECL(updateToolbarAppearance)

  // Toolbox methods
  virtual void		toolboxItemSnap			(XFE_ToolboxItem * item);
  virtual void		toolboxItemClose		(XFE_ToolboxItem * item);
  virtual void		toolboxItemOpen		(XFE_ToolboxItem * item);
  virtual void		toolboxItemChangeShowing(XFE_ToolboxItem * item);

  virtual void		configureToolbox	();

};

extern "C" MWContext *fe_showBrowser(Widget toplevel, XFE_Frame *parent_frame, Chrome *chromespec, URL_Struct *url);

extern "C" MWContext *fe_reuseBrowser(MWContext *context,
                                      URL_Struct *url);
#endif /* _xfe_browserframe_h */
