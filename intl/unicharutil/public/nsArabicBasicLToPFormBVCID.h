/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */
#ifndef nsArabicBasicLToPFormBVCID_h__
#define nsArabicBasicLToPFormBVCID_h__


#include "nsITextTransform.h"
#include "nsISupports.h"
#include "nscore.h"


// {6A381F92-65F1-11d3-B3C5-00805F8A6670}
#define NS_ARABIC_BASICL_TO_PFORMBV_CID \
{ 0x6a381f92, 0x65f1, 0x11d3, \
  { 0xb3, 0xc5, 0x0, 0x80, 0x5f, 0x8a, 0x66, 0x70} }

#define NS_ARABIC_BASICL_TO_PFORMBV_CONTRACTID NS_TEXTTRANSFORM_CONTRACTID_BASE "ArabicBasicLToPFormBV"


#endif
