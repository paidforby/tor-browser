/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is Mozilla Communicator client code.
 *
 * The Initial Developer of the Original Code is Netscape Communications
 * Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

#ifndef nsUCVJADll_h___
#define nsUCVJADll_h___

#include "prtypes.h"

extern PRInt32 g_InstanceCount;
extern PRInt32 g_LockCount;
extern PRUint16 g_ut0201Mapping[];
extern PRUint16 g_ut0208Mapping[];
extern PRUint16 g_ut0212Mapping[];

extern PRUint16 g_uf0201Mapping[];
extern PRUint16 g_uf0201GLMapping[];
extern PRUint16 g_uf0208Mapping[];
extern PRUint16 g_uf0212Mapping[];

#endif /* nsUCVJADll_h___ */
