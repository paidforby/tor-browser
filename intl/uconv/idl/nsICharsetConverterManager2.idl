/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

#include "nsISupports.idl"
#include "nsISupportsArray.idl"
#include "nsIAtom.idl"
%{ C++
#include "nsIUnicodeDecoder.h"
#include "nsIUnicodeEncoder.h"
#include "nsString.h"
%}

[ptr] native nsDecoderPtr(nsIUnicodeDecoder);
[ptr] native nsEncoderPtr(nsIUnicodeEncoder);
[ptr] native nsStringPtr(nsString);

/**
 * Replacement interface for nsICharsetConverterManager.
 *
 * Here Charsets are indentified by nsIAtom's. I know, we could have our own 
 * interface for charsets (something like nsICharacterSet). But for now, I 
 * will attempt to use Atom's. That is because it requires minimal work, all 
 * the support stuff is already there. The drawback is that we might have some
 * performance loss from going to the Atom engine. Another possible problem is
 * people creating directly the Atom instead of going through GetCharsetAtom()
 * If these problems will hurt us, we'll switch to nsICharacterSet. The 
 * implementation of this interface is ment to be quite flexible.
 *
 * I provide here some nonscriptable "friendly methods". They accept nsString 
 * as params and assign the result to them, freeing the received memory result.
 * These methods are prone to optimisation, in order to elliminate any 
 * allocation when it is not strictly necessary.
 *
 * @created         21/Feb/2000
 * @author  Catalin Rotaru [CATA]
 */
[scriptable, uuid(8BAFE891-E4CC-11d3-9D0D-0050040007B2)]
interface nsICharsetConverterManager2 : nsISupports
{
  /**
   * Get the Unicode decoder for the given charset.
   */
  [noscript] nsDecoderPtr GetUnicodeDecoder([const] in nsIAtom charset);

  /**
   * Get the Unicode encoder for the given charset.
   */
  [noscript] nsEncoderPtr GetUnicodeEncoder([const] in nsIAtom charset);

  /**
   * Get the complete list of available decoders.
   */
  nsISupportsArray GetDecoderList();

  /**
   * Get the complete list of available encoders.
   */
  nsISupportsArray GetEncoderList();

  /**
   * Get the complete list of available charset detectors.
   */
  nsISupportsArray GetCharsetDetectorList();

  /**
   * Get the Atom representing the a given character set. PLEASE USE THIS 
   * METHOD!!! Do not create Atoms directly by going to NS_NewAtom(), because 
   * here we also do alias resolution...
   *
   * Just to let you know, this method will first attempt to resolve this 
   * charset as an alias. If that failed, the original string will be used.
   * Then an atom is created and returned.
   */
  nsIAtom GetCharsetAtom([const] in wstring charset);

  /**
   * Friendlier version.
   */
  nsIAtom GetCharsetAtom2([const] in string charset);

  /**
   * Get the human-readable name for the given charset.
   */
  wstring GetCharsetTitle([const] in nsIAtom charset);

  /**
   * Friendlier but non scriptable version.
   */
  [noscript] void GetCharsetTitle2([const] in nsIAtom charset, 
    in nsStringPtr str);

  /**
   * Get some data about the given charset.
   */
  wstring GetCharsetData([const] in nsIAtom charset, 
    [const] in wstring prop);

  /**
   * Friendlier but non scriptable version.
   */
  [noscript] void GetCharsetData2([const] in nsIAtom charset, 
    [const] in wstring prop, in nsStringPtr str);

  /**
   * Get the language group for the given charset.
   */
  nsIAtom GetCharsetLangGroup([const] in nsIAtom charset);
};
