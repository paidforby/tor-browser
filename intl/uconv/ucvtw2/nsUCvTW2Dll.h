/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is Mozilla Communicator client code.
 *
 * The Initial Developer of the Original Code is Netscape Communications
 * Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

#ifndef nsUCvTW2Dll_h___
#define nsUCvTW2Dll_h___

#include "prtypes.h"

extern "C" PRInt32 g_InstanceCount;
extern "C" PRInt32 g_LockCount;

extern "C" PRUint16 g_ASCIIMappingTable[] ;
extern "C" PRUint16 g_ufCNS1MappingTable[];
extern "C" PRUint16 g_ufCNS2MappingTable[];
extern "C" PRUint16 g_ufCNS3MappingTable[];
extern "C" PRUint16 g_ufCNS4MappingTable[];
extern "C" PRUint16 g_ufCNS5MappingTable[];
extern "C" PRUint16 g_ufCNS6MappingTable[];
extern "C" PRUint16 g_ufCNS7MappingTable[];
extern "C" PRUint16 g_utCNS1MappingTable[];
extern "C" PRUint16 g_utCNS2MappingTable[];
extern "C" PRUint16 g_utCNS3MappingTable[];
extern "C" PRUint16 g_utCNS4MappingTable[];
extern "C" PRUint16 g_utCNS5MappingTable[];
extern "C" PRUint16 g_utCNS6MappingTable[];
extern "C" PRUint16 g_utCNS7MappingTable[];

#endif /* nsUCvTW2Dll_h___ */
