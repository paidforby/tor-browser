/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is Mozilla Communicator client code.
 *
 * The Initial Developer of the Original Code is Netscape Communications
 * Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

#ifndef nsICharsetConverterManager_h___
#define nsICharsetConverterManager_h___

#include "nsString.h"
#include "nsError.h"
#include "nsISupports.h"
#include "nsIAtom.h"
#include "nsIUnicodeEncoder.h"
#include "nsIUnicodeDecoder.h"
#include "nsVoidArray.h"

#define NS_ICHARSETCONVERTERMANAGER_IID \
  {0x3c1c0161, 0x9bd0, 0x11d3, { 0x9d, 0x9, 0x0, 0x50, 0x4, 0x0, 0x7, 0xb2}}

// XXX change to NS_CHARSETCONVERTERMANAGER_CID
#define NS_ICHARSETCONVERTERMANAGER_CID \
  {0x3c1c0163, 0x9bd0, 0x11d3, { 0x9d, 0x9, 0x0, 0x50, 0x4, 0x0, 0x7, 0xb2}}

// XXX change to NS_CHARSETCONVERTERMANAGER_PID
#define NS_CHARSETCONVERTERMANAGER_CONTRACTID "@mozilla.org/charset-converter-manager;1"

#define NS_REGISTRY_UCONV_BASE          "software/netscape/intl/uconv/"
// XXX change "xuconv" to "uconv" when the new enc&dec trees are in place
#define NS_DATA_BUNDLE_REGISTRY_KEY     "software/netscape/intl/xuconv/data/"
#define NS_TITLE_BUNDLE_REGISTRY_KEY    "software/netscape/intl/xuconv/titles/"

#define NS_ERROR_UCONV_NOCONV \
  NS_ERROR_GENERATE_FAILURE(NS_ERROR_MODULE_UCONV, 0x01)


#define NS_IMPL_NSUCONVERTERREGSELF                                     \
static NS_IMETHODIMP                                                    \
nsUConverterRegSelf( const char* aFromCharset,                          \
                     const char* aToCharset,                            \
                     nsCID aCID)                                        \
{                                                                       \
  nsresult res = NS_OK;                                                 \
  nsRegistryKey key;                                                    \
  char buff[1024];                                                      \
  PRBool isOpen = PR_FALSE;                                             \
  NS_WITH_SERVICE( nsIRegistry, registry, NS_REGISTRY_CONTRACTID, &res);    \
  if (NS_FAILED(res))                                                   \
    goto done;                                                          \
  res = registry->IsOpen(&isOpen);                                      \
  if (NS_FAILED(res))                                                   \
    goto done;                                                          \
  if(! isOpen) {                                                        \
    res = registry->OpenWellKnownRegistry(                              \
        nsIRegistry::ApplicationComponentRegistry);                     \
    if (NS_FAILED(res))                                                 \
      goto done;                                                        \
  }                                                                     \
  char * cid_string;                                                    \
  cid_string = aCID.ToString();                                         \
  sprintf(buff, "%s/%s", "software/netscape/intl/uconv", cid_string);   \
  nsCRT::free(cid_string);                                              \
  res = registry -> AddSubtree(nsIRegistry::Common, buff, &key);        \
  if (NS_FAILED(res))                                                   \
    goto done;                                                          \
  res = registry -> SetStringUTF8(key, "source", aFromCharset);             \
  if (NS_FAILED(res))                                                   \
    goto done;                                                          \
  res = registry -> SetStringUTF8(key, "destination", aToCharset);          \
  if (NS_FAILED(res))                                                   \
    goto done;                                                          \
  printf("RegSelf %s to %s converter complete\n",                       \
         aFromCharset, aToCharset);                                     \
done:                                                                   \
  return res;                                                           \
}

#define NS_UCONV_REG_UNREG(_InstanceClass, _From, _To, _CID )         \
static NS_IMETHODIMP                                                  \
_InstanceClass##RegSelf (nsIComponentManager *aCompMgr,               \
                         nsIFile *aPath,                              \
                         const char* registryLocation,                \
                         const char* componentType)                   \
{                                                                     \
   nsCID cid = _CID;                                                  \
   return nsUConverterRegSelf( _From, _To, cid);                      \
}                                                                     \
static NS_IMETHODIMP                                                  \
_InstanceClass##UnRegSelf (nsIComponentManager *aCompMgr,             \
                           nsIFile *aPath,                            \
                           const char* registryLocation)              \
{                                                                     \
  printf("UnRegSelf " _From " to " _To "converter not implement\n");  \
  return NS_OK;                                                       \
}

/**
 * Interface for a Manager of Charset Converters.
 * 
 * This Manager's data is a cache of the stuff available directly through 
 * Registry and Extensible String Bundles. Plus a set of convenient APIs.
 * 
 * Note: The term "Charset" used in the classes, interfaces and file names 
 * should be read as "Coded Character Set". I am saying "charset" only for 
 * length considerations: it is a much shorter word. This convention is for 
 * source-code only, in the attached documents I will be either using the 
 * full expression or I'll specify a different convention.
 *
 * A DECODER converts from a random encoding into Unicode.
 * An ENCODER converts from Unicode into a random encoding.
 * All our data structures and APIs are divided like that.
 * However, when you have a charset data, you may have 3 cases:
 * a) the data is charset-dependet, but it is common for encoders and decoders
 * b) the data is different for the two of them, thus needing different APIs
 * and different "aProp" identifying it.
 * c) the data is relevant only for one: encoder or decoder; its nature making 
 * the distinction.
 *
 * @created         15/Nov/1999
 * @author  Catalin Rotaru [CATA]
 */
class nsICharsetConverterManager : public nsISupports
{
public:
  NS_DEFINE_STATIC_IID_ACCESSOR(NS_ICHARSETCONVERTERMANAGER_IID)

  NS_IMETHOD GetUnicodeEncoder(const nsString * aDest, 
      nsIUnicodeEncoder ** aResult) = 0;
  NS_IMETHOD GetUnicodeDecoder(const nsString * aSrc, 
      nsIUnicodeDecoder ** aResult) = 0;

  NS_IMETHOD GetCharsetLangGroup(nsString * aCharset, nsIAtom ** aResult) = 0;
};

#endif /* nsICharsetConverterManager_h___ */
