/* -*- Mode: Java; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

platform.windows = true;

pref("browser.bookmark_window_showwindow",  1);		// SW_NORMAL
pref("mailnews.folder_window_showwindow",   1);		// SW_NORMAL
pref("mailnews.thread_window_showwindow",   1);		// SW_NORMAL
pref("mailnews.message_window_showwindow",  1);		// SW_NORMAL

pref("browser.bookmark_columns_win",        "");
pref("mailnews.folder_columns_win",         "");
pref("mail.thread_columns_win",             "");
pref("news.thread_columns_win",             "");
pref("category.thread_columns_win",         "");
pref("news.category_columns_win",           "");

pref("ui.key.menuAccessKeyFocuses", true);

pref("font.name.serif.ar", "Times New Roman");
pref("font.name.sans-serif.ar", "Arial");
pref("font.name.monospace.ar", "Courier New");
pref("font.name.cursive.ar", "Comic Sans MS");

pref("font.name.serif.el", "Times New Roman");
pref("font.name.sans-serif.el", "Arial");
pref("font.name.monospace.el", "Courier New");
pref("font.name.cursive.el", "Comic Sans MS");

pref("font.name.serif.he", "Times New Roman");
pref("font.name.sans-serif.he", "Arial");
pref("font.name.monospace.he", "Courier New");
pref("font.name.cursive.he", "Comic Sans MS");

pref("font.name.serif.ja", "ＭＳ Ｐ明朝"); // "MS PMincho"
pref("font.name.sans-serif.ja", "ＭＳ Ｐゴシック"); // "MS PGothic"
pref("font.name.monospace.ja", "ＭＳ ゴシック"); // "MS Gothic"

pref("font.name.serif.ko", "굴림"); // "Gulim" 
pref("font.name.sans-serif.ko", "굴림"); // "Gulim" 
pref("font.name.monospace.ko", "굴림체"); // "GulimChe" 

pref("font.name.serif.th", "Times New Roman");
pref("font.name.sans-serif.th", "Arial");
pref("font.name.monospace.th", "Courier New");
pref("font.name.cursive.th", "Comic Sans MS");

pref("font.name.serif.tr", "Times New Roman");
pref("font.name.sans-serif.tr", "Arial");
pref("font.name.monospace.tr", "Courier New");
pref("font.name.cursive.tr", "Comic Sans MS");

pref("font.name.serif.x-baltic", "Times New Roman");
pref("font.name.sans-serif.x-baltic", "Arial");
pref("font.name.monospace.x-baltic", "Courier New");
pref("font.name.cursive.x-baltic", "Comic Sans MS");

pref("font.name.serif.x-central-euro", "Times New Roman");
pref("font.name.sans-serif.x-central-euro", "Arial");
pref("font.name.monospace.x-central-euro", "Courier New");
pref("font.name.cursive.x-central-euro", "Comic Sans MS");

pref("font.name.serif.x-cyrillic", "Times New Roman");
pref("font.name.sans-serif.x-cyrillic", "Arial");
pref("font.name.monospace.x-cyrillic", "Courier New");
pref("font.name.cursive.x-cyrillic", "Comic Sans MS");

pref("font.name.serif.x-unicode", "Times New Roman");
pref("font.name.sans-serif.x-unicode", "Arial");
pref("font.name.monospace.x-unicode", "Courier New");
pref("font.name.cursive.x-unicode", "Comic Sans MS");

pref("font.name.serif.x-western", "Times New Roman");
pref("font.name.sans-serif.x-western", "Arial");
pref("font.name.monospace.x-western", "Courier New");
pref("font.name.cursive.x-western", "Comic Sans MS");

pref("font.name.serif.zh-CN", "MS Song");
pref("font.name.sans-serif.zh-CN", "MS Hei");
pref("font.name.monospace.zh-CN", "MS Hei");

pref("font.name.serif.zh-TW", "細明體"); // "MingLiU" 
pref("font.name.sans-serif.zh-TW", "細明體"); // "MingLiU" 
pref("font.name.monospace.zh-TW", "細明體"); // "MingLiU" 

pref("font.size.nav4rounding", false);


pref("taskbar.x",                           -1); 
pref("taskbar.y",                           -1);
pref("taskbar.floating",                    true);
pref("taskbar.horizontal",                  false);
pref("taskbar.ontop",                       true);
pref("taskbar.button_style",                -1);

pref("netinst.profile.show_profile_wizard", true); 

//The following pref is internal to Communicator. Please
//do *not* place it in the docs...
pref("netinst.profile.show_dir_overwrite_msg",  true); 
