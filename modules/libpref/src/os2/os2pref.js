/* -*- Mode: Java; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

pref("intl.font2.win.prop_font",        "Tms Rmn");
pref("intl.font2.win.prop_size",        12);
pref("intl.font2.win.fixed_font",       "Courier");
pref("intl.font2.win.fixed_size",       10);

pref("intl.font260.win.prop_font",      "Tms Rmn");
pref("intl.font260.win.prop_size",      10);
pref("intl.font260.win.fixed_font",     "Courier");
pref("intl.font260.win.fixed_size",     10);

pref("intl.font263.win.prop_font",      "Tms Rmn");
pref("intl.font263.win.prop_size",      12);
pref("intl.font263.win.fixed_font",     "Courier");
pref("intl.font263.win.fixed_size",     10);

pref("intl.font1292.win.prop_font",     "Tms Rmn");
pref("intl.font1292.win.prop_size",     12);
pref("intl.font1292.win.fixed_font",    "Courier");
pref("intl.font1292.win.fixed_size",    10);

pref("intl.font264.win.prop_font",      "Tms Rmn");
pref("intl.font264.win.prop_size",      12);
pref("intl.font264.win.fixed_font",     "Courier");
pref("intl.font264.win.fixed_size",     10);

pref("intl.font44.win.prop_font",       "Tms Rmn");
pref("intl.font44.win.prop_size",       12);
pref("intl.font44.win.fixed_font",      "Courier");
pref("intl.font44.win.fixed_size",      10);

pref("intl.font41.win.prop_font",       "Tms Rmn");
pref("intl.font41.win.prop_size",       12);
pref("intl.font41.win.fixed_font",      "Courier");
pref("intl.font41.win.fixed_size",      10);

pref("intl.font43.win.prop_font",       "Tms Rmn");
pref("intl.font43.win.prop_size",       12);
pref("intl.font43.win.fixed_font",      "Courier");
pref("intl.font43.win.fixed_size",      10);

pref("intl.font20.win.prop_font",       "Tms Rmn");
pref("intl.font20.win.prop_size",       12);
pref("intl.font20.win.fixed_font",      "Courier");
pref("intl.font20.win.fixed_size",      10);

pref("intl.font290.win.prop_font",      "Tms Rmn");
pref("intl.font290.win.prop_size",      12);
pref("intl.font290.win.fixed_font",     "Courier");
pref("intl.font290.win.fixed_size",     10);

pref("intl.font254.win.prop_font",      "Tms Rmn");
pref("intl.font254.win.prop_size",      12);
pref("intl.font254.win.fixed_font",     "Courier");
pref("intl.font254.win.fixed_size",     10);

pref("intl.font58.win.mimecharset",     "ibm864");
pref("intl.font58.win.prop_font",       "Tms Rmn");
pref("intl.font58.win.prop_size",       12);
pref("intl.font58.win.fixed_font",      "Courier");
pref("intl.font58.win.fixed_size",      10);

pref("intl.font57.win.mimecharset",     "ibm862");
pref("intl.font57.win.prop_font",       "Tms Rmn");
pref("intl.font57.win.prop_size",       12);
pref("intl.font57.win.fixed_font",      "Courier");
pref("intl.font57.win.fixed_size",      10);

pref("intl.font60.win.mimecharset",     "ibm874");
pref("intl.font60.win.prop_font",       "Tms Rmn");
pref("intl.font60.win.prop_size",       12);
pref("intl.font60.win.fixed_font",      "Courier");
pref("intl.font60.win.fixed_size",      10);
pref("security.enable_java",            false);

