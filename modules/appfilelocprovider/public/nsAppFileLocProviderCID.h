

#ifndef __nsAppFileLocProviderCID_h__

#define __nsAppFileLocProviderCID_h__



#include "nsIDirectoryService.h"



//	{2f977d40-5485-11d4-87e2-0010a4e75ef2} - 

#define NS_APPFILELOCATIONPROVIDER_CID \

{ 0x2f977d40, 0x5485, 0x11d4, { 0x87, 0xe2, 0x00, 0x10, 0xa4, 0xe7, 0x5e, 0xf2 } }

#define NS_APPFILELOCATIONPROVIDER_CONTRACTID \

"@mozilla.org/nsAppFileLocationProvider;1"



#endif /* __nsAppFileLocProviderCID_h__ */

