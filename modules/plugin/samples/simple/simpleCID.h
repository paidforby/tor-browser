/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

#ifndef __SIMPLECID_H
#define __SIMPLECID_H

// {312DA841-3DBF-11d3-BCFB-00A0C9C8D91D}
#define NS_SIMPLEPLUGIN_CID { 0x312da841, 0x3dbf, 0x11d3, { 0xbc, 0xfb, 0x0, 0xa0, 0xc9, 0xc8, 0xd9, 0x1d } }

#endif
