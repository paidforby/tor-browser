/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/*
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

#ifndef NS_XPFCSTRINGS__
#define NS_XPFCSTRINGS__

#define XPFC_STRING_QUESTIONMARK                   "?"
#define XPFC_STRING_BACKGROUNDCOLOR                "backgroundcolor"
#define XPFC_STRING_BACKGROUNDIMAGE                "backgroundimage"
#define XPFC_STRING_BOTTOM                         "bottom"
#define XPFC_STRING_CENTER                         "center"
#define XPFC_STRING_CLASS                          "class"
#define XPFC_STRING_COMMAND                        "command"
#define XPFC_STRING_CONTAINER                      "container"
#define XPFC_STRING_ENABLE                         "enable"
#define XPFC_STRING_FOLDER_CANVAS                  "foldercanvas"
#define XPFC_STRING_FOREGROUNDCOLOR                "foregroundcolor"
#define XPFC_STRING_FULLIMAGE                      "fullimage"
#define XPFC_STRING_FULLHOVERIMAGE                 "fullhoverimage"
#define XPFC_STRING_FULLPRESSEDIMAGE               "fullpressedimage"
#define XPFC_STRING_GETBACKGROUNDCOLOR             "getbackgroundcolor"
#define XPFC_STRING_GETBORDERCOLOR                 "getbordercolor"
#define XPFC_STRING_GETFOREGROUNDCOLOR             "getforegroundcolor"
#define XPFC_STRING_HALIGN                         "halign"
#define XPFC_STRING_HEIGHT                         "height"
#define XPFC_STRING_HELP                           "help"
#define XPFC_STRING_HGAP                           "hgap"
#define XPFC_STRING_HTML_CANVAS                    "htmlcanvas"
#define XPFC_STRING_ID                             "id"
#define XPFC_STRING_KEYBOARDFOCUS                  "keyboardfocus"
#define XPFC_STRING_LABEL                          "label"
#define XPFC_STRING_LAYOUT                         "layout"
#define XPFC_STRING_LEFT                           "left"
#define XPFC_STRING_MINIIMAGE                      "miniimage"
#define XPFC_STRING_MINIHOVERIMAGE                 "minihoverimage"
#define XPFC_STRING_MINIPRESSEDIMAGE               "minipressedimage"
#define XPFC_STRING_MENUCONTAINER                  "menucontainer"
#define XPFC_STRING_NAME                           "name"
#define XPFC_STRING_PANEL                          "panel"
#define XPFC_STRING_RIGHT                          "right"
#define XPFC_STRING_SETBACKGROUNDCOLOR             "setbackgroundcolor"
#define XPFC_STRING_SETBORDERCOLOR                 "setbordercolor"
#define XPFC_STRING_SETFOREGROUNDCOLOR             "setforegroundcolor"
#define XPFC_STRING_SRC                            "src"
#define XPFC_STRING_TABGROUP                       "tabgroup"
#define XPFC_STRING_TABID                          "tabid"
#define XPFC_STRING_TOP                            "top"
#define XPFC_STRING_USEPREFERREDSIZE               "usepreferredsize"
#define XPFC_STRING_VALIGN                         "valign"
#define XPFC_STRING_VGAP                           "vgap"
#define XPFC_STRING_WEIGHTMAJOR                    "weightmajor"
#define XPFC_STRING_WEIGHTMINOR                    "weightminor"
#define XPFC_STRING_WIDTH                          "width"
#define XPFC_STRING_XBOX                           "xbox"
#define XPFC_STRING_XPITEM                         "xpitem"
#define XPFC_STRING_YBOX                           "ybox"
#define XPFC_STRING_ONACTION                       "onaction"

#endif


