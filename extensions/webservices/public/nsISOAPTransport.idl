/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

#include "nsISupports.idl"

interface nsIDOMDocument;
interface nsISOAPTransportListener;

[scriptable, uuid(99ec6695-535f-11d4-9a58-000064657374)]
interface nsISOAPTransport : nsISupports {
  boolean canDoSync();
  nsIDOMDocument syncCall(in string url,
                          in string action,
                          in nsIDOMDocument messageDocument);
  void asyncCall(in string url,
                 in string action,
                 in nsIDOMDocument messageDocument,
                 in nsISOAPTransportListener listener);
  readonly attribute unsigned long status;
};

%{ C++
#define NS_SOAPTRANSPORT_CONTRACTID  \
"@mozilla.org/xmlextras/soap/transport;1"
#define NS_SOAPTRANSPORT_CONTRACTID_PREFIX   NS_SOAPTRANSPORT_CONTRACTID "?protocol="
%}
