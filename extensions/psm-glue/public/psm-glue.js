pref("general.useragent.security",       "U");

pref("security.enable_ssl2",             true);
pref("security.enable_ssl3",             true);
pref("security.default_personal_cert",   "Select Automatically");
pref("security.ask_for_password",        0);
pref("security.password_lifetime",       30);
pref("security.warn_entering_secure",    true);
pref("security.warn_leaving_secure",     true);
pref("security.warn_viewing_mixed",      true);
pref("security.warn_submit_insecure",    true);
