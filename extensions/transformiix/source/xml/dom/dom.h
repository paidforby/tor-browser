/*
 * The contents of this file are subject to the Mozilla Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is Transformiix XSLT processor.
 *
 * Contributor(s):
 *
 * Peter Van der Beken
 *    -- original author
 *
 * $Id: dom.h,v 1.8 2000/07/06 12:35:38 axel%pike.org Exp $
 */

#ifndef MOZ_XSL
#include "standalone/dom.h"
#else
#include "mozImpl/mozilladom.h"
#endif


