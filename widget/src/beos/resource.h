/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */
#define IDC_SELECTANCHOR                4100
#define IDC_ARROWSOUTH                  4101
#define IDC_ARROWNORTH                  4102
#define IDC_ARROWEAST                   4103
#define IDC_ARROWWEST                   4104
#define IDC_ARROWSOUTHPLUS              4105
#define IDC_ARROWNORTHPLUS              4106
#define IDC_ARROWEASTPLUS               4107
#define IDC_ARROWWESTPLUS               4108

