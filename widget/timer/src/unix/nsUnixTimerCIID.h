/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

#ifndef nsUnixTimerCIID_h__
#define nsUnixTimerCIID_h__

#include "nsISupports.h"

// {48B62AD1-48D3-11d3-B224-000064657374}
#define NS_TIMER_MOTIF_CID \
{ 0x48b62ad1, 0x48d3, 0x11d3, \
  { 0xb2, 0x24, 0x0, 0x0, 0x64, 0x65, 0x73, 0x74 } }

// {48B62AD3-48D3-11d3-B224-000064657374}
#define NS_TIMER_XLIB_CID \
{ 0x48b62ad3, 0x48d3, 0x11d3, \
  { 0xb2, 0x24, 0x0, 0x0, 0x64, 0x65, 0x73, 0x74 } }

// {C373F981-5480-11d3-B22A-000064657374}
#define NS_TIMER_QT_CID \
{ 0xc373f981, 0x5480, 0x11d3, \
  { 0xb2, 0x2a, 0x0, 0x0, 0x64, 0x65, 0x73, 0x74 } }

#endif // nsUnixTimerCIID_h__
