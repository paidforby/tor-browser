/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is Mozilla Communicator client code.
 *
 * The Initial Developer of the Original Code is Netscape Communications
 * Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Original Author(s):
 *   Chris Waterson <waterson@netscape.com>
 *
 * Contributor(s): 
 *
 */

/*

  XUL atom declarations.

 */

NS_XULATOM(action);
NS_XULATOM(binding);
NS_XULATOM(bindings);
NS_XULATOM(broadcaster);
NS_XULATOM(browser);
NS_XULATOM(child);
NS_XULATOM2(clazz, "class");
NS_XULATOM(command);
NS_XULATOM(conditions);
NS_XULATOM(container);
NS_XULATOM(containment);
NS_XULATOM(content);
NS_XULATOM(context);
NS_XULATOM(editor);
NS_XULATOM(empty);
NS_XULATOM(flags);
NS_XULATOM(height);
NS_XULATOM(hidden);
NS_XULATOM(id);
NS_XULATOM(iframe);
NS_XULATOM(ignore);
NS_XULATOM(instanceOf);
NS_XULATOM(iscontainer);
NS_XULATOM(isempty);
NS_XULATOM(key);
NS_XULATOM(member);
NS_XULATOM(menu);
NS_XULATOM(menubutton);
NS_XULATOM(menulist);
NS_XULATOM(menupopup);
NS_XULATOM(object);
NS_XULATOM(observes);
NS_XULATOM(open);
NS_XULATOM(parent);
NS_XULATOM(persist);
NS_XULATOM(popup);
NS_XULATOM(popupset);
NS_XULATOM(predicate);
NS_XULATOM(property);
NS_XULATOM(ref);
NS_XULATOM(resource);
NS_XULATOM(rule);
NS_XULATOM(scrollbar);
NS_XULATOM(selected);
NS_XULATOM(style);
NS_XULATOM(subject);
NS_XULATOM(tag);
NS_XULATOM(textnode);
NS_XULATOM(tooltip);
NS_XULATOM(tree);
NS_XULATOM(treecell);
NS_XULATOM(treechildren);
NS_XULATOM(treecol);
NS_XULATOM(treeitem);
NS_XULATOM(triple);
NS_XULATOM(uri);
NS_XULATOM(value);
NS_XULATOM(width);
NS_XULATOM(window);
NS_XULATOM(xulcontentsgenerated);
NS_XULATOM(scrollbox);
NS_XULATOM(left);
NS_XULATOM(top);
