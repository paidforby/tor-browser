/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */

#include "nsISupports.idl"
#include "nsIRDFContainer.idl"
#include "nsIRDFResource.idl"


// Container utilities
[scriptable, uuid(D4214E91-FB94-11D2-BDD8-00104BDE6048)]
interface nsIRDFContainerUtils : nsISupports {
    // Returns 'true' if the property is an RDF ordinal property.
    boolean IsOrdinalProperty(in nsIRDFResource aProperty);

    // Convert the specified index to an ordinal property.
    nsIRDFResource IndexToOrdinalResource(in long aIndex);

    // Convert the specified ordinal property into an index
    long OrdinalResourceToIndex(in nsIRDFResource aOrdinal);

    // Return 'true' if the specified resource is a container
    boolean IsContainer(in nsIRDFDataSource aDataSource, in nsIRDFResource aResource);

    // Return 'true' if the specified resource is a container and it is empty
    boolean IsEmpty(in nsIRDFDataSource aDataSource, in nsIRDFResource aResource);

    // Return 'true' if the specified resource is a bag
    boolean IsBag(in nsIRDFDataSource aDataSource, in nsIRDFResource aResource);

    // Return 'true' if the specified resource is a sequence
    boolean IsSeq(in nsIRDFDataSource aDataSource, in nsIRDFResource aResource);

    // Return 'true' if the specified resource is an alternation
    boolean IsAlt(in nsIRDFDataSource aDataSource, in nsIRDFResource aResource);

    // Decorates the specified resource appropriately to make it
    // usable as an empty bag in the specified data source.
    nsIRDFContainer MakeBag(in nsIRDFDataSource aDataSource, in nsIRDFResource aResource);

    // Decorates the specified resource appropriately to make it
    // usable as an empty sequence in the specified data source.
    nsIRDFContainer MakeSeq(in nsIRDFDataSource aDataSource, in nsIRDFResource aResource);

    // Decorates the specified resource appropriately to make it
    // usable as an empty alternation in the specified data source.
    nsIRDFContainer MakeAlt(in nsIRDFDataSource aDataSource, in nsIRDFResource aResource);
};

%{C++
extern nsresult
NS_NewRDFContainerUtils(nsIRDFContainerUtils** aResult);
%}
