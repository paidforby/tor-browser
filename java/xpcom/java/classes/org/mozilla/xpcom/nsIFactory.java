/*
 * ************* DO NOT EDIT THIS FILE ***********
 *
 * This file was automatically generated from nsIFactory.idl.
 */


package org.mozilla.xpcom;


/**
 * Interface nsIFactory
 *
 * IID: 0x00000001-0000-0000-c000-000000000046
 */

public interface nsIFactory extends nsISupports
{
    public static final String IID =
        "00000001-0000-0000-c000-000000000046";


    /* void createInstance (in nsISupports aOuter, in nsIIDRef iid, [iid_is (iid), retval] out nsQIResult result); */
    public void createInstance(nsISupports aOuter, IID iid, Object[] result);

    /* void lockFactory (in PRBool lock); */
    public void lockFactory(boolean lock);

}

/*
 * end
 */
