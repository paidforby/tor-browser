/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */
#ifndef nsDOMEVENTSIIDs_h___
#define nsDOMEVENTSIIDs_h___

#include "nsISupports.h"

extern const nsIID kIDOMMouseListenerIID;
extern const nsIID kIDOMKeyListenerIID;
extern const nsIID kIDOMMouseMotionListenerIID;
extern const nsIID kIDOMFocusListenerIID;
extern const nsIID kIDOMFormListenerIID;
extern const nsIID kIDOMLoadListenerIID;
extern const nsIID kIDOMDragListenerIID;
extern const nsIID kIDOMPaintListenerIID;
extern const nsIID kIDOMTextListenerIID;
extern const nsIID kIDOMCompositionListenerIID;
extern const nsIID kIDOMMenuListenerIID;
extern const nsIID kIDOMScrollListenerIID;

#endif /* nsDOMEVENTSIIDs_h___ */
