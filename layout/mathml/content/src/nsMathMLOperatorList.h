/*
 *  The contents of this file are subject to the Mozilla Public
 *  License Version 1.1 (the "License"); you may not use this file
 *  except in compliance with the License. You may obtain a copy of
 *  the License at http://www.mozilla.org/MPL/
 *  
 *  Software distributed under the License is distributed on an "AS
 *  IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  rights and limitations under the License.
 *  
 *  The Original Code is Mozilla MathML Project.
 *  
 *  The Initial Developer of the Original Code is The University of
 *  Queensland.  Portions created by The University of Queensland are
 *  Copyright (C) 1999 The University of Queensland.  All
 *  Rights Reserved.
 *  
 *  Contributor(s):
 *    Roger B. Sidje <rbs@maths.uq.edu.au>
 */

/* MathML Operator Dictionary - Auto-generated. Do not edit! */

/* FORMAT
MATHML_OPERATOR(_rank,
                _operator,= string value of the operator
                _flags,   = bitwise: movablelimits|separator|largeop|accent|fence|stretchy|form
                _lspace,  = leftspace in em
                _rspace)  = rightspace in em
*/

#if defined(WANT_MATHML_OPERATOR_COUNT)
  #define NS_MATHML_OPERATOR_COUNT 349
#else
  #define _ , // dirty trick to make the macro handle a variable number of arguments

// short names for a short while
#define INFIX         NS_MATHML_OPERATOR_FORM_INFIX
#define PREFIX        NS_MATHML_OPERATOR_FORM_PREFIX
#define POSTFIX       NS_MATHML_OPERATOR_FORM_POSTFIX
#define STRETCHY      NS_MATHML_OPERATOR_STRETCHY 
#define FENCE         NS_MATHML_OPERATOR_FENCE
#define ACCENT        NS_MATHML_OPERATOR_ACCENT
#define LARGEOP       NS_MATHML_OPERATOR_LARGEOP 
#define SEPARATOR     NS_MATHML_OPERATOR_SEPARATOR 
#define MOVABLELIMITS NS_MATHML_OPERATOR_MOVABLELIMITS

MATHML_OPERATOR(  0,0x0028,PREFIX|STRETCHY|FENCE,0.0f,0.0f) // (
MATHML_OPERATOR(  1,0x0029,POSTFIX|STRETCHY|FENCE,0.0f,0.0f) // )
MATHML_OPERATOR(  2,0x005B,PREFIX|STRETCHY|FENCE,0.0f,0.0f) // [
MATHML_OPERATOR(  3,0x005D,POSTFIX|STRETCHY|FENCE,0.0f,0.0f) // ]
MATHML_OPERATOR(  4,0x007B,PREFIX|STRETCHY|FENCE,0.0f,0.0f) // {
MATHML_OPERATOR(  5,0x007D,POSTFIX|STRETCHY|FENCE,0.0f,0.0f) // }
MATHML_OPERATOR(  6,0x201D,POSTFIX|FENCE,0.0f,0.0f) // &CloseCurlyDoubleQuote;
MATHML_OPERATOR(  7,0x2019,POSTFIX|FENCE,0.0f,0.0f) // &CloseCurlyQuote;
MATHML_OPERATOR(  8,0x3008,PREFIX|STRETCHY|FENCE,0.0f,0.0f) // &LeftAngleBracket;
MATHML_OPERATOR(  9,0xF603,PREFIX|STRETCHY|FENCE,0.0f,0.0f) // &LeftBracketingBar;
MATHML_OPERATOR( 10,0x2308,PREFIX|STRETCHY|FENCE,0.0f,0.0f) // &LeftCeiling;
MATHML_OPERATOR( 11,0x301A,PREFIX|STRETCHY|FENCE,0.0f,0.0f) // &LeftDoubleBracket;
MATHML_OPERATOR( 12,0xF605,PREFIX|STRETCHY|FENCE,0.0f,0.0f) // &LeftDoubleBracketingBar;
MATHML_OPERATOR( 13,0x230A,PREFIX|STRETCHY|FENCE,0.0f,0.0f) // &LeftFloor;
MATHML_OPERATOR( 14,0x201C,PREFIX|FENCE,0.0f,0.0f) // &OpenCurlyDoubleQuote;
MATHML_OPERATOR( 15,0x2018,PREFIX|FENCE,0.0f,0.0f) // &OpenCurlyQuote;
MATHML_OPERATOR( 16,0x3009,POSTFIX|STRETCHY|FENCE,0.0f,0.0f) // &RightAngleBracket;
MATHML_OPERATOR( 17,0xF604,POSTFIX|STRETCHY|FENCE,0.0f,0.0f) // &RightBracketingBar;
MATHML_OPERATOR( 18,0x2309,POSTFIX|STRETCHY|FENCE,0.0f,0.0f) // &RightCeiling;
MATHML_OPERATOR( 19,0x301B,POSTFIX|STRETCHY|FENCE,0.0f,0.0f) // &RightDoubleBracket;
MATHML_OPERATOR( 20,0xF606,POSTFIX|STRETCHY|FENCE,0.0f,0.0f) // &RightDoubleBracketingBar;
MATHML_OPERATOR( 21,0x230B,POSTFIX|STRETCHY|FENCE,0.0f,0.0f) // &RightFloor;
MATHML_OPERATOR( 22,0xE850,PREFIX|FENCE,0.0f,0.0f) // &LeftSkeleton;
MATHML_OPERATOR( 23,0xE851,POSTFIX|FENCE,0.0f,0.0f) // &RightSkeleton;
MATHML_OPERATOR( 24,0xE89C,INFIX|SEPARATOR,0.0f,0.0f) // &InvisibleComma;
MATHML_OPERATOR( 25,0x002C,INFIX|SEPARATOR,0.0f,.33333f) // ,
MATHML_OPERATOR( 26,0xE859,INFIX|STRETCHY,0.0f,0.0f) // &HorizontalLine;
MATHML_OPERATOR( 27,0xE85A,INFIX|STRETCHY,0.0f,0.0f) // &VerticalLine;
MATHML_OPERATOR( 28,0x003B,INFIX|SEPARATOR,0.0f,.27777f) // ;
MATHML_OPERATOR( 29,0x003B,POSTFIX|SEPARATOR,0.0f,0.0f) // ;
MATHML_OPERATOR( 30,0x003A _ 0x003D,INFIX,.27777f,.27777f) // :=
MATHML_OPERATOR( 31,0xE85B,INFIX,.27777f,.27777f) // &Assign;
MATHML_OPERATOR( 32,0x2235,INFIX,.27777f,.27777f) // &Because;
MATHML_OPERATOR( 33,0x2234,INFIX,.27777f,.27777f) // &Therefore;
MATHML_OPERATOR( 34,0xE85C,INFIX|STRETCHY,.27777f,.27777f) // &VerticalSeparator;
MATHML_OPERATOR( 35,0x002F _ 0x002F,INFIX,.27777f,.27777f) // //
MATHML_OPERATOR( 36,0x2237,INFIX,.27777f,.27777f) // &Colon; &Proportion;
MATHML_OPERATOR( 37,0x0026,PREFIX,0.0f,.27777f) // &amp;
MATHML_OPERATOR( 38,0x0026,POSTFIX,.27777f,0.0f) // &amp;
MATHML_OPERATOR( 39,0x002A _ 0x003D,INFIX,.27777f,.27777f) // *=
MATHML_OPERATOR( 40,0x002D _ 0x003D,INFIX,.27777f,.27777f) // -=
MATHML_OPERATOR( 41,0x002B _ 0x003D,INFIX,.27777f,.27777f) // +=
MATHML_OPERATOR( 42,0x002F _ 0x003D,INFIX,.27777f,.27777f) // /=
MATHML_OPERATOR( 43,0x002D _ 0x003E,INFIX,.27777f,.27777f) // ->
MATHML_OPERATOR( 44,0x003A,INFIX,.27777f,.27777f) // :
MATHML_OPERATOR( 45,0x002E _ 0x002E,POSTFIX,.22222f,0.0f) // ..
MATHML_OPERATOR( 46,0x002E _ 0x002E _ 0x002E,POSTFIX,.22222f,0.0f) // ...
MATHML_OPERATOR( 47,0x220B,INFIX,.27777f,.27777f) // &SuchThat; &ReverseElement;
MATHML_OPERATOR( 48,0xE30F,INFIX,.27777f,.27777f) // &DoubleLeftTee;
MATHML_OPERATOR( 49,0x22A8,INFIX,.27777f,.27777f) // &DoubleRightTee;
MATHML_OPERATOR( 50,0x22A4,INFIX,.27777f,.27777f) // &DownTee;
MATHML_OPERATOR( 51,0x22A3,INFIX,.27777f,.27777f) // &LeftTee;
MATHML_OPERATOR( 52,0x22A2,INFIX,.27777f,.27777f) // &RightTee;
MATHML_OPERATOR( 53,0x21D2,INFIX|STRETCHY,.27777f,.27777f) // &Implies; &DoubleRightArrow;
MATHML_OPERATOR( 54,0xF524,INFIX,.27777f,.27777f) // &RoundImplies;
MATHML_OPERATOR( 55,0x007C,INFIX|STRETCHY,.27777f,.27777f) // |
MATHML_OPERATOR( 56,0x007C _ 0x007C,INFIX,.22222f,.22222f) // ||
MATHML_OPERATOR( 57,0xE375,INFIX|STRETCHY,.22222f,.22222f) // &Or;
MATHML_OPERATOR( 58,0x0026 _ 0x0026,INFIX,.27777f,.27777f) // &amp;&amp;
MATHML_OPERATOR( 59,0xE374,INFIX|STRETCHY,.22222f,.22222f) // &And;
MATHML_OPERATOR( 60,0x0026,INFIX,.27777f,.27777f) // &amp;
MATHML_OPERATOR( 61,0x0021,PREFIX,0.0f,.27777f) // !
MATHML_OPERATOR( 62,0xE3AC,PREFIX,0.0f,.27777f) // &Not;
MATHML_OPERATOR( 63,0x2203,PREFIX,0.0f,.27777f) // &Exists;
MATHML_OPERATOR( 64,0x2200,PREFIX,0.0f,.27777f) // &ForAll;
MATHML_OPERATOR( 65,0x2204,PREFIX,0.0f,.27777f) // &NotExists;
MATHML_OPERATOR( 66,0x2208,INFIX,.27777f,.27777f) // &Element;
MATHML_OPERATOR( 67,0x2209,INFIX,.27777f,.27777f) // &NotElement;
MATHML_OPERATOR( 68,0x220C,INFIX,.27777f,.27777f) // &NotReverseElement;
MATHML_OPERATOR( 69,0xE604,INFIX,.27777f,.27777f) // &NotSquareSubset;
MATHML_OPERATOR( 70,0x22E2,INFIX,.27777f,.27777f) // &NotSquareSubsetEqual;
MATHML_OPERATOR( 71,0xE615,INFIX,.27777f,.27777f) // &NotSquareSuperset;
MATHML_OPERATOR( 72,0x22E3,INFIX,.27777f,.27777f) // &NotSquareSupersetEqual;
MATHML_OPERATOR( 73,0x2284,INFIX,.27777f,.27777f) // &NotSubset;
MATHML_OPERATOR( 74,0x2288,INFIX,.27777f,.27777f) // &NotSubsetEqual;
MATHML_OPERATOR( 75,0x2285,INFIX,.27777f,.27777f) // &NotSuperset;
MATHML_OPERATOR( 76,0x2289,INFIX,.27777f,.27777f) // &NotSupersetEqual;
MATHML_OPERATOR( 77,0x228F,INFIX,.27777f,.27777f) // &SquareSubset;
MATHML_OPERATOR( 78,0x2291,INFIX,.27777f,.27777f) // &SquareSubsetEqual;
MATHML_OPERATOR( 79,0x2290,INFIX,.27777f,.27777f) // &SquareSuperset;
MATHML_OPERATOR( 80,0x2292,INFIX,.27777f,.27777f) // &SquareSupersetEqual;
MATHML_OPERATOR( 81,0x22D0,INFIX,.27777f,.27777f) // &Subset;
MATHML_OPERATOR( 82,0x2286,INFIX,.27777f,.27777f) // &SubsetEqual;
MATHML_OPERATOR( 83,0x2283,INFIX,.27777f,.27777f) // &Superset;
MATHML_OPERATOR( 84,0x2287,INFIX,.27777f,.27777f) // &SupersetEqual;
MATHML_OPERATOR( 85,0x21D0,INFIX|STRETCHY,.27777f,.27777f) // &DoubleLeftArrow;
MATHML_OPERATOR( 86,0x21D4,INFIX|STRETCHY,.27777f,.27777f) // &DoubleLeftRightArrow;
MATHML_OPERATOR( 87,0xF50B,INFIX|STRETCHY,.27777f,.27777f) // &DownLeftRightVector;
MATHML_OPERATOR( 88,0xF50E,INFIX|STRETCHY,.27777f,.27777f) // &DownLeftTeeVector;
MATHML_OPERATOR( 89,0x21BD,INFIX|STRETCHY,.27777f,.27777f) // &DownLeftVector;
MATHML_OPERATOR( 90,0xF50C,INFIX|STRETCHY,.27777f,.27777f) // &DownLeftVectorBar;
MATHML_OPERATOR( 91,0xF50F,INFIX|STRETCHY,.27777f,.27777f) // &DownRightTeeVector;
MATHML_OPERATOR( 92,0x21C1,INFIX|STRETCHY,.27777f,.27777f) // &DownRightVector;
MATHML_OPERATOR( 93,0xF50D,INFIX|STRETCHY,.27777f,.27777f) // &DownRightVectorBar;
MATHML_OPERATOR( 94,0x2190,INFIX|STRETCHY,.27777f,.27777f) // &LeftArrow;
MATHML_OPERATOR( 95,0x21E4,INFIX|STRETCHY,.27777f,.27777f) // &LeftArrowBar;
MATHML_OPERATOR( 96,0x21C6,INFIX|STRETCHY,.27777f,.27777f) // &LeftArrowRightArrow;
MATHML_OPERATOR( 97,0x2194,INFIX|STRETCHY,.27777f,.27777f) // &LeftRightArrow;
MATHML_OPERATOR( 98,0xF505,INFIX|STRETCHY,.27777f,.27777f) // &LeftRightVector;
MATHML_OPERATOR( 99,0x21A4,INFIX|STRETCHY,.27777f,.27777f) // &LeftTeeArrow;
MATHML_OPERATOR(100,0xF509,INFIX|STRETCHY,.27777f,.27777f) // &LeftTeeVector;
MATHML_OPERATOR(101,0x21BC,INFIX|STRETCHY,.27777f,.27777f) // &LeftVector;
MATHML_OPERATOR(102,0xF507,INFIX|STRETCHY,.27777f,.27777f) // &LeftVectorBar;
MATHML_OPERATOR(103,0x2199,INFIX|STRETCHY,.27777f,.27777f) // &LowerLeftArrow;
MATHML_OPERATOR(104,0x2198,INFIX|STRETCHY,.27777f,.27777f) // &LowerRightArrow;
MATHML_OPERATOR(105,0x2192,INFIX|STRETCHY,.27777f,.27777f) // &RightArrow;
MATHML_OPERATOR(106,0x21E5,INFIX|STRETCHY,.27777f,.27777f) // &RightArrowBar;
MATHML_OPERATOR(107,0x21C4,INFIX|STRETCHY,.27777f,.27777f) // &RightArrowLeftArrow;
MATHML_OPERATOR(108,0x21A6,INFIX|STRETCHY,.27777f,.27777f) // &RightTeeArrow;
MATHML_OPERATOR(109,0xF50A,INFIX|STRETCHY,.27777f,.27777f) // &RightTeeVector;
MATHML_OPERATOR(110,0x21C0,INFIX|STRETCHY,.27777f,.27777f) // &RightVector;
MATHML_OPERATOR(111,0xF508,INFIX|STRETCHY,.27777f,.27777f) // &RightVectorBar;
MATHML_OPERATOR(112,0xE233,INFIX,.27777f,.27777f) // &ShortLeftArrow;
MATHML_OPERATOR(113,0xE232,INFIX,.27777f,.27777f) // &ShortRightArrow;
MATHML_OPERATOR(114,0x2196,INFIX|STRETCHY,.27777f,.27777f) // &UpperLeftArrow;
MATHML_OPERATOR(115,0x2197,INFIX|STRETCHY,.27777f,.27777f) // &UpperRightArrow;
MATHML_OPERATOR(116,0x003D,INFIX,.27777f,.27777f) // =
MATHML_OPERATOR(117,0x003C,INFIX,.27777f,.27777f) // &lt;
MATHML_OPERATOR(118,0x003E,INFIX,.27777f,.27777f) // >
MATHML_OPERATOR(119,0x0021 _ 0x003D,INFIX,.27777f,.27777f) // !=
MATHML_OPERATOR(120,0x003D _ 0x003D,INFIX,.27777f,.27777f) // ==
MATHML_OPERATOR(121,0x003C _ 0x003D,INFIX,.27777f,.27777f) // &lt;=
MATHML_OPERATOR(122,0x003E _ 0x003D,INFIX,.27777f,.27777f) // >=
MATHML_OPERATOR(123,0x2261,INFIX,.27777f,.27777f) // &Congruent;
MATHML_OPERATOR(124,0x224D,INFIX,.27777f,.27777f) // &CupCap;
MATHML_OPERATOR(125,0x2250,INFIX,.27777f,.27777f) // &DotEqual;
MATHML_OPERATOR(126,0x2225,INFIX,.27777f,.27777f) // &DoubleVerticalBar;
MATHML_OPERATOR(127,0xF431,INFIX,.27777f,.27777f) // &Equal;
MATHML_OPERATOR(128,0x2242,INFIX,.27777f,.27777f) // &EqualTilde;
MATHML_OPERATOR(129,0x21CC,INFIX|STRETCHY,.27777f,.27777f) // &Equilibrium;
MATHML_OPERATOR(130,0x2265,INFIX,.27777f,.27777f) // &GreaterEqual;
MATHML_OPERATOR(131,0x22DB,INFIX,.27777f,.27777f) // &GreaterEqualLess;
MATHML_OPERATOR(132,0x2267,INFIX,.27777f,.27777f) // &GreaterFullEqual;
MATHML_OPERATOR(133,0xE2F7,INFIX,.27777f,.27777f) // &GreaterGreater;
MATHML_OPERATOR(134,0x2277,INFIX,.27777f,.27777f) // &GreaterLess;
MATHML_OPERATOR(135,0xE2F6,INFIX,.27777f,.27777f) // &GreaterSlantEqual;
MATHML_OPERATOR(136,0x2273,INFIX,.27777f,.27777f) // &GreaterTilde;
MATHML_OPERATOR(137,0x224E,INFIX,.27777f,.27777f) // &HumpDownHump;
MATHML_OPERATOR(138,0x224F,INFIX,.27777f,.27777f) // &HumpEqual;
MATHML_OPERATOR(139,0x22B2,INFIX,.27777f,.27777f) // &LeftTriangle;
MATHML_OPERATOR(140,0xF410,INFIX,.27777f,.27777f) // &LeftTriangleBar;
MATHML_OPERATOR(141,0x22B4,INFIX,.27777f,.27777f) // &LeftTriangleEqual;
MATHML_OPERATOR(142,0x2264,INFIX,.27777f,.27777f) // &le;
MATHML_OPERATOR(143,0x22DA,INFIX,.27777f,.27777f) // &LessEqualGreater;
MATHML_OPERATOR(144,0x2266,INFIX,.27777f,.27777f) // &LessFullEqual;
MATHML_OPERATOR(145,0x2276,INFIX,.27777f,.27777f) // &LessGreater;
MATHML_OPERATOR(146,0xE2FB,INFIX,.27777f,.27777f) // &LessLess;
MATHML_OPERATOR(147,0xE2FA,INFIX,.27777f,.27777f) // &LessSlantEqual;
MATHML_OPERATOR(148,0x2272,INFIX,.27777f,.27777f) // &LessTilde;
MATHML_OPERATOR(149,0x226B,INFIX,.27777f,.27777f) // &NestedGreaterGreater;
MATHML_OPERATOR(150,0x226A,INFIX,.27777f,.27777f) // &NestedLessLess;
MATHML_OPERATOR(151,0x2262,INFIX,.27777f,.27777f) // &NotCongruent;
MATHML_OPERATOR(152,0x226D,INFIX,.27777f,.27777f) // &NotCupCap;
MATHML_OPERATOR(153,0x2226,INFIX,.27777f,.27777f) // &NotDoubleVerticalBar;
MATHML_OPERATOR(154,0x2260,INFIX,.27777f,.27777f) // &NotEqual;
MATHML_OPERATOR(155,0xE84E,INFIX,.27777f,.27777f) // &NotEqualTilde;
MATHML_OPERATOR(156,0x226F,INFIX,.27777f,.27777f) // &NotGreater;
MATHML_OPERATOR(157,0xE2A6,INFIX,.27777f,.27777f) // &NotGreaterEqual;
MATHML_OPERATOR(158,0x2270,INFIX,.27777f,.27777f) // &NotGreaterFullEqual; &NotLessSlantEqual;
MATHML_OPERATOR(159,0xE2CC,INFIX,.27777f,.27777f) // &NotGreaterGreater;
MATHML_OPERATOR(160,0x2279,INFIX,.27777f,.27777f) // &NotGreaterLess;
MATHML_OPERATOR(161,0x2271,INFIX,.27777f,.27777f) // &NotGreaterSlantEqual;
MATHML_OPERATOR(162,0x2275,INFIX,.27777f,.27777f) // &NotGreaterTilde;
MATHML_OPERATOR(163,0xE616,INFIX,.27777f,.27777f) // &NotHumpDownHump;
MATHML_OPERATOR(164,0xE84D,INFIX,.27777f,.27777f) // &NotHumpEqual;
MATHML_OPERATOR(165,0x22EA,INFIX,.27777f,.27777f) // &NotLeftTriangle;
MATHML_OPERATOR(166,0xF412,INFIX,.27777f,.27777f) // &NotLeftTriangleBar;
MATHML_OPERATOR(167,0x22EC,INFIX,.27777f,.27777f) // &NotLeftTriangleEqual;
MATHML_OPERATOR(168,0x226E,INFIX,.27777f,.27777f) // &NotLess;
MATHML_OPERATOR(169,0xE2A7,INFIX,.27777f,.27777f) // &NotLessEqual;
MATHML_OPERATOR(170,0x2278,INFIX,.27777f,.27777f) // &NotLessGreater;
MATHML_OPERATOR(171,0xE2CB,INFIX,.27777f,.27777f) // &NotLessLess;
MATHML_OPERATOR(172,0x2274,INFIX,.27777f,.27777f) // &NotLessTilde;
MATHML_OPERATOR(173,0xF428,INFIX,.27777f,.27777f) // &NotNestedGreaterGreater;
MATHML_OPERATOR(174,0xF423,INFIX,.27777f,.27777f) // &NotNestedLessLess;
MATHML_OPERATOR(175,0x2280,INFIX,.27777f,.27777f) // &NotPrecedes;
MATHML_OPERATOR(176,0xE5DC,INFIX,.27777f,.27777f) // &NotPrecedesEqual;
MATHML_OPERATOR(177,0x22E0,INFIX,.27777f,.27777f) // &NotPrecedesSlantEqual;
MATHML_OPERATOR(178,0x22EB,INFIX,.27777f,.27777f) // &NotRightTriangle;
MATHML_OPERATOR(179,0xF413,INFIX,.27777f,.27777f) // &NotRightTriangleBar;
MATHML_OPERATOR(180,0x22ED,INFIX,.27777f,.27777f) // &NotRightTriangleEqual;
MATHML_OPERATOR(181,0x2281,INFIX,.27777f,.27777f) // &NotSucceeds;
MATHML_OPERATOR(182,0xE5F1,INFIX,.27777f,.27777f) // &NotSucceedsEqual;
MATHML_OPERATOR(183,0x22E1,INFIX,.27777f,.27777f) // &NotSucceedsSlantEqual;
MATHML_OPERATOR(184,0xE837,INFIX,.27777f,.27777f) // &NotSucceedsTilde;
MATHML_OPERATOR(185,0x2241,INFIX,.27777f,.27777f) // &NotTilde;
MATHML_OPERATOR(186,0x2244,INFIX,.27777f,.27777f) // &NotTildeEqual;
MATHML_OPERATOR(187,0x2247,INFIX,.27777f,.27777f) // &NotTildeFullEqual;
MATHML_OPERATOR(188,0x2249,INFIX,.27777f,.27777f) // &NotTildeTilde;
MATHML_OPERATOR(189,0x2224,INFIX,.27777f,.27777f) // &NotVerticalBar;
MATHML_OPERATOR(190,0x227A,INFIX,.27777f,.27777f) // &Precedes;
MATHML_OPERATOR(191,0xE2FE,INFIX,.27777f,.27777f) // &PrecedesEqual;
MATHML_OPERATOR(192,0x227C,INFIX,.27777f,.27777f) // &PrecedesSlantEqual;
MATHML_OPERATOR(193,0x227E,INFIX,.27777f,.27777f) // &PrecedesTilde;
MATHML_OPERATOR(194,0x221D,INFIX,.27777f,.27777f) // &Proportional;
MATHML_OPERATOR(195,0x21CB,INFIX|STRETCHY,.27777f,.27777f) // &ReverseEquilibrium;
MATHML_OPERATOR(196,0x22B3,INFIX,.27777f,.27777f) // &RightTriangle;
MATHML_OPERATOR(197,0xF411,INFIX,.27777f,.27777f) // &RightTriangleBar;
MATHML_OPERATOR(198,0x22B5,INFIX,.27777f,.27777f) // &RightTriangleEqual;
MATHML_OPERATOR(199,0x227B,INFIX,.27777f,.27777f) // &Succeeds;
MATHML_OPERATOR(200,0x227D,INFIX,.27777f,.27777f) // &SucceedsEqual; &SucceedsSlantEqual;
MATHML_OPERATOR(201,0x227F,INFIX,.27777f,.27777f) // &SucceedsTilde;
MATHML_OPERATOR(202,0x223C,INFIX,.27777f,.27777f) // &Tilde;
MATHML_OPERATOR(203,0x2243,INFIX,.27777f,.27777f) // &TildeEqual;
MATHML_OPERATOR(204,0x2245,INFIX,.27777f,.27777f) // &TildeFullEqual;
MATHML_OPERATOR(205,0x2248,INFIX,.27777f,.27777f) // &TildeTilde;
MATHML_OPERATOR(206,0x22A5,INFIX,.27777f,.27777f) // &UpTee;
MATHML_OPERATOR(207,0x2223,INFIX,.27777f,.27777f) // &VerticalBar;
MATHML_OPERATOR(208,0x2294,INFIX|STRETCHY,.22222f,.22222f) // &SquareUnion;
MATHML_OPERATOR(209,0x22C3,INFIX|STRETCHY,.22222f,.22222f) // &Union;
MATHML_OPERATOR(210,0x228E,INFIX|STRETCHY,.22222f,.22222f) // &UnionPlus;
MATHML_OPERATOR(211,0x002D,INFIX,.22222f,.22222f) // -
MATHML_OPERATOR(212,0x002B,INFIX,.22222f,.22222f) // +
MATHML_OPERATOR(213,0x22C2,INFIX|STRETCHY,.22222f,.22222f) // &Intersection;
MATHML_OPERATOR(214,0x2213,INFIX,.22222f,.22222f) // &MinusPlus;
MATHML_OPERATOR(215,0x00B1,INFIX,.22222f,.22222f) // &PlusMinus;
MATHML_OPERATOR(216,0x2293,INFIX|STRETCHY,.22222f,.22222f) // &SquareIntersection;
MATHML_OPERATOR(217,0x22C1,PREFIX|STRETCHY|LARGEOP|MOVABLELIMITS,0.0f,.16666f) // &Vee;
MATHML_OPERATOR(218,0x2296,PREFIX|LARGEOP|MOVABLELIMITS,0.0f,.16666f) // &CircleMinus;
MATHML_OPERATOR(219,0x2295,PREFIX|LARGEOP|MOVABLELIMITS,0.0f,.16666f) // &CirclePlus;
MATHML_OPERATOR(220,0x2211,PREFIX|STRETCHY|LARGEOP|MOVABLELIMITS,0.0f,.16666f) // &Sum;
MATHML_OPERATOR(221,0x22C3,PREFIX|STRETCHY|LARGEOP|MOVABLELIMITS,0.0f,.16666f) // &Union;
MATHML_OPERATOR(222,0x228E,PREFIX|STRETCHY|LARGEOP|MOVABLELIMITS,0.0f,.16666f) // &UnionPlus;
MATHML_OPERATOR(223,0x006C _ 0x0069 _ 0x006D,PREFIX|MOVABLELIMITS,0.0f,.16666f) // lim
MATHML_OPERATOR(224,0x006D _ 0x0061 _ 0x0078,PREFIX|MOVABLELIMITS,0.0f,.16666f) // max
MATHML_OPERATOR(225,0x006D _ 0x0069 _ 0x006E,PREFIX|MOVABLELIMITS,0.0f,.16666f) // min
MATHML_OPERATOR(226,0x2296,INFIX,.16666f,.16666f) // &CircleMinus;
MATHML_OPERATOR(227,0x2295,INFIX,.16666f,.16666f) // &CirclePlus;
MATHML_OPERATOR(228,0x2232,PREFIX|STRETCHY|LARGEOP,0.0f,0.0f) // &ClockwiseContourIntegral;
MATHML_OPERATOR(229,0x222E,PREFIX|STRETCHY|LARGEOP,0.0f,0.0f) // &ContourIntegral;
MATHML_OPERATOR(230,0x2233,PREFIX|STRETCHY|LARGEOP,0.0f,0.0f) // &CounterClockwiseContourIntegral;
MATHML_OPERATOR(231,0x222F,PREFIX|STRETCHY|LARGEOP,0.0f,0.0f) // &DoubleContourIntegral;
MATHML_OPERATOR(232,0x222B,PREFIX|STRETCHY|LARGEOP,0.0f,0.0f) // &Integral;
MATHML_OPERATOR(233,0x22D3,INFIX,.16666f,.16666f) // &Cup;
MATHML_OPERATOR(234,0x22D2,INFIX,.16666f,.16666f) // &Cap;
MATHML_OPERATOR(235,0x2240,INFIX,.16666f,.16666f) // &VerticalTilde;
MATHML_OPERATOR(236,0x22C0,PREFIX|STRETCHY|LARGEOP|MOVABLELIMITS,0.0f,.16666f) // &Wedge;
MATHML_OPERATOR(237,0x2297,PREFIX|LARGEOP|MOVABLELIMITS,0.0f,.16666f) // &CircleTimes;
MATHML_OPERATOR(238,0x2210,PREFIX|STRETCHY|LARGEOP|MOVABLELIMITS,0.0f,.16666f) // &Coproduct;
MATHML_OPERATOR(239,0x220F,PREFIX|STRETCHY|LARGEOP|MOVABLELIMITS,0.0f,.16666f) // &Product;
MATHML_OPERATOR(240,0x22C2,PREFIX|STRETCHY|LARGEOP|MOVABLELIMITS,0.0f,.16666f) // &Intersection;
MATHML_OPERATOR(241,0x2210,INFIX,.16666f,.16666f) // &Coproduct;
MATHML_OPERATOR(242,0x22C6,INFIX,.16666f,.16666f) // &Star;
MATHML_OPERATOR(243,0x2299,PREFIX|LARGEOP|MOVABLELIMITS,0.0f,.16666f) // &CircleDot;
MATHML_OPERATOR(244,0x002A,INFIX,.16666f,.16666f) // *
MATHML_OPERATOR(245,0xE89E,INFIX,0.0f,0.0f) // &InvisibleTimes;
MATHML_OPERATOR(246,0x00B7,INFIX,.16666f,.16666f) // &CenterDot;
MATHML_OPERATOR(247,0x2297,INFIX,.16666f,.16666f) // &CircleTimes;
MATHML_OPERATOR(248,0x22C1,INFIX,.16666f,.16666f) // &Vee;
MATHML_OPERATOR(249,0x22C0,INFIX,.16666f,.16666f) // &Wedge;
MATHML_OPERATOR(250,0x22C4,INFIX,.16666f,.16666f) // &Diamond;
MATHML_OPERATOR(251,0x2216,INFIX|STRETCHY,.16666f,.16666f) // &Backslash;
MATHML_OPERATOR(252,0x002F,INFIX|STRETCHY,.16666f,.16666f) // /
MATHML_OPERATOR(253,0x002D,PREFIX,0.0f,.05555f) // -
MATHML_OPERATOR(254,0x002B,PREFIX,0.0f,.05555f) // +
MATHML_OPERATOR(255,0x2213,PREFIX,0.0f,.05555f) // &MinusPlus;
MATHML_OPERATOR(256,0x00B1,PREFIX,0.0f,.05555f) // &PlusMinus;
MATHML_OPERATOR(257,0x002E,INFIX,0.0f,0.0f) // .
MATHML_OPERATOR(258,0xE619,INFIX,.11111f,.11111f) // &Cross;
MATHML_OPERATOR(259,0x002A _ 0x002A,INFIX,.11111f,.11111f) // **
MATHML_OPERATOR(260,0x2299,INFIX,.11111f,.11111f) // &CircleDot;
MATHML_OPERATOR(261,0x2218,INFIX,.11111f,.11111f) // &SmallCircle;
MATHML_OPERATOR(262,0x25A1,PREFIX,0.0f,.11111f) // &Square;
MATHML_OPERATOR(263,0x2207,PREFIX,0.0f,.11111f) // &Del;
MATHML_OPERATOR(264,0x2202,PREFIX,0.0f,.11111f) // &PartialD;
MATHML_OPERATOR(265,0xF74B,PREFIX,0.0f,.11111f) // &CapitalDifferentialD;
MATHML_OPERATOR(266,0xF74C,PREFIX,0.0f,.11111f) // &DifferentialD;
MATHML_OPERATOR(267,0x221A,PREFIX|STRETCHY,0.0f,.11111f) // &Sqrt;
MATHML_OPERATOR(268,0x21D3,INFIX|STRETCHY,.11111f,.11111f) // &DoubleDownArrow;
MATHML_OPERATOR(269,0xE200,INFIX|STRETCHY,.11111f,.11111f) // &DoubleLongLeftArrow;
MATHML_OPERATOR(270,0xE202,INFIX|STRETCHY,.11111f,.11111f) // &DoubleLongLeftRightArrow;
MATHML_OPERATOR(271,0xE204,INFIX|STRETCHY,.11111f,.11111f) // &DoubleLongRightArrow;
MATHML_OPERATOR(272,0x21D1,INFIX|STRETCHY,.11111f,.11111f) // &DoubleUpArrow;
MATHML_OPERATOR(273,0x21D5,INFIX|STRETCHY,.11111f,.11111f) // &DoubleUpDownArrow;
MATHML_OPERATOR(274,0x2193,INFIX|STRETCHY,.11111f,.11111f) // &DownArrow;
MATHML_OPERATOR(275,0xF504,INFIX|STRETCHY,.11111f,.11111f) // &DownArrowBar;
MATHML_OPERATOR(276,0xE216,INFIX|STRETCHY,.11111f,.11111f) // &DownArrowUpArrow;
MATHML_OPERATOR(277,0x21A7,INFIX|STRETCHY,.11111f,.11111f) // &DownTeeArrow;
MATHML_OPERATOR(278,0xF519,INFIX|STRETCHY,.11111f,.11111f) // &LeftDownTeeVector;
MATHML_OPERATOR(279,0x21C3,INFIX|STRETCHY,.11111f,.11111f) // &LeftDownVector;
MATHML_OPERATOR(280,0xF517,INFIX|STRETCHY,.11111f,.11111f) // &LeftDownVectorBar;
MATHML_OPERATOR(281,0xF515,INFIX|STRETCHY,.11111f,.11111f) // &LeftUpDownVector;
MATHML_OPERATOR(282,0xF518,INFIX|STRETCHY,.11111f,.11111f) // &LeftUpTeeVector;
MATHML_OPERATOR(283,0x21BF,INFIX|STRETCHY,.11111f,.11111f) // &LeftUpVector;
MATHML_OPERATOR(284,0xF516,INFIX|STRETCHY,.11111f,.11111f) // &LeftUpVectorBar;
MATHML_OPERATOR(285,0xE201,INFIX|STRETCHY,.11111f,.11111f) // &LongLeftArrow;
MATHML_OPERATOR(286,0xE203,INFIX|STRETCHY,.11111f,.11111f) // &LongLeftRightArrow;
MATHML_OPERATOR(287,0xE205,INFIX|STRETCHY,.11111f,.11111f) // &LongRightArrow;
MATHML_OPERATOR(288,0xE217,INFIX|STRETCHY,.11111f,.11111f) // &ReverseUpEquilibrium;
MATHML_OPERATOR(289,0xF514,INFIX|STRETCHY,.11111f,.11111f) // &RightDownTeeVector;
MATHML_OPERATOR(290,0x21C2,INFIX|STRETCHY,.11111f,.11111f) // &RightDownVector;
MATHML_OPERATOR(291,0xF512,INFIX|STRETCHY,.11111f,.11111f) // &RightDownVectorBar;
MATHML_OPERATOR(292,0xF510,INFIX|STRETCHY,.11111f,.11111f) // &RightUpDownVector;
MATHML_OPERATOR(293,0xF513,INFIX|STRETCHY,.11111f,.11111f) // &RightUpTeeVector;
MATHML_OPERATOR(294,0x21BE,INFIX|STRETCHY,.11111f,.11111f) // &RightUpVector;
MATHML_OPERATOR(295,0xF511,INFIX|STRETCHY,.11111f,.11111f) // &RightUpVectorBar;
MATHML_OPERATOR(296,0xE87F,INFIX,.11111f,.11111f) // &ShortDownArrow;
MATHML_OPERATOR(297,0xE880,INFIX,.11111f,.11111f) // &ShortUpArrow;
MATHML_OPERATOR(298,0x2191,INFIX|STRETCHY,.11111f,.11111f) // &UpArrow;
MATHML_OPERATOR(299,0xF503,INFIX|STRETCHY,.11111f,.11111f) // &UpArrowBar;
MATHML_OPERATOR(300,0x21C5,INFIX|STRETCHY,.11111f,.11111f) // &UpArrowDownArrow;
MATHML_OPERATOR(301,0x2195,INFIX|STRETCHY,.11111f,.11111f) // &UpDownArrow;
MATHML_OPERATOR(302,0xE218,INFIX|STRETCHY,.11111f,.11111f) // &UpEquilibrium;
MATHML_OPERATOR(303,0x21A5,INFIX|STRETCHY,.11111f,.11111f) // &UpTeeArrow;
MATHML_OPERATOR(304,0x005E,INFIX,.11111f,.11111f) // ^
MATHML_OPERATOR(305,0x003C _ 0x003E,INFIX,.11111f,.11111f) // &lt;>
MATHML_OPERATOR(306,0x0027,POSTFIX,.11111f,0.0f) // '
MATHML_OPERATOR(307,0x0021,POSTFIX,.11111f,0.0f) // !
MATHML_OPERATOR(308,0x0021 _ 0x0021,POSTFIX,.11111f,0.0f) // !!
MATHML_OPERATOR(309,0x007E,INFIX,.11111f,.11111f) // ~
MATHML_OPERATOR(310,0x0040,INFIX,.11111f,.11111f) // @
MATHML_OPERATOR(311,0x002D _ 0x002D,POSTFIX,.11111f,0.0f) // --
MATHML_OPERATOR(312,0x002D _ 0x002D,PREFIX,0.0f,.11111f) // --
MATHML_OPERATOR(313,0x002B _ 0x002B,POSTFIX,.11111f,0.0f) // ++
MATHML_OPERATOR(314,0x002B _ 0x002B,PREFIX,0.0f,.11111f) // ++
MATHML_OPERATOR(315,0xE8A0,INFIX,0.0f,0.0f) // &ApplyFunction;
MATHML_OPERATOR(316,0x003F,INFIX,.11111f,.11111f) // ?
MATHML_OPERATOR(317,0x005F,INFIX,.11111f,.11111f) // _
MATHML_OPERATOR(318,0x02D8,POSTFIX|ACCENT,0.0f,0.0f) // &Breve;
MATHML_OPERATOR(319,0x00B8,POSTFIX|ACCENT,0.0f,0.0f) // &Cedilla;
MATHML_OPERATOR(320,0x0060,POSTFIX|ACCENT,0.0f,0.0f) // &DiacriticalGrave;
MATHML_OPERATOR(321,0x02D9,POSTFIX|ACCENT,0.0f,0.0f) // &DiacriticalDot;
MATHML_OPERATOR(322,0x02DD,POSTFIX|ACCENT,0.0f,0.0f) // &DiacriticalDoubleAcute;
MATHML_OPERATOR(323,0x00B4,POSTFIX|ACCENT,0.0f,0.0f) // &DiacriticalAcute;
MATHML_OPERATOR(324,0x02DC,POSTFIX|STRETCHY|ACCENT,0.0f,0.0f) // &DiacriticalTilde;
MATHML_OPERATOR(325,0x00A8,POSTFIX|ACCENT,0.0f,0.0f) // &DoubleDot;
MATHML_OPERATOR(326,0x0311,POSTFIX|ACCENT,0.0f,0.0f) // &DownBreve;
MATHML_OPERATOR(327,0x02C7,POSTFIX|STRETCHY|ACCENT,0.0f,0.0f) // &Hacek;
MATHML_OPERATOR(328,0x0302,POSTFIX|STRETCHY|ACCENT,0.0f,0.0f) // &Hat;
MATHML_OPERATOR(329,0x00AF,POSTFIX|STRETCHY|ACCENT,0.0f,0.0f) // &OverBar;
MATHML_OPERATOR(330,0xF612,POSTFIX|STRETCHY|ACCENT,0.0f,0.0f) // &OverBrace;
MATHML_OPERATOR(331,0xF614,POSTFIX|STRETCHY|ACCENT,0.0f,0.0f) // &OverBracket;
MATHML_OPERATOR(332,0xF610,POSTFIX|STRETCHY|ACCENT,0.0f,0.0f) // &OverParenthesis;
MATHML_OPERATOR(333,0x20DB,POSTFIX|ACCENT,0.0f,0.0f) // &TripleDot;
MATHML_OPERATOR(334,0x0332,POSTFIX|STRETCHY|ACCENT,0.0f,0.0f) // &UnderBar;
MATHML_OPERATOR(335,0xF613,POSTFIX|STRETCHY|ACCENT,0.0f,0.0f) // &UnderBrace;
MATHML_OPERATOR(336,0xF615,POSTFIX|STRETCHY|ACCENT,0.0f,0.0f) // &UnderBracket;
MATHML_OPERATOR(337,0xF611,POSTFIX|STRETCHY|ACCENT,0.0f,0.0f) // &UnderParenthesis;
MATHML_OPERATOR(338,0x2225,PREFIX|STRETCHY|FENCE,.0f,.0f) // &DoubleVerticalBar;
MATHML_OPERATOR(339,0x2225,POSTFIX|STRETCHY|FENCE,.0f,.0f) // &DoubleVerticalBar;
MATHML_OPERATOR(340,0x2223,PREFIX|STRETCHY|FENCE,.0f,.0f) // &VerticalBar;
MATHML_OPERATOR(341,0x2223,POSTFIX|STRETCHY|FENCE,.0f,.0f) // &VerticalBar;
MATHML_OPERATOR(342,0x007C,PREFIX|STRETCHY|FENCE,.0f,.0f) // |
MATHML_OPERATOR(343,0x007C,POSTFIX|STRETCHY|FENCE,.0f,.0f) // |
MATHML_OPERATOR(344,0x20D7,POSTFIX|STRETCHY|ACCENT,0.0f,0.0f) // &#x20D7;
MATHML_OPERATOR(345,0x20D6,POSTFIX|STRETCHY|ACCENT,0.0f,0.0f) // &#x20D6;
MATHML_OPERATOR(346,0x20E1,POSTFIX|STRETCHY|ACCENT,0.0f,0.0f) // &#x20E1;
MATHML_OPERATOR(347,0x20D1,POSTFIX|STRETCHY|ACCENT,0.0f,0.0f) // &#x20D1;
MATHML_OPERATOR(348,0x20D0,POSTFIX|STRETCHY|ACCENT,0.0f,0.0f) // &#x20D0;

#undef INFIX         
#undef PREFIX        
#undef POSTFIX       
#undef STRETCHY      
#undef FENCE         
#undef ACCENT        
#undef LARGEOP       
#undef SEPARATOR     
#undef MOVABLELIMITS 

  #undef _
#endif
