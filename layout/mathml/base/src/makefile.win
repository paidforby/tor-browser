#!nmake
#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
# 
# Software distributed under the License is distributed on an "AS
# IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
# implied. See the License for the specific language governing
# rights and limitations under the License.
# 
# The Original Code is Mozilla MathML Project.
# 
# The Initial Developer of the Original Code is Heriot-Watt 
# University, Edinburgh.  Portions created by Heriot-Watt University 
# are Copyright (C) 1999 Heriot-Watt University. All Rights Reserved.
# 
# Contributor(s):
#   David J. Fiddes <D.J.Fiddes@hw.ac.uk>
#

DEPTH=..\..\..\.. 

LIBRARY_NAME=raptormathmlbase_s 
MODULE=raptor 

DEFINES= -DWIN32_LEAN_AND_MEAN

CPPSRCS=          nsMathMLChar.cpp                      \
                  nsMathMLContainerFrame.cpp		\
                  nsMathMLmrowFrame.obj 		\
                  nsMathMLmphantomFrame.cpp		\
                  nsMathMLmfencedFrame.cpp		\
                  nsMathMLmfracFrame.cpp		\
                  nsMathMLmsubFrame.cpp			\
                  nsMathMLmiFrame.cpp			\
                  nsMathMLmtextFrame.cpp		\
                  nsMathMLmsubsupFrame.cpp		\
                  nsMathMLmmultiscriptsFrame.cpp	\
                  nsMathMLmsupFrame.cpp			\
                  nsMathMLmtableFrame.cpp		\
                  nsMathMLmoFrame.cpp			\
                  nsMathMLmunderFrame.cpp		\
                  nsMathMLmoverFrame.cpp		\
                  nsMathMLmunderoverFrame.cpp		\
                  nsMathMLmpaddedFrame.cpp		\
                  nsMathMLmspaceFrame.cpp		\
                  nsMathMLmsFrame.cpp			\
                  nsMathMLmstyleFrame.cpp		\
                  nsMathMLmsqrtFrame.cpp		\
                  nsMathMLmrootFrame.cpp		\
                  nsMathMLmactionFrame.cpp		\
                  $(NULL)

CPP_OBJS= .\$(OBJDIR)\nsMathMLChar.obj		        \
          .\$(OBJDIR)\nsMathMLContainerFrame.obj        \
          .\$(OBJDIR)\nsMathMLmrowFrame.obj		\
          .\$(OBJDIR)\nsMathMLmphantomFrame.obj		\
          .\$(OBJDIR)\nsMathMLmfencedFrame.obj		\
          .\$(OBJDIR)\nsMathMLmfracFrame.obj		\
          .\$(OBJDIR)\nsMathMLmsubFrame.obj		\
          .\$(OBJDIR)\nsMathMLmiFrame.obj		\
          .\$(OBJDIR)\nsMathMLmtextFrame.obj		\
          .\$(OBJDIR)\nsMathMLmsubsupFrame.obj		\
          .\$(OBJDIR)\nsMathMLmmultiscriptsFrame.obj	\
          .\$(OBJDIR)\nsMathMLmsupFrame.obj		\
          .\$(OBJDIR)\nsMathMLmtableFrame.obj		\
          .\$(OBJDIR)\nsMathMLmoFrame.obj		\
          .\$(OBJDIR)\nsMathMLmunderFrame.obj		\
          .\$(OBJDIR)\nsMathMLmoverFrame.obj		\
          .\$(OBJDIR)\nsMathMLmunderoverFrame.obj	\
          .\$(OBJDIR)\nsMathMLmpaddedFrame.obj		\
          .\$(OBJDIR)\nsMathMLmspaceFrame.obj		\
          .\$(OBJDIR)\nsMathMLmsFrame.obj		\
          .\$(OBJDIR)\nsMathMLmstyleFrame.obj		\
          .\$(OBJDIR)\nsMathMLmsqrtFrame.obj		\
          .\$(OBJDIR)\nsMathMLmrootFrame.obj		\
          .\$(OBJDIR)\nsMathMLmactionFrame.obj		\
          $(NULL)

EXPORTS =					\
	$(NULL)

LINCS=-I$(PUBLIC)\xpcom -I$(PUBLIC)\raptor -I$(PUBLIC)\js \
  -I..\..\..\html\style\src \
  -I..\..\..\html\base\src \
  -I..\..\..\html\content\src \
  -I..\..\..\html\table\src \
  -I$(PUBLIC)\dom \
  -I..\..\content\src \
  -I..\..\..\base\public \
  -I$(PUBLIC)\netlib -I..\..\..\base\src -I$(PUBLIC)\pref

LCFLAGS = \
        $(LCFLAGS) \
        $(DEFINES) \
        $(NULL)

include <$(DEPTH)\config\rules.mak>

install:: $(LIBRARY)
	$(MAKE_INSTALL) $(LIBRARY) $(DIST)\lib

clobber::
	rm -f $(DIST)\lib\$(LIBRARY_NAME).lib
