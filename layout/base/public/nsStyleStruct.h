/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */
#ifndef nsStyleStruct_h___
#define nsStyleStruct_h___

enum nsStyleStructID {
  eStyleStruct_Font           = 1,
  eStyleStruct_Color          = 2,
  eStyleStruct_Spacing        = 3,
  eStyleStruct_List           = 4,
  eStyleStruct_Position       = 5,
  eStyleStruct_Text           = 6,
  eStyleStruct_Display        = 7,
  eStyleStruct_Table          = 8,
  eStyleStruct_Content        = 9,
  eStyleStruct_UserInterface  = 10,
  eStyleStruct_Print					= 11
};


struct nsStyleStruct {
};


#endif /* nsStyleStruct_h___ */

