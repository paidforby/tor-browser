/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is Mozilla Communicator client code.
 *
 * The Initial Developer of the Original Code is Netscape Communications
 * Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */
#include "nsIDOMHTMLTableRowElement.h"
#include "nsIDOMHTMLTableElement.h"
#include "nsIDOMHTMLTableSectionElement.h"
#include "nsIDOMHTMLTableCellElement.h"
#include "nsIScriptObjectOwner.h"
#include "nsIDOMEventReceiver.h"
#include "nsIHTMLContent.h"
#include "nsIHTMLAttributes.h"
#include "nsGenericHTMLElement.h"
#include "GenericElementCollection.h"
#include "nsHTMLAtoms.h"
#include "nsHTMLIIDs.h"
#include "nsIStyleContext.h"
#include "nsIMutableStyleContext.h"
#include "nsStyleConsts.h"
#include "nsIPresContext.h"
#include "nsHTMLParts.h"

// temporary
#include "nsIDocument.h"
#include "nsIPresShell.h"
#include "nsIFrame.h"

// nsTableCellCollection is needed because GenericElementCollection 
// only supports element of a single tag. This collection supports
// elements <td> or <th> elements.

class nsTableCellCollection : public GenericElementCollection
{
public:

  nsTableCellCollection(nsIContent* aParent, 
                        nsIAtom*    aTag);
  ~nsTableCellCollection();

  NS_IMETHOD GetLength(PRUint32* aLength);
  NS_IMETHOD Item(PRUint32 aIndex, nsIDOMNode** aReturn);
};


nsTableCellCollection::nsTableCellCollection(nsIContent* aParent, 
                                             nsIAtom*    aTag)
  : GenericElementCollection(aParent, aTag)
{
}

nsTableCellCollection::~nsTableCellCollection()
{
}

NS_IMETHODIMP 
nsTableCellCollection::GetLength(PRUint32* aLength)
{
  if (!aLength) {
    return NS_ERROR_NULL_POINTER;
  }
  *aLength = 0;
  nsresult result = NS_OK;
  if (mParent) {
    nsIContent* child = nsnull;
    PRUint32 childIndex = 0;
    mParent->ChildAt(childIndex, child);
    while (child) {
      nsIAtom* childTag;
      child->GetTag(childTag);
      if ((nsHTMLAtoms::td ==childTag) || (nsHTMLAtoms::th ==childTag)) {
        (*aLength)++;
      }
      NS_RELEASE(childTag);
      NS_RELEASE(child);
      childIndex++;
      mParent->ChildAt(childIndex, child);
    }
  }
  return result;
}

NS_IMETHODIMP 
nsTableCellCollection::Item(PRUint32     aIndex, 
                            nsIDOMNode** aReturn)
{
  *aReturn = nsnull;
  PRUint32 theIndex = 0;
  nsresult rv = NS_OK;
  if (mParent) {
    nsIContent* child = nsnull;
    PRUint32 childIndex = 0;
    mParent->ChildAt(childIndex, child);
    while (child) {
      nsIAtom* childTag;
      child->GetTag(childTag);
      if ((nsHTMLAtoms::td ==childTag) || (nsHTMLAtoms::th ==childTag)) {
        if (aIndex == theIndex) {
          child->QueryInterface(NS_GET_IID(nsIDOMNode), (void**)aReturn);   // out-param addref
          NS_ASSERTION(aReturn, "content element must be an nsIDOMNode");
          NS_RELEASE(childTag);
          NS_RELEASE(child);
          break;
        }
        theIndex++;
      }
      NS_RELEASE(childTag);
      NS_RELEASE(child);
      childIndex++;
      mParent->ChildAt(childIndex, child);
    }
  }
  return rv;
}

//----------------------------------------------------------------------

class nsHTMLTableRowElement : public nsIDOMHTMLTableRowElement,
                              public nsIJSScriptObject,
                              public nsIHTMLContent
{
public:
  nsHTMLTableRowElement(nsINodeInfo *aNodeInfo);
  virtual ~nsHTMLTableRowElement();

  // nsISupports
  NS_DECL_ISUPPORTS

  // nsIDOMNode
  NS_IMPL_IDOMNODE_USING_GENERIC(mInner)

  // nsIDOMElement
  NS_IMPL_IDOMELEMENT_USING_GENERIC(mInner)

  // nsIDOMHTMLElement
  NS_IMPL_IDOMHTMLELEMENT_USING_GENERIC(mInner)

  // nsIDOMHTMLTableRowElement
  NS_DECL_IDOMHTMLTABLEROWELEMENT

  // nsIJSScriptObject
  NS_IMPL_IJSSCRIPTOBJECT_USING_GENERIC(mInner)

  // nsIContent
  NS_IMPL_ICONTENT_USING_GENERIC(mInner)

  // nsIHTMLContent
  NS_IMPL_IHTMLCONTENT_USING_GENERIC(mInner)

protected:
  nsresult GetSection(nsIDOMHTMLTableSectionElement** aSection);
  nsresult GetTable(nsIDOMHTMLTableElement** aTable);
  nsGenericHTMLContainerElement mInner;
  nsTableCellCollection* mCells;
};

#ifdef XXX_debugging
static
void DebugList(nsIDOMHTMLTableElement* aTable) {
  nsIHTMLContent* content = nsnull;
  nsresult result = aTable->QueryInterface(NS_GET_IID(nsIHTMLContent), (void**)&content);
  if (NS_SUCCEEDED(result) && (nsnull != content)) {
    nsIDocument* doc = nsnull;
    result = content->GetDocument(doc);
    if (NS_SUCCEEDED(result) && (nsnull != doc)) {
      nsIContent* root = doc->GetRootContent();
      if (root) {
        root->List();
      }
      nsIPresShell* shell = doc->GetShellAt(0);
      if (nsnull != shell) {
        nsIFrame* rootFrame;
        shell->GetRootFrame(rootFrame);
        if (nsnull != rootFrame) {
          rootFrame->List(stdout, 0);
        }
      }
      NS_RELEASE(shell);
      NS_RELEASE(doc);
    }
    NS_RELEASE(content);
  }
}
#endif 

nsresult
NS_NewHTMLTableRowElement(nsIHTMLContent** aInstancePtrResult,
                          nsINodeInfo *aNodeInfo)
{
  NS_ENSURE_ARG_POINTER(aInstancePtrResult);
  NS_ENSURE_ARG_POINTER(aNodeInfo);

  nsIHTMLContent* it = new nsHTMLTableRowElement(aNodeInfo);
  if (nsnull == it) {
    return NS_ERROR_OUT_OF_MEMORY;
  }
  return it->QueryInterface(NS_GET_IID(nsIHTMLContent), (void**) aInstancePtrResult);
}


nsHTMLTableRowElement::nsHTMLTableRowElement(nsINodeInfo *aNodeInfo)
{
  NS_INIT_REFCNT();
  mInner.Init(this, aNodeInfo);
  mCells = nsnull;
}

nsHTMLTableRowElement::~nsHTMLTableRowElement()
{
  if (nsnull != mCells) {
    mCells->ParentDestroyed();
    NS_RELEASE(mCells);
  }
}

NS_IMPL_ADDREF(nsHTMLTableRowElement)

NS_IMPL_RELEASE(nsHTMLTableRowElement)

nsresult
nsHTMLTableRowElement::QueryInterface(REFNSIID aIID, void** aInstancePtr)
{
  NS_IMPL_HTML_CONTENT_QUERY_INTERFACE(aIID, aInstancePtr, this)
  if (aIID.Equals(NS_GET_IID(nsIDOMHTMLTableRowElement))) {
    nsIDOMHTMLTableRowElement* tmp = this;
    *aInstancePtr = (void*) tmp;
    NS_ADDREF_THIS();
    return NS_OK;
  }
  return NS_NOINTERFACE;
}

nsresult
nsHTMLTableRowElement::CloneNode(PRBool aDeep, nsIDOMNode** aReturn)
{
  nsHTMLTableRowElement* it = new nsHTMLTableRowElement(mInner.mNodeInfo);
  if (nsnull == it) {
    return NS_ERROR_OUT_OF_MEMORY;
  }
  nsCOMPtr<nsIDOMNode> kungFuDeathGrip(it);
  mInner.CopyInnerTo(this, &it->mInner, aDeep);
  return it->QueryInterface(NS_GET_IID(nsIDOMNode), (void**) aReturn);
}

// protected method
nsresult
nsHTMLTableRowElement::GetSection(nsIDOMHTMLTableSectionElement** aSection)
{
  *aSection = nsnull;
  if (nsnull == aSection) {
    return NS_ERROR_OUT_OF_MEMORY;
  }
  nsIDOMNode *sectionNode = nsnull;
  nsresult result = GetParentNode(&sectionNode);
  if (NS_SUCCEEDED(result) && (nsnull != sectionNode)) {
    result = sectionNode->QueryInterface(NS_GET_IID(nsIDOMHTMLTableSectionElement), (void**)aSection);
    NS_RELEASE(sectionNode);
  }
  return result;
}

// protected method
nsresult
nsHTMLTableRowElement::GetTable(nsIDOMHTMLTableElement** aTable)
{
  *aTable = nsnull;
  if (nsnull == aTable) {
    return NS_ERROR_OUT_OF_MEMORY;
  }
  nsIDOMNode *sectionNode = nsnull;
  nsresult result = GetParentNode(&sectionNode); 
  if (NS_SUCCEEDED(result) && (nsnull != sectionNode)) {
    nsIDOMNode *tableNode = nsnull;
    result = sectionNode->GetParentNode(&tableNode);
    if (NS_SUCCEEDED(result) && (nsnull != tableNode)) {
      result = tableNode->QueryInterface(NS_GET_IID(nsIDOMHTMLTableElement), (void**)aTable);
      NS_RELEASE(tableNode);
    }
    NS_RELEASE(sectionNode);
  }
  return result;
}

NS_IMETHODIMP
nsHTMLTableRowElement::GetRowIndex(PRInt32* aValue)
{
  *aValue = -1;
  nsIDOMHTMLTableElement* table = nsnull;
  nsresult result = GetTable(&table);
  if (NS_SUCCEEDED(result) && (nsnull != table)) {
    nsIDOMHTMLCollection *rows = nsnull;
    table->GetRows(&rows);
    PRUint32 numRows;
    rows->GetLength(&numRows);
	PRBool found = PR_FALSE;
    for (PRUint32 i = 0; (i < numRows) && !found; i++) {
      nsIDOMNode *node = nsnull;
      rows->Item(i, &node);
      if (this == node) {
        *aValue = i;
        found = PR_TRUE;
      }
	  NS_IF_RELEASE(node);
    }
    NS_RELEASE(rows);
    NS_RELEASE(table);
  }

  return result;
}

// this tells the table to delete a row and then insert it in a different place. This will generate 2 reflows
// until things get fixed at a higher level (e.g. DOM batching).
NS_IMETHODIMP
nsHTMLTableRowElement::SetRowIndex(PRInt32 aValue)
{
  PRInt32 oldIndex;
  nsresult result = GetRowIndex(&oldIndex);
  if ((-1 == oldIndex) || (oldIndex == aValue) || (NS_OK != result)) {
    return result;
  }

  nsIDOMHTMLTableElement* table = nsnull;
  result = GetTable(&table);
  if (NS_FAILED(result) || (nsnull == table)) {
    return result;
  }

  nsIDOMHTMLCollection *rows = nsnull;
  table->GetRows(&rows);
  PRUint32 numRowsU;
  rows->GetLength(&numRowsU);
  PRInt32 numRows = numRowsU; // numRows will be > 0 since this must be a row

  // check if it really moves
  if ( !(((0 == oldIndex) && (aValue <= 0)) || ((numRows-1 == oldIndex) && (aValue >= numRows-1)))) {
    nsIDOMNode *section = nsnull;
    nsIDOMNode* refRow = nsnull;
	PRInt32 refIndex = aValue;
    if (aValue < numRows) {
	  refIndex = 0;
    } else {
	  refIndex = numRows-1;
	}
	rows->Item(refIndex, &refRow);
    refRow->GetParentNode(&section);

    AddRef(); // don't use NS_ADDREF_THIS
    table->DeleteRow(oldIndex);     // delete this from the table
    nsIDOMNode *returnNode;
    if (aValue >= numRows) {
      section->AppendChild(this, &returnNode); // add this back into the table
    } else {
      section->InsertBefore(this, refRow, &returnNode); // add this back into the table
    }
    Release(); // from addref above, can't use NS_RELEASE

    NS_RELEASE(section);
    NS_RELEASE(refRow);  // XXX is this right, check nsHTMLTableElement also
  }

  NS_RELEASE(rows);
  NS_RELEASE(table);

  return NS_OK;
}

NS_IMETHODIMP
nsHTMLTableRowElement::GetSectionRowIndex(PRInt32* aValue)
{
  *aValue = -1;
  nsIDOMHTMLTableSectionElement* section = nsnull;
  nsresult result = GetSection(&section);
  if (NS_SUCCEEDED(result) && (nsnull != section)) {
    nsIDOMHTMLCollection *rows = nsnull;
    section->GetRows(&rows);
    PRUint32 numRows;
    rows->GetLength(&numRows);
	PRBool found = PR_FALSE;
    for (PRUint32 i = 0; (i < numRows) && !found; i++) {
      nsIDOMNode *node = nsnull;
      rows->Item(i, &node);
      if (this == node) {
        *aValue = i;
        found = PR_TRUE;
      }
	  NS_IF_RELEASE(node);
    } 
    NS_RELEASE(rows);
    NS_RELEASE(section);
  }

  return NS_OK;
}

// this generates 2 reflows like SetRowIndex
NS_IMETHODIMP
nsHTMLTableRowElement::SetSectionRowIndex(PRInt32 aValue)
{
  PRInt32 oldIndex;
  nsresult result = GetRowIndex(&oldIndex);
  if ((-1 == oldIndex) || (oldIndex == aValue) || (NS_OK != result)) {
    return result;
  }

  nsIDOMHTMLTableSectionElement* section = nsnull;
  result = GetSection(&section);
  if (NS_FAILED(result) || (nsnull == section)) {
    return result;
  }

  nsIDOMHTMLCollection *rows = nsnull;
  section->GetRows(&rows);
  PRUint32 numRowsU;
  rows->GetLength(&numRowsU);
  PRInt32 numRows = numRowsU; 

  // check if it really moves
  if ( !(((0 == oldIndex) && (aValue <= 0)) || ((numRows-1 == oldIndex) && (aValue >= numRows-1)))) {
    AddRef(); // don't use NS_ADDREF_THIS
    section->DeleteRow(oldIndex);     // delete this from the section
    numRows--;
    nsIDOMNode *returnNode;
    if ((numRows <= 0) || (aValue >= numRows)) {
      section->AppendChild(this, &returnNode); // add this back into the section
    } else {
      PRInt32 newIndex = aValue;
      if (aValue <= 0) {
        newIndex = 0;
      } else if (aValue > oldIndex) {
        newIndex--;                   // since this got removed before GetLength was called
      }
      nsIDOMNode *refNode;
      rows->Item(newIndex, &refNode);
      section->InsertBefore(this, refNode, &returnNode); // add this back into the section
	  NS_IF_RELEASE(refNode);
    }
    Release(); // from addref above, can't use NS_RELEASE
  }

  NS_RELEASE(rows);
  NS_RELEASE(section);

  return NS_OK;
}

NS_IMETHODIMP
nsHTMLTableRowElement::GetCells(nsIDOMHTMLCollection** aValue)
{
  if (nsnull == mCells) {
    mCells = new nsTableCellCollection(this, nsHTMLAtoms::td);
    NS_ADDREF(mCells); // this table's reference, released in the destructor
  }
  mCells->QueryInterface(NS_GET_IID(nsIDOMHTMLCollection), (void **)aValue);   // caller's addref 
  return NS_OK;
}

NS_IMETHODIMP
nsHTMLTableRowElement::SetCells(nsIDOMHTMLCollection* aValue)
{
  nsIDOMHTMLCollection* cells;
  GetCells(&cells);
  PRUint32 numCells;
  cells->GetLength(&numCells);
  PRUint32 i;
  for (i = 0; i < numCells; i++) {
    DeleteCell(i);
  }

  aValue->GetLength(&numCells);
  for (i = 0; i < numCells; i++) {
    nsIDOMNode *node = nsnull;
    cells->Item(i, &node);
    nsIDOMNode* aReturn;
    AppendChild(node, (nsIDOMNode**)&aReturn);
  }
  NS_RELEASE(cells);
  return NS_OK;
}


NS_IMETHODIMP
nsHTMLTableRowElement::InsertCell(PRInt32 aIndex, nsIDOMHTMLElement** aValue)
{
  *aValue = nsnull;

  PRInt32 refIndex = (0 <= aIndex) ? aIndex : 0;
      
  nsCOMPtr<nsIDOMHTMLCollection> cells;
  GetCells(getter_AddRefs(cells));
  PRUint32 cellCount;
  cells->GetLength(&cellCount);
  PRBool doInsert = (aIndex < PRInt32(cellCount));

    // create the cell
  nsIHTMLContent* cellContent = nsnull;
  nsCOMPtr<nsINodeInfo> nodeInfo;
  mInner.mNodeInfo->NameChanged(nsHTMLAtoms::td, *getter_AddRefs(nodeInfo));

  nsresult rv = NS_NewHTMLTableCellElement(&cellContent, nodeInfo);
  if (NS_SUCCEEDED(rv) && (nsnull != cellContent)) {
    nsIDOMNode* cellNode = nsnull;
    rv = cellContent->QueryInterface(NS_GET_IID(nsIDOMNode), (void **)&cellNode); 
    if (NS_SUCCEEDED(rv) && (nsnull != cellNode)) {
      if (doInsert) {
        PRInt32 refIndex = PR_MAX(aIndex, 0);   
        nsCOMPtr<nsIDOMNode> refCell;
        cells->Item(refIndex, getter_AddRefs(refCell));
        rv = InsertBefore(cellNode, refCell, (nsIDOMNode **)aValue);
      } else {
        rv = AppendChild(cellNode, (nsIDOMNode **)aValue);
      }
      NS_RELEASE(cellNode);
    }
    NS_RELEASE(cellContent);
  }  
  return NS_OK;
}


NS_IMETHODIMP
nsHTMLTableRowElement::DeleteCell(PRInt32 aValue)
{
  nsIDOMHTMLCollection *cells;
  GetCells(&cells);
  nsIDOMNode *cell = nsnull;
  cells->Item(aValue, &cell);
  if (nsnull != cell) {
    RemoveChild(cell, &cell);
  }
  NS_RELEASE(cells);
  return NS_OK;
}

NS_IMPL_STRING_ATTR(nsHTMLTableRowElement, Align, align)
NS_IMPL_STRING_ATTR(nsHTMLTableRowElement, BgColor, bgcolor)
NS_IMPL_STRING_ATTR(nsHTMLTableRowElement, Ch, ch)
NS_IMPL_STRING_ATTR(nsHTMLTableRowElement, ChOff, choff)
NS_IMPL_STRING_ATTR(nsHTMLTableRowElement, VAlign, valign)


NS_IMETHODIMP
nsHTMLTableRowElement::StringToAttribute(nsIAtom* aAttribute,
                                  const nsAReadableString& aValue,
                                  nsHTMLValue& aResult)
{
  /* ignore these attributes, stored simply as strings
     ch
   */
  /* attributes that resolve to integers with default=0*/
  if (aAttribute == nsHTMLAtoms::choff) {
    if (nsGenericHTMLElement::ParseValue(aValue, 0, aResult, eHTMLUnit_Integer)) {
      return NS_CONTENT_ATTR_HAS_VALUE;
    }
  }

  /* attributes that resolve to integers or percents */
  else if (aAttribute == nsHTMLAtoms::height) {
    if (nsGenericHTMLElement::ParseValueOrPercent(aValue, aResult, eHTMLUnit_Pixel)) {
      return NS_CONTENT_ATTR_HAS_VALUE;
    }
  }

  /* attributes that resolve to integers or percents or proportions */
  else if (aAttribute == nsHTMLAtoms::width) {
    if (nsGenericHTMLElement::ParseValueOrPercentOrProportional(aValue, aResult, eHTMLUnit_Pixel)) {
      return NS_CONTENT_ATTR_HAS_VALUE;
    }
  }

  /* other attributes */
  else if (aAttribute == nsHTMLAtoms::align) {
    if (mInner.ParseTableCellHAlignValue(aValue, aResult)) {
      return NS_CONTENT_ATTR_HAS_VALUE;
    }
  }
  else if (aAttribute == nsHTMLAtoms::bgcolor) {
    if (nsGenericHTMLElement::ParseColor(aValue, mInner.mDocument, aResult)) {
      return NS_CONTENT_ATTR_HAS_VALUE;
    }
  }
  else if (aAttribute == nsHTMLAtoms::valign) {
    if (nsGenericHTMLElement::ParseTableVAlignValue(aValue, aResult)) {
      return NS_CONTENT_ATTR_HAS_VALUE;
    }
  }
  return NS_CONTENT_ATTR_NOT_THERE;
}

NS_IMETHODIMP
nsHTMLTableRowElement::AttributeToString(nsIAtom* aAttribute,
                                  const nsHTMLValue& aValue,
                                  nsAWritableString& aResult) const
{
  /* ignore these attributes, stored already as strings
     ch
   */
  /* ignore attributes that are of standard types
     choff, height, width, background, bgcolor
   */
  if (aAttribute == nsHTMLAtoms::align) {
    if (mInner.TableCellHAlignValueToString(aValue, aResult)) {
      return NS_CONTENT_ATTR_HAS_VALUE;
    }
  }
  else if (aAttribute == nsHTMLAtoms::valign) {
    if (nsGenericHTMLElement::TableVAlignValueToString(aValue, aResult)) {
      return NS_CONTENT_ATTR_HAS_VALUE;
    }
  }
  return mInner.AttributeToString(aAttribute, aValue, aResult);
}

static void
MapAttributesInto(const nsIHTMLMappedAttributes* aAttributes,
                  nsIMutableStyleContext* aContext,
                  nsIPresContext* aPresContext)
{
  NS_PRECONDITION(nsnull!=aContext, "bad style context arg");
  NS_PRECONDITION(nsnull!=aPresContext, "bad presentation context arg");

  if (nsnull!=aAttributes)
  {
    nsHTMLValue value;
    nsHTMLValue widthValue;
    nsStyleText* textStyle = nsnull;

    // align: enum
    aAttributes->GetAttribute(nsHTMLAtoms::align, value);
    if (value.GetUnit() == eHTMLUnit_Enumerated) 
    {
      textStyle = (nsStyleText*)aContext->GetMutableStyleData(eStyleStruct_Text);
      textStyle->mTextAlign = value.GetIntValue();
    }
  
    // valign: enum
    aAttributes->GetAttribute(nsHTMLAtoms::valign, value);
    if (value.GetUnit() == eHTMLUnit_Enumerated) 
    {
      if (nsnull==textStyle)
        textStyle = (nsStyleText*)aContext->GetMutableStyleData(eStyleStruct_Text);
      textStyle->mVerticalAlign.SetIntValue(value.GetIntValue(), eStyleUnit_Enumerated);
    }

    // height: pixel
    aAttributes->GetAttribute(nsHTMLAtoms::height, value);
    if (value.GetUnit() == eHTMLUnit_Pixel) {
      float p2t;
      aPresContext->GetScaledPixelsToTwips(&p2t);
      nsStylePosition* pos = (nsStylePosition*)
        aContext->GetMutableStyleData(eStyleStruct_Position);
      nscoord twips = NSIntPixelsToTwips(value.GetPixelValue(), p2t);
      pos->mHeight.SetCoordValue(twips);
    }
    nsGenericHTMLElement::MapBackgroundAttributesInto(aAttributes, aContext, aPresContext);
    nsGenericHTMLElement::MapCommonAttributesInto(aAttributes, aContext, aPresContext);
  }
}

NS_IMETHODIMP
nsHTMLTableRowElement::GetMappedAttributeImpact(const nsIAtom* aAttribute,
                                                PRInt32& aHint) const
{
  if ((aAttribute == nsHTMLAtoms::align) ||
      (aAttribute == nsHTMLAtoms::valign) ||
      (aAttribute == nsHTMLAtoms::height)) {
    aHint = NS_STYLE_HINT_REFLOW;
  }
  else if (! nsGenericHTMLElement::GetCommonMappedAttributesImpact(aAttribute, aHint)) {
    if (! nsGenericHTMLElement::GetBackgroundAttributesImpact(aAttribute, aHint)) {
      aHint = NS_STYLE_HINT_CONTENT;
    }
  }

  return NS_OK;
}

NS_IMETHODIMP
nsHTMLTableRowElement::GetAttributeMappingFunctions(nsMapAttributesFunc& aFontMapFunc,
                                                    nsMapAttributesFunc& aMapFunc) const
{
  aFontMapFunc = nsnull;
  aMapFunc = &MapAttributesInto;
  return NS_OK;
}


NS_IMETHODIMP
nsHTMLTableRowElement::HandleDOMEvent(nsIPresContext* aPresContext,
                               nsEvent* aEvent,
                               nsIDOMEvent** aDOMEvent,
                               PRUint32 aFlags,
                               nsEventStatus* aEventStatus)
{
  return mInner.HandleDOMEvent(aPresContext, aEvent, aDOMEvent,
                               aFlags, aEventStatus);
}




NS_IMETHODIMP
nsHTMLTableRowElement::SizeOf(nsISizeOfHandler* aSizer, PRUint32* aResult) const
{
  if (!aResult) return NS_ERROR_NULL_POINTER;
#ifdef DEBUG
  PRUint32 sum = 0;
  mInner.SizeOf(aSizer, &sum, sizeof(*this));
  if (mCells) {
    PRUint32 asize;
    mCells->SizeOf(aSizer, &asize);
    sum += asize;
  }
  *aResult = sum;
#endif
  return NS_OK;
}
