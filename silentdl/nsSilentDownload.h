/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- 
 * 
 * The contents of this file are subject to the Netscape Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Netscape 
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1998 Netscape Communications Corporation. All
 * Rights Reserved.
 *
 * Contributor(s): 
 */ 

#ifndef nsSilentDownload_h___
#define nsSilentDownload_h___

#define NS_SilentDownload_CID                        \
{ /* 18c2f982-b09f-11d2-bcde-00805f0e1353 */         \
    0x18c2f982,                                      \
    0xb09f,                                          \
    0x11d2,                                          \
    {0xbc, 0xde, 0x00, 0x80, 0x5f, 0x0e, 0x13, 0x53} \
  }

#define NS_SilentDownloadTask_CID                    \
{ /* 18c2f983-b09f-11d2-bcde-00805f0e1353 */         \
    0x18c2f983,                                      \
    0xb09f,                                          \
    0x11d2,                                          \
    {0xbc, 0xde, 0x00, 0x80, 0x5f, 0x0e, 0x13, 0x53} \
  }

#define NS_SilentDownloadFactory_CID                 \
{ /* 18c2f984-b09f-11d2-bcde-00805f0e1353 */         \
    0x18c2f984,                                      \
    0xb09f,                                          \
    0x11d2,                                          \
    {0xbc, 0xde, 0x00, 0x80, 0x5f, 0x0e, 0x13, 0x53} \
  }

#define NS_SilentDownloadTaskFactory_CID             \
{ /* 18c2f985-b09f-11d2-bcde-00805f0e1353 */         \
    0x18c2f985,                                      \
    0xb09f,                                          \
    0x11d2,                                          \
    {0xbc, 0xde, 0x00, 0x80, 0x5f, 0x0e, 0x13, 0x53} \
  }

#endif /* nsSilentDownload_h___ */
