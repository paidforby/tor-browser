#
# The contents of this file are subject to the Netscape Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/NPL/
#
# Software distributed under the License is distributed on an "AS
# IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
# implied. See the License for the specific language governing
# rights and limitations under the License.
#
# The Original Code is mozilla.org code.
#
# The Initial Developer of the Original Code is Netscape
# Communications Corporation.  Portions created by Netscape are
# Copyright (C) 1998 Netscape Communications Corporation. All
# Rights Reserved.
#
# Contributor(s): 
#

DEPTH		= ../..
topsrcdir	= @top_srcdir@
srcdir		= @srcdir@
VPATH		= @srcdir@

include $(DEPTH)/config/autoconf.mk

# A unique identifier that describes this host
ifdef MOZILLA_CONFIG_HOST_IDENT
DETECT_UNIQ_IDENT = $(MOZILLA_CONFIG_HOST_IDENT)
else
DETECT_UNIQ_IDENT = $(shell $(DETECT_DIR)/detect_hostident.sh)
endif

# Where we live
DETECT_DIR	= $(topsrcdir)/config/mkdetect

# Name of generated makefile
DETECT_NAME	= detect_$(DETECT_UNIQ_IDENT)_gen.mk

# The glibc detection script
DETECT_GLIBC	= $(DETECT_DIR)/detect_glibc.sh -o $(DETECT_UNIQ_IDENT) -cc $(CC)

# The Motif detection script
ifeq ($(MOZ_WIDGET_TOOLKIT),motif)
DETECT_MOTIF	= $(DETECT_DIR)/detect_motif.sh -o $(DETECT_UNIQ_IDENT) -xif $(X11_INCLUDE_FLAGS) -cc $(CC)
endif

# The X11 detection script
DETECT_X11	= $(DETECT_DIR)/detect_x11.sh -o $(DETECT_UNIQ_IDENT) -cc $(CC)

# This is needed for DETECT_MOTIF to work
X11_INCLUDE_FLAGS = $(shell $(DETECT_X11) -if)

#
# The following macros are generated:
#
# MOZILLA_XFE_GLIBC_BROKEN_LOCALE_FLAGS
#	Needed because of currently broken locale support when mixing GNU
#	libc2 with any version of Motif.
#
# MOZILLA_XFE_MOTIF_PRINT_SHELL_FLAGS
#	X Print Shell Extension (available starting with X11R6.3) needed
#	by Motif 2.1.
#
# MOZILLA_XFE_USE_WINNING_FILE_SELECTION
#	Hacks to the Motif file selection code to "fix" it.  Where "fix" 
#	means "make the Motif fsb not suck as hell".  Motif source code
#	is needed for this hack (it breaks with Lesstif)
#
# Questions about any of these flags or this wacky scheme in general, should 
# go to ramiro@netscape.com.
#

all: export

export detect: $(DETECT_NAME)

clean realclean clobber clobber_all:
	@rm -f $(DETECT_NAME) *_motif_$(DETECT_UNIQ_IDENT)* *_glibc_$(DETECT_UNIQ_IDENT)* *_x11_$(DETECT_UNIQ_IDENT)*

$(DETECT_NAME):
	@echo "Generating $(DETECT_NAME)."
	@rm -f $@
	@touch $@
	@echo "#" >> $@
	@echo "# Do Not edit." >> $@
	@echo "# Automagically generated file." >> $@
	@echo "#" >> $@
	@echo >> $@
	@echo >> $@
##
## GLIBC
##
## In theory, you can use glibc2 in many platforms.  Right now only Linux
## seems to be using it, though.
##
## Test whether -lBrokenLocale is needed.  Locale support is broken
## when using Motif (any version) with GNU libc2.  
##
ifeq ($(shell $(DETECT_GLIBC)),2)
	@echo "# GLIBC2" >> $@
	@echo "MOZILLA_XFE_USING_GLIBC2 = 1" >> $@
	@echo >> $@
	@echo "# Broken Locale" >> $@
	@echo "MOZILLA_XFE_GLIBC_BROKEN_LOCALE_FLAGS = -lBrokenLocale" >> $@
endif
##
## X11
##
## Set the X11 include flags
##
	@echo >> $@
	@echo "# X11" >> $@
	@echo "MOZILLA_XFE_X11_INCLUDE_FLAGS = `$(DETECT_X11) -if`" >> $@
##
## Determine if we have static X11 libs and set static flags.
##
ifneq ($(shell $(DETECT_X11) -sl),unknown)
	@echo "MOZILLA_XFE_X11_HAVE_STATIC_LIB = 1" >> $@
	@echo "MOZILLA_XFE_X11_STATIC_FLAGS = `$(DETECT_X11) -sf`" >> $@
endif
##
## Determine if we have dynamic X11 libs and set dynamic flags.
##
ifneq ($(shell $(DETECT_X11) -dl),unknown)
	@echo "MOZILLA_XFE_X11_HAVE_DYNAMIC_LIB = 1" >> $@
	@echo "MOZILLA_XFE_X11_DYNAMIC_PATHS = `$(DETECT_X11) -dp`" >> $@
	@echo "MOZILLA_XFE_X11_DYNAMIC_FLAGS = `$(DETECT_X11) -df`" >> $@
endif
##
## Test whether -lSM -lICE are needed.   These libs are need for session
## management in X11R6.
##
ifeq ($(shell $(DETECT_X11)),6)
	@echo "MOZILLA_XFE_X11_SM_FLAGS = -lSM -lICE" >> $@
endif
##
## Motif
##
ifeq ($(MOZ_WIDGET_TOOLKIT),motif)
##
## Set the Motif include flags
##
	@echo >> $@
	@echo "# Motif" >> $@
	@echo "MOZILLA_XFE_MOTIF_INCLUDE_FLAGS = `$(DETECT_MOTIF) -if`" >> $@
##
## Determine if we have static Motif libs and set static flags.
##
ifneq ($(shell $(DETECT_MOTIF) -sl),unknown)
	@echo "MOZILLA_XFE_MOTIF_HAVE_STATIC_LIB = 1" >> $@
	@echo "MOZILLA_XFE_MOTIF_STATIC_FLAGS = `$(DETECT_MOTIF) -sf`" >> $@
endif
##
## Determine if we have dynamic Motif libs and set dynamic flags.
##
ifneq ($(shell $(DETECT_MOTIF) -dl),unknown)
	@echo "MOZILLA_XFE_MOTIF_HAVE_DYNAMIC_LIB = 1" >> $@
	@echo "MOZILLA_XFE_MOTIF_DYNAMIC_PATHS = `$(DETECT_MOTIF) -dp`" >> $@
	@echo "MOZILLA_XFE_MOTIF_DYNAMIC_FLAGS = `$(DETECT_MOTIF) -df`" >> $@
endif
##
## Test whether -lXp is needed.   The -lXp (Print Shell Extensions)
## are needed only with Motif 2.1.
##
ifeq ($(shell $(DETECT_MOTIF)),2.1)
	@echo "MOZILLA_XFE_MOTIF_PRINT_SHELL_FLAGS = -lXp" >> $@
endif
##
## Test whether -lXpm is needed.   The -lXpm (X Pixmap Libary) is needed
## by some versions of Motif.  Some vendors try to do their customers
## favors by hacking the stock Motif libs to include Xpm support.
##
ifeq ($(shell $(DETECT_MOTIF) -xpm),True)
	@echo "MOZILLA_XFE_MOTIF_XPM_FLAGS = -lXpm" >> $@
endif
##
## Test whether we are using "real" Motif libraries.  If we are, then
## we can turn on the file selection hack which only works with Motif
## (not Lesstif).
##
ifneq ($(shell $(DETECT_MOTIF) -l),True)
	@echo >> $@
	@echo "# Winning File Selection" >> $@
	@echo "MOZILLA_XFE_USE_WINNING_FILE_SELECTION = 1" >> $@
endif
endif

