# The contents of this file are subject to the Netscape Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/NPL/
#
# Software distributed under the License is distributed on an "AS
# IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
# implied. See the License for the specific language governing
# rights and limitations under the License.
#
# The Original Code is mozilla.org code.
#
# The Initial Developer of the Original Code is Netscape
# Communications Corporation.  Portions created by Netscape are
# Copyright (C) 1998 Netscape Communications Corporation. All
# Rights Reserved.
#
# Contributor(s): 

#//------------------------------------------------------------------------
#//
#// Makefile for MOZILLA/CONFIG - various commands used by other makefiles
#//
#//------------------------------------------------------------------------

!if "$(MOZ_BITS)" == "16"
!error This makefile must be build using 32-bit tools
!endif

#//------------------------------------------------------------------------
#//
#// Specify the depth of the current directory relative to the
#// root of MOZILLA
#//
#//------------------------------------------------------------------------
DEPTH = ..

#// Specify any "command" targets. (ie. DIRS, INSTALL_FILES, ...)
#// (these must come before the common makefiles are included)
#// 
#// DIRS          - There is a subdirectory to process
#// INSTALL_FILES - Files to be installed in the distribution area
#//
#//------------------------------------------------------------------------

!ifdef MOZ_FULLCIRCLE
INSTALL_FILE_LIST=$(MOZ_TOOLS)\bin\fullsoft.dll $(MOZ_TOOLS)\bin\talkback.cnt $(MOZ_TOOLS)\bin\talkback.exe $(MOZ_TOOLS)\bin\talkback.hlp
INSTALL_DIR=$(DIST)\bin
!endif

GARBAGE = *.obj *.sbr *.pdb

#//------------------------------------------------------------------------
#//
#// Include the common makefile rules
#//
#//------------------------------------------------------------------------
include <$(DEPTH)/config/config.mak>
include <$(DEPTH)/config/rules.mak>

# update build number from within make script
build_number::
!ifdef MOZILLA_OFFICIAL 
    echo Generating build number...
    $(RM) $@
    $(PERL) bdate.pl > $@
    echo Setting build number...
    $(PERL) aboutime.pl $(DEPTH)/xpfe/browser/resources/locale/en-US/navigator.dtd $@
    $(PERL) aboutime.pl $(DEPTH)/xpfe/appshell/public/nsBuildID.h $@
!endif

#// Rule to set the timebomb on official mozilla builds


#//
#// Rules to build make utils
#//

#CFLAGS = /FR /Zi -MDd /W4
CFLAGS = /O2 /GB /MD

makecopy.exe:: makecopy.cpp
    $(CC) $(CFLAGS) $**

mangle.exe:: mangle.c
    $(CC) $(CFLAGS) $**

mantomak.exe:: mantomak.c
    $(CC) $(CFLAGS) $**

bin2rc.exe:: bin2rc.c
    $(CC) $(CFLAGS) $**

makedep.exe:: makedep.cpp
    $(CC) -MT /O2 /GB $**

export:: makecopy.exe mangle.exe mantomak.exe bin2rc.exe makedep.exe build_number
       $(MAKE_INSTALL) nsBuildID.h $(PUBLIC)

!ifdef MOZ_FULLCIRCLE

master.pl: build_number
        $(PERL) build-number.pl

master.ini: master.pl
	$(RM) $@
	$(PERL) master.pl

install:: master.ini
	$(MAKE_INSTALL) master.ini $(DIST)\bin\
!endif

#install::
#	$(RM) $(DIST)\bin\chrome\installed-chrome.txt

FORCE::
